(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.th_yellow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FEC54E").s().p("A53QxQDIieIwoEQI0oGC5jGQENktCQiYQEFkTBbgZQAsgNDCBMQEEBlA5AQQBEATDmAtQC2AkACALQjXC/kPDpQocHQkRDQQkODOm/DeQlFChkvBRQlEBWlCAAIgVAAg");
	this.shape.setTransform(683.9,1025.6,2.151,2.151);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEC54E").s().p("AkMgCQiOiYhvhwQChAFEPAWQEgAnFDA8IkRDJQilB6hpBUIj3kNg");
	this.shape_1.setTransform(112.2,1081.5,2.151,2.151);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FEC54E").s().p("Au1MLQmAh4gMgkQgGgTDOjPQDvjvEajoQNCqwJIhxQDIg5DDDJQBEBHAxBTQAqBJgCAYQgEAhi5CGQhtBPlODhQrkH0j9DfIhtBqQhAA+gcAFQgIACgMAAQhlAAlchug");
	this.shape_2.setTransform(1496.8,191.1,2.151,2.151);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.th_yellow, new cjs.Rectangle(0,0,1786.3,1256.4), null);


(lib.th_red = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5443D").s().p("AcNNQQg4gGhlhKQg1gniZiCQiUh9hZg/QiJhihtgoQoajDmQhWQn/hum5AbQm/AbkmBHQgvALhFAWIh1AmIgBAAIjpBLQg4gKhRgMQiigXh+gKQGZljIbjrQGui8HGhaQHRhcHKARQFJANF/BOQFTBFFuCwQDrBxFCDwQE8DqBDBsQAOAWADALQAHAUheBnQheBniABzQlDEgh3AAIgJAAg");
	this.shape.setTransform(551.8,182.5,2.151,2.151);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E5443D").s().p("Aq3HuQBehYD/kYQDVjoB6hiQA+gyCshjQCthkBOg+QAmgeBeBKQAgAYAeAfQAaAaACAFQAJAWi9DsQjMD9iJBsQh2BcheA5Qh6BJh1AjQhnAghqABIgKAAQhpAAhfgeg");
	this.shape_1.setTransform(1261.7,335.3,2.151,2.151);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.th_red, new cjs.Rectangle(0,0,1411.4,448.1), null);


(lib.th_orange = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF6136").s().p("AoLJuIgGgqQgEgkC5jfQBmh8DPj2IB8ihQBOhkA5hBQCji8CThVIhyLXQgGAIiZCdQh6BmjfB1QjFBojPBQQgEACgEAAQgQAAgHgbg");
	this.shape.setTransform(1554.3,139.6,2.151,2.151);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EF6136").s().p("A5wG9QKiqHNjrHQEcjpDRiRQEwjTBdAQQAZADE7B7QENBpEBBoIgJAdQgGATgFAIQhTCKjiDJQgsAmiYCDQh6BnhDA/ImUF4QjuDciiCcQjLDFimCSQjLCzi4CLQmqFCnVCuQg+nrhCoog");
	this.shape_1.setTransform(354.8,811.3,2.151,2.151);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.th_orange, new cjs.Rectangle(0,0,1668.3,1131.4), null);


(lib.th_green = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#82C050").s().p("AjCR8QkrgQlJhFQnphnpgjdQh8gtkuh6QkVhvieg3IAegYQAogeAzgdQChhfDKg4QDhg9EXgUQEPgTDXAcQCTATFbBcQE/BUC0AOQE6AYFLhFQE2hBEpiNQDnhsDiicQDmifC8i1QFUlJEOkVQByDDDzFvQjyFfkeE2QkvFJk0DqQj1C7lGCCQk9B/lBAtQjaAejgAAQhcAAhdgFg");
	this.shape.setTransform(1287.9,248,2.151,2.151);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#82C050").s().p("AvUSwQjTgYgugBQiKiLiFh6QDjnlDhlpQCbj3DDjlQDFjmDdi+QC6igEchjQD6hYEWgaQCvgRFAAZQEuAYACATInwE/Qk3DIjfDcQhIBHhsBbIi9CbQjkC/hvCVQhHBfhaCmQgoBJhwDfQhaCygwBPQhFBzgvAaQgOAIgsAAQguAAhQgJg");
	this.shape_1.setTransform(324.5,383.6,2.151,2.151);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.th_green, new cjs.Rectangle(0,0,1886.1,643.6), null);


(lib.th_blue = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0088C0").s().p("EgqYATyQgNgYDRlkQDXltCbjSQDOkWEWkIQE/kxF+j+QGNkIFziAQGxiXImgaQKGgfJKCeQGFBpEvCoInRLAQjogVlghdQirgthOgQQiHgdhxgFQmggSkxA7QlyBHkpDEQk0DKlbEgIjCCnQmAFMi2CaQlLEXiQBkg");
	this.shape.setTransform(583.7,360.9,2.151,2.151);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0088C0").s().p("AlXEnQhGhfgZgZQDLheDpiPQCxhuDgidQAVgPATgEQgZBBhUBUQhjBbgnAnQiOCRhGBIQh/CAhVA/QgNAKgPAAQgkAAgvg2g");
	this.shape_1.setTransform(1551.3,75,2.151,2.151);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0088C0").s().p("AjiC/IAzg+QDFj0BtieQBZBHBPA6QkDDblSDHIBIhTg");
	this.shape_2.setTransform(1702.5,169.5,2.151,2.151);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.th_blue, new cjs.Rectangle(0,0,1766.8,654.6), null);


(lib.th_black = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#012A3F").s().p("AqSFxQg0gJALgoQAFgQAfgtQCPjPEtnCQAwARG5EYQDdCLDTCJQgPAGgGAEQjxCPmUAyQisAVigAAQi+AAisgeg");
	this.shape.setTransform(1472.1,549.3,2.151,2.151);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#012A3F").s().p("AvMGqQgGgcHinUQEDj9GTjwQDZiCICkGQB4B1hHEBQgRA/guB3QgiBcAAAWQAABXnxIcQoIIngtA/g");
	this.shape_1.setTransform(1755.9,205.6,2.151,2.151);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#012A3F").s().p("Aj/EMQiFhsh5hQQBJg6BFgsQBDgsCchfQCghiD6h0QDqhtAKAKIhKBpIiwE5QjAFdhTC2QiLh9hlhSg");
	this.shape_2.setTransform(109.6,777.4,2.151,2.151);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#012A3F").s().p("Aq9BnQB2hlDCivQDmjPBQhHIAZAPQAdAOATAEQDyAxHfAQQDvAJC+gCQhhBgikB5QlKDulSBwQlRBxlXAyQi/AdnnAnQEajbCgiCg");
	this.shape_3.setTransform(1217.3,866.2,2.151,2.151);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.th_black, new cjs.Rectangle(0,0,1965.1,963.3), null);


(lib.stroke_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.7,y:250}).wait(1).to({graphics:mask_graphics_3,x:-499.2,y:250}).wait(1).to({graphics:mask_graphics_4,x:-498.2,y:250}).wait(1).to({graphics:mask_graphics_5,x:-496.6,y:250}).wait(1).to({graphics:mask_graphics_6,x:-494.2,y:250}).wait(1).to({graphics:mask_graphics_7,x:-490.8,y:250}).wait(1).to({graphics:mask_graphics_8,x:-486.3,y:250}).wait(1).to({graphics:mask_graphics_9,x:-480.5,y:250}).wait(1).to({graphics:mask_graphics_10,x:-473.3,y:250}).wait(1).to({graphics:mask_graphics_11,x:-464.5,y:250}).wait(1).to({graphics:mask_graphics_12,x:-453.9,y:250}).wait(1).to({graphics:mask_graphics_13,x:-441.4,y:250}).wait(1).to({graphics:mask_graphics_14,x:-426.8,y:250}).wait(1).to({graphics:mask_graphics_15,x:-410,y:250}).wait(1).to({graphics:mask_graphics_16,x:-390.7,y:250}).wait(1).to({graphics:mask_graphics_17,x:-369,y:250}).wait(1).to({graphics:mask_graphics_18,x:-344.5,y:250}).wait(1).to({graphics:mask_graphics_19,x:-317.1,y:250}).wait(1).to({graphics:mask_graphics_20,x:-286.7,y:250}).wait(1).to({graphics:mask_graphics_21,x:-253.1,y:250}).wait(1).to({graphics:mask_graphics_22,x:-216.1,y:250}).wait(1).to({graphics:mask_graphics_23,x:-175.6,y:250}).wait(1).to({graphics:mask_graphics_24,x:-132.4,y:250}).wait(1).to({graphics:mask_graphics_25,x:-91.9,y:250}).wait(1).to({graphics:mask_graphics_26,x:-54.9,y:250}).wait(1).to({graphics:mask_graphics_27,x:-21.3,y:250}).wait(1).to({graphics:mask_graphics_28,x:9.1,y:250}).wait(1).to({graphics:mask_graphics_29,x:36.5,y:250}).wait(1).to({graphics:mask_graphics_30,x:61,y:250}).wait(1).to({graphics:mask_graphics_31,x:82.8,y:250}).wait(1).to({graphics:mask_graphics_32,x:102,y:250}).wait(1).to({graphics:mask_graphics_33,x:118.8,y:250}).wait(1).to({graphics:mask_graphics_34,x:133.4,y:250}).wait(1).to({graphics:mask_graphics_35,x:145.9,y:250}).wait(1).to({graphics:mask_graphics_36,x:156.5,y:250}).wait(1).to({graphics:mask_graphics_37,x:165.3,y:250}).wait(1).to({graphics:mask_graphics_38,x:172.5,y:250}).wait(1).to({graphics:mask_graphics_39,x:178.3,y:250}).wait(1).to({graphics:mask_graphics_40,x:182.8,y:250}).wait(1).to({graphics:mask_graphics_41,x:186.2,y:250}).wait(1).to({graphics:mask_graphics_42,x:188.6,y:250}).wait(1).to({graphics:mask_graphics_43,x:190.3,y:250}).wait(1).to({graphics:mask_graphics_44,x:191.2,y:250}).wait(1).to({graphics:mask_graphics_45,x:191.8,y:250}).wait(1).to({graphics:mask_graphics_46,x:191.9,y:250}).wait(1).to({graphics:mask_graphics_47,x:191.9,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#012A3F").s().p("AsTG3QisgDjtgOImYgbQADAAAFgGQAGgGADAAIHUAdQEJAODJAAQE9gBEjg3QErg3EchyQFdiMEEiSQECiRC3jJQACgCAGgCQAHgCACgCQhaBihrBVQhsBWhyBAQk5CwkDBvQk/CJkbA6QkxBAmTAAIhbgBg");
	this.shape.setTransform(345.1,94.6,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,189.3);


(lib.stroke_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.7,y:250}).wait(1).to({graphics:mask_graphics_3,x:-499.3,y:250}).wait(1).to({graphics:mask_graphics_4,x:-498.4,y:250}).wait(1).to({graphics:mask_graphics_5,x:-496.9,y:250}).wait(1).to({graphics:mask_graphics_6,x:-494.7,y:250}).wait(1).to({graphics:mask_graphics_7,x:-491.7,y:250}).wait(1).to({graphics:mask_graphics_8,x:-487.6,y:250}).wait(1).to({graphics:mask_graphics_9,x:-482.4,y:250}).wait(1).to({graphics:mask_graphics_10,x:-475.9,y:250}).wait(1).to({graphics:mask_graphics_11,x:-468,y:250}).wait(1).to({graphics:mask_graphics_12,x:-458.4,y:250}).wait(1).to({graphics:mask_graphics_13,x:-447.1,y:250}).wait(1).to({graphics:mask_graphics_14,x:-434,y:250}).wait(1).to({graphics:mask_graphics_15,x:-418.8,y:250}).wait(1).to({graphics:mask_graphics_16,x:-401.5,y:250}).wait(1).to({graphics:mask_graphics_17,x:-381.9,y:250}).wait(1).to({graphics:mask_graphics_18,x:-359.8,y:250}).wait(1).to({graphics:mask_graphics_19,x:-335.1,y:250}).wait(1).to({graphics:mask_graphics_20,x:-307.7,y:250}).wait(1).to({graphics:mask_graphics_21,x:-277.4,y:250}).wait(1).to({graphics:mask_graphics_22,x:-244.1,y:250}).wait(1).to({graphics:mask_graphics_23,x:-207.6,y:250}).wait(1).to({graphics:mask_graphics_24,x:-168.6,y:250}).wait(1).to({graphics:mask_graphics_25,x:-132.1,y:250}).wait(1).to({graphics:mask_graphics_26,x:-98.8,y:250}).wait(1).to({graphics:mask_graphics_27,x:-68.5,y:250}).wait(1).to({graphics:mask_graphics_28,x:-41.1,y:250}).wait(1).to({graphics:mask_graphics_29,x:-16.4,y:250}).wait(1).to({graphics:mask_graphics_30,x:5.7,y:250}).wait(1).to({graphics:mask_graphics_31,x:25.3,y:250}).wait(1).to({graphics:mask_graphics_32,x:42.6,y:250}).wait(1).to({graphics:mask_graphics_33,x:57.8,y:250}).wait(1).to({graphics:mask_graphics_34,x:70.9,y:250}).wait(1).to({graphics:mask_graphics_35,x:82.2,y:250}).wait(1).to({graphics:mask_graphics_36,x:91.7,y:250}).wait(1).to({graphics:mask_graphics_37,x:99.7,y:250}).wait(1).to({graphics:mask_graphics_38,x:106.2,y:250}).wait(1).to({graphics:mask_graphics_39,x:111.4,y:250}).wait(1).to({graphics:mask_graphics_40,x:115.5,y:250}).wait(1).to({graphics:mask_graphics_41,x:118.5,y:250}).wait(1).to({graphics:mask_graphics_42,x:120.7,y:250}).wait(1).to({graphics:mask_graphics_43,x:122.2,y:250}).wait(1).to({graphics:mask_graphics_44,x:123.1,y:250}).wait(1).to({graphics:mask_graphics_45,x:123.5,y:250}).wait(1).to({graphics:mask_graphics_46,x:123.7,y:250}).wait(1).to({graphics:mask_graphics_47,x:123.7,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#82C14F").s().p("A2gEuQACABAFgFQAFgFAEABQGPBnFyAIQJWAMJZjSIB+gtQlgCLlcA9QkSAvkYAAQmlAAmzhrgAQXgMQEEieB2jnQACgCAGgCQAHgBABgCQhaCWh2BuQh5BwidBKQAygaAqgYg");
	this.shape.setTransform(310,87.9,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,175.9);


(lib.stroke_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.7,y:250}).wait(1).to({graphics:mask_graphics_3,x:-499.2,y:250}).wait(1).to({graphics:mask_graphics_4,x:-498.3,y:250}).wait(1).to({graphics:mask_graphics_5,x:-496.8,y:250}).wait(1).to({graphics:mask_graphics_6,x:-494.4,y:250}).wait(1).to({graphics:mask_graphics_7,x:-491.2,y:250}).wait(1).to({graphics:mask_graphics_8,x:-486.9,y:250}).wait(1).to({graphics:mask_graphics_9,x:-481.4,y:250}).wait(1).to({graphics:mask_graphics_10,x:-474.6,y:250}).wait(1).to({graphics:mask_graphics_11,x:-466.2,y:250}).wait(1).to({graphics:mask_graphics_12,x:-456.1,y:250}).wait(1).to({graphics:mask_graphics_13,x:-444.2,y:250}).wait(1).to({graphics:mask_graphics_14,x:-430.3,y:250}).wait(1).to({graphics:mask_graphics_15,x:-414.3,y:250}).wait(1).to({graphics:mask_graphics_16,x:-396,y:250}).wait(1).to({graphics:mask_graphics_17,x:-375.2,y:250}).wait(1).to({graphics:mask_graphics_18,x:-351.9,y:250}).wait(1).to({graphics:mask_graphics_19,x:-325.8,y:250}).wait(1).to({graphics:mask_graphics_20,x:-296.9,y:250}).wait(1).to({graphics:mask_graphics_21,x:-264.9,y:250}).wait(1).to({graphics:mask_graphics_22,x:-229.7,y:250}).wait(1).to({graphics:mask_graphics_23,x:-191.1,y:250}).wait(1).to({graphics:mask_graphics_24,x:-150,y:250}).wait(1).to({graphics:mask_graphics_25,x:-111.4,y:250}).wait(1).to({graphics:mask_graphics_26,x:-76.2,y:250}).wait(1).to({graphics:mask_graphics_27,x:-44.2,y:250}).wait(1).to({graphics:mask_graphics_28,x:-15.3,y:250}).wait(1).to({graphics:mask_graphics_29,x:10.8,y:250}).wait(1).to({graphics:mask_graphics_30,x:34.1,y:250}).wait(1).to({graphics:mask_graphics_31,x:54.9,y:250}).wait(1).to({graphics:mask_graphics_32,x:73.2,y:250}).wait(1).to({graphics:mask_graphics_33,x:89.2,y:250}).wait(1).to({graphics:mask_graphics_34,x:103.1,y:250}).wait(1).to({graphics:mask_graphics_35,x:115,y:250}).wait(1).to({graphics:mask_graphics_36,x:125,y:250}).wait(1).to({graphics:mask_graphics_37,x:133.4,y:250}).wait(1).to({graphics:mask_graphics_38,x:140.3,y:250}).wait(1).to({graphics:mask_graphics_39,x:145.8,y:250}).wait(1).to({graphics:mask_graphics_40,x:150.1,y:250}).wait(1).to({graphics:mask_graphics_41,x:153.3,y:250}).wait(1).to({graphics:mask_graphics_42,x:155.6,y:250}).wait(1).to({graphics:mask_graphics_43,x:157.2,y:250}).wait(1).to({graphics:mask_graphics_44,x:158.1,y:250}).wait(1).to({graphics:mask_graphics_45,x:158.6,y:250}).wait(1).to({graphics:mask_graphics_46,x:158.8,y:250}).wait(1).to({graphics:mask_graphics_47,x:158.8,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FEC54E").s().p("AzIG0QDkodDxlhQB1irB+iKQghAlggAoQh9CaiEDZQj7GejKIQQibGUhFFSQgBADgHAEQgIAEgBACQBgm+DQnwgAkBvtIAIgGQhFA2hEA9QBAg8BBgxgAgkyAQFQi+GygeQGugdFtCJQgDgBgGAEQgGAEgCgBQmiimntBMQj6AnjKBPQjTBSi2CFQBlhNBrg8g");
	this.shape.setTransform(328.8,296.3,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,500);


(lib.stroke_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.7,y:250}).wait(1).to({graphics:mask_graphics_3,x:-499.2,y:250}).wait(1).to({graphics:mask_graphics_4,x:-498.3,y:250}).wait(1).to({graphics:mask_graphics_5,x:-496.8,y:250}).wait(1).to({graphics:mask_graphics_6,x:-494.5,y:250}).wait(1).to({graphics:mask_graphics_7,x:-491.3,y:250}).wait(1).to({graphics:mask_graphics_8,x:-487.1,y:250}).wait(1).to({graphics:mask_graphics_9,x:-481.6,y:250}).wait(1).to({graphics:mask_graphics_10,x:-474.8,y:250}).wait(1).to({graphics:mask_graphics_11,x:-466.5,y:250}).wait(1).to({graphics:mask_graphics_12,x:-456.5,y:250}).wait(1).to({graphics:mask_graphics_13,x:-444.7,y:250}).wait(1).to({graphics:mask_graphics_14,x:-430.9,y:250}).wait(1).to({graphics:mask_graphics_15,x:-415,y:250}).wait(1).to({graphics:mask_graphics_16,x:-396.9,y:250}).wait(1).to({graphics:mask_graphics_17,x:-376.4,y:250}).wait(1).to({graphics:mask_graphics_18,x:-353.2,y:250}).wait(1).to({graphics:mask_graphics_19,x:-327.4,y:250}).wait(1).to({graphics:mask_graphics_20,x:-298.7,y:250}).wait(1).to({graphics:mask_graphics_21,x:-267,y:250}).wait(1).to({graphics:mask_graphics_22,x:-232.1,y:250}).wait(1).to({graphics:mask_graphics_23,x:-193.9,y:250}).wait(1).to({graphics:mask_graphics_24,x:-153.1,y:250}).wait(1).to({graphics:mask_graphics_25,x:-114.9,y:250}).wait(1).to({graphics:mask_graphics_26,x:-80,y:250}).wait(1).to({graphics:mask_graphics_27,x:-48.3,y:250}).wait(1).to({graphics:mask_graphics_28,x:-19.6,y:250}).wait(1).to({graphics:mask_graphics_29,x:6.3,y:250}).wait(1).to({graphics:mask_graphics_30,x:29.4,y:250}).wait(1).to({graphics:mask_graphics_31,x:49.9,y:250}).wait(1).to({graphics:mask_graphics_32,x:68.1,y:250}).wait(1).to({graphics:mask_graphics_33,x:83.9,y:250}).wait(1).to({graphics:mask_graphics_34,x:97.7,y:250}).wait(1).to({graphics:mask_graphics_35,x:109.5,y:250}).wait(1).to({graphics:mask_graphics_36,x:119.5,y:250}).wait(1).to({graphics:mask_graphics_37,x:127.8,y:250}).wait(1).to({graphics:mask_graphics_38,x:134.6,y:250}).wait(1).to({graphics:mask_graphics_39,x:140.1,y:250}).wait(1).to({graphics:mask_graphics_40,x:144.3,y:250}).wait(1).to({graphics:mask_graphics_41,x:147.5,y:250}).wait(1).to({graphics:mask_graphics_42,x:149.8,y:250}).wait(1).to({graphics:mask_graphics_43,x:151.4,y:250}).wait(1).to({graphics:mask_graphics_44,x:152.3,y:250}).wait(1).to({graphics:mask_graphics_45,x:152.8,y:250}).wait(1).to({graphics:mask_graphics_46,x:152.9,y:250}).wait(1).to({graphics:mask_graphics_47,x:152.9,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C5E4A1").s().p("AxBQRQnQgPoYjGQADABAFgEQAGgDADABQIeDOH6AGQDhACDhgnQDhgoDShOQDIhMDWiLQCrhvDSitQCFhvCLh7IgkAhQjUC9jBCTQlPD+kaBuQjhBXj2ApQjKAijKAAIhUgBgEAgZgQEIAJgHIAIgGQhNBXhNBRICJibg");
	this.shape.setTransform(449.6,224.1,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,448.2);


(lib.stroke_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_3,x:-499.7,y:250}).wait(1).to({graphics:mask_graphics_4,x:-499.4,y:250}).wait(1).to({graphics:mask_graphics_5,x:-498.8,y:250}).wait(1).to({graphics:mask_graphics_6,x:-498,y:250}).wait(1).to({graphics:mask_graphics_7,x:-496.9,y:250}).wait(1).to({graphics:mask_graphics_8,x:-495.4,y:250}).wait(1).to({graphics:mask_graphics_9,x:-493.5,y:250}).wait(1).to({graphics:mask_graphics_10,x:-491.1,y:250}).wait(1).to({graphics:mask_graphics_11,x:-488.1,y:250}).wait(1).to({graphics:mask_graphics_12,x:-484.6,y:250}).wait(1).to({graphics:mask_graphics_13,x:-480.5,y:250}).wait(1).to({graphics:mask_graphics_14,x:-475.6,y:250}).wait(1).to({graphics:mask_graphics_15,x:-470.1,y:250}).wait(1).to({graphics:mask_graphics_16,x:-463.7,y:250}).wait(1).to({graphics:mask_graphics_17,x:-456.4,y:250}).wait(1).to({graphics:mask_graphics_18,x:-448.3,y:250}).wait(1).to({graphics:mask_graphics_19,x:-439.2,y:250}).wait(1).to({graphics:mask_graphics_20,x:-429.1,y:250}).wait(1).to({graphics:mask_graphics_21,x:-418,y:250}).wait(1).to({graphics:mask_graphics_22,x:-405.7,y:250}).wait(1).to({graphics:mask_graphics_23,x:-392.2,y:250}).wait(1).to({graphics:mask_graphics_24,x:-377.9,y:250}).wait(1).to({graphics:mask_graphics_25,x:-364.4,y:250}).wait(1).to({graphics:mask_graphics_26,x:-352.2,y:250}).wait(1).to({graphics:mask_graphics_27,x:-341,y:250}).wait(1).to({graphics:mask_graphics_28,x:-330.9,y:250}).wait(1).to({graphics:mask_graphics_29,x:-321.8,y:250}).wait(1).to({graphics:mask_graphics_30,x:-313.7,y:250}).wait(1).to({graphics:mask_graphics_31,x:-306.4,y:250}).wait(1).to({graphics:mask_graphics_32,x:-300.1,y:250}).wait(1).to({graphics:mask_graphics_33,x:-294.5,y:250}).wait(1).to({graphics:mask_graphics_34,x:-289.6,y:250}).wait(1).to({graphics:mask_graphics_35,x:-285.5,y:250}).wait(1).to({graphics:mask_graphics_36,x:-282,y:250}).wait(1).to({graphics:mask_graphics_37,x:-279,y:250}).wait(1).to({graphics:mask_graphics_38,x:-276.6,y:250}).wait(1).to({graphics:mask_graphics_39,x:-274.7,y:250}).wait(1).to({graphics:mask_graphics_40,x:-273.2,y:250}).wait(1).to({graphics:mask_graphics_41,x:-272.1,y:250}).wait(1).to({graphics:mask_graphics_42,x:-271.3,y:250}).wait(1).to({graphics:mask_graphics_43,x:-270.7,y:250}).wait(1).to({graphics:mask_graphics_44,x:-270.4,y:250}).wait(1).to({graphics:mask_graphics_45,x:-270.2,y:250}).wait(1).to({graphics:mask_graphics_46,x:-270.2,y:250}).wait(1).to({graphics:mask_graphics_47,x:-270.2,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF6136").s().p("AkFC8QBsiWCBiQQkjFXjOGZQgCADgHADQgHAEgCACQCIkQCOjGgAHppnIAzgqQh8BphsBlIg/A7QByhwCChvg");
	this.shape.setTransform(116.1,141.6,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,283.2);


(lib.stroke_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk KFramed (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.7,y:250}).wait(1).to({graphics:mask_graphics_3,x:-499.3,y:250}).wait(1).to({graphics:mask_graphics_4,x:-498.5,y:250}).wait(1).to({graphics:mask_graphics_5,x:-497.1,y:250}).wait(1).to({graphics:mask_graphics_6,x:-495.1,y:250}).wait(1).to({graphics:mask_graphics_7,x:-492.3,y:250}).wait(1).to({graphics:mask_graphics_8,x:-488.5,y:250}).wait(1).to({graphics:mask_graphics_9,x:-483.6,y:250}).wait(1).to({graphics:mask_graphics_10,x:-477.6,y:250}).wait(1).to({graphics:mask_graphics_11,x:-470.2,y:250}).wait(1).to({graphics:mask_graphics_12,x:-461.3,y:250}).wait(1).to({graphics:mask_graphics_13,x:-450.8,y:250}).wait(1).to({graphics:mask_graphics_14,x:-438.5,y:250}).wait(1).to({graphics:mask_graphics_15,x:-424.4,y:250}).wait(1).to({graphics:mask_graphics_16,x:-408.3,y:250}).wait(1).to({graphics:mask_graphics_17,x:-390,y:250}).wait(1).to({graphics:mask_graphics_18,x:-369.4,y:250}).wait(1).to({graphics:mask_graphics_19,x:-346.5,y:250}).wait(1).to({graphics:mask_graphics_20,x:-320.9,y:250}).wait(1).to({graphics:mask_graphics_21,x:-292.7,y:250}).wait(1).to({graphics:mask_graphics_22,x:-261.7,y:250}).wait(1).to({graphics:mask_graphics_23,x:-227.7,y:250}).wait(1).to({graphics:mask_graphics_24,x:-191.4,y:250}).wait(1).to({graphics:mask_graphics_25,x:-157.4,y:250}).wait(1).to({graphics:mask_graphics_26,x:-126.4,y:250}).wait(1).to({graphics:mask_graphics_27,x:-98.2,y:250}).wait(1).to({graphics:mask_graphics_28,x:-72.6,y:250}).wait(1).to({graphics:mask_graphics_29,x:-49.7,y:250}).wait(1).to({graphics:mask_graphics_30,x:-29.1,y:250}).wait(1).to({graphics:mask_graphics_31,x:-10.8,y:250}).wait(1).to({graphics:mask_graphics_32,x:5.3,y:250}).wait(1).to({graphics:mask_graphics_33,x:19.4,y:250}).wait(1).to({graphics:mask_graphics_34,x:31.7,y:250}).wait(1).to({graphics:mask_graphics_35,x:42.2,y:250}).wait(1).to({graphics:mask_graphics_36,x:51,y:250}).wait(1).to({graphics:mask_graphics_37,x:58.4,y:250}).wait(1).to({graphics:mask_graphics_38,x:64.5,y:250}).wait(1).to({graphics:mask_graphics_39,x:69.4,y:250}).wait(1).to({graphics:mask_graphics_40,x:73.1,y:250}).wait(1).to({graphics:mask_graphics_41,x:76,y:250}).wait(1).to({graphics:mask_graphics_42,x:78,y:250}).wait(1).to({graphics:mask_graphics_43,x:79.4,y:250}).wait(1).to({graphics:mask_graphics_44,x:80.2,y:250}).wait(1).to({graphics:mask_graphics_45,x:80.6,y:250}).wait(1).to({graphics:mask_graphics_46,x:80.8,y:250}).wait(1).to({graphics:mask_graphics_47,x:80.8,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AuJEJQHqlUI3j8QEoiEE1hYQBAgSBagTICZgfQBJgPBDABQBUABA2AbQgCgBgHAHQgHAHgCgBQg2gbhUgBQhEgBhIAPIiUAeQhXATg+ARQntCLomEpQlICwjXCRQkdDAjJDPIgJAGQgGADgDACQDOjMDmihg");
	this.shape.setTransform(288.5,135.5,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1000,0,1000,500);


(lib.stroke_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.8,y:250}).wait(1).to({graphics:mask_graphics_3,x:-499.5,y:250}).wait(1).to({graphics:mask_graphics_4,x:-498.9,y:250}).wait(1).to({graphics:mask_graphics_5,x:-497.9,y:250}).wait(1).to({graphics:mask_graphics_6,x:-496.4,y:250}).wait(1).to({graphics:mask_graphics_7,x:-494.3,y:250}).wait(1).to({graphics:mask_graphics_8,x:-491.5,y:250}).wait(1).to({graphics:mask_graphics_9,x:-487.9,y:250}).wait(1).to({graphics:mask_graphics_10,x:-483.4,y:250}).wait(1).to({graphics:mask_graphics_11,x:-478,y:250}).wait(1).to({graphics:mask_graphics_12,x:-471.4,y:250}).wait(1).to({graphics:mask_graphics_13,x:-463.6,y:250}).wait(1).to({graphics:mask_graphics_14,x:-454.6,y:250}).wait(1).to({graphics:mask_graphics_15,x:-444.2,y:250}).wait(1).to({graphics:mask_graphics_16,x:-432.3,y:250}).wait(1).to({graphics:mask_graphics_17,x:-418.8,y:250}).wait(1).to({graphics:mask_graphics_18,x:-403.6,y:250}).wait(1).to({graphics:mask_graphics_19,x:-386.7,y:250}).wait(1).to({graphics:mask_graphics_20,x:-367.8,y:250}).wait(1).to({graphics:mask_graphics_21,x:-347,y:250}).wait(1).to({graphics:mask_graphics_22,x:-324.1,y:250}).wait(1).to({graphics:mask_graphics_23,x:-299,y:250}).wait(1).to({graphics:mask_graphics_24,x:-272.2,y:250}).wait(1).to({graphics:mask_graphics_25,x:-247.1,y:250}).wait(1).to({graphics:mask_graphics_26,x:-224.2,y:250}).wait(1).to({graphics:mask_graphics_27,x:-203.4,y:250}).wait(1).to({graphics:mask_graphics_28,x:-184.6,y:250}).wait(1).to({graphics:mask_graphics_29,x:-167.6,y:250}).wait(1).to({graphics:mask_graphics_30,x:-152.4,y:250}).wait(1).to({graphics:mask_graphics_31,x:-138.9,y:250}).wait(1).to({graphics:mask_graphics_32,x:-127,y:250}).wait(1).to({graphics:mask_graphics_33,x:-116.6,y:250}).wait(1).to({graphics:mask_graphics_34,x:-107.6,y:250}).wait(1).to({graphics:mask_graphics_35,x:-99.8,y:250}).wait(1).to({graphics:mask_graphics_36,x:-93.3,y:250}).wait(1).to({graphics:mask_graphics_37,x:-87.8,y:250}).wait(1).to({graphics:mask_graphics_38,x:-83.3,y:250}).wait(1).to({graphics:mask_graphics_39,x:-79.7,y:250}).wait(1).to({graphics:mask_graphics_40,x:-76.9,y:250}).wait(1).to({graphics:mask_graphics_41,x:-74.8,y:250}).wait(1).to({graphics:mask_graphics_42,x:-73.3,y:250}).wait(1).to({graphics:mask_graphics_43,x:-72.3,y:250}).wait(1).to({graphics:mask_graphics_44,x:-71.7,y:250}).wait(1).to({graphics:mask_graphics_45,x:-71.4,y:250}).wait(1).to({graphics:mask_graphics_46,x:-71.3,y:250}).wait(1).to({graphics:mask_graphics_47,x:-71.3,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#556B7F").s().p("ApYDzQDDjADiiiQhBAyhBA1Ql3EwkjGKQgCADgGADQgIADgBADQDNkRC7i6gAPiq9IgJAGQgFAEgEABQjrBPjdBfQDihmD4hTg");
	this.shape.setTransform(213.7,150.9,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,301.9);


(lib.stroke_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.7,y:250}).wait(1).to({graphics:mask_graphics_3,x:-499.1,y:250}).wait(1).to({graphics:mask_graphics_4,x:-498,y:250}).wait(1).to({graphics:mask_graphics_5,x:-496.2,y:250}).wait(1).to({graphics:mask_graphics_6,x:-493.5,y:250}).wait(1).to({graphics:mask_graphics_7,x:-489.7,y:250}).wait(1).to({graphics:mask_graphics_8,x:-484.6,y:250}).wait(1).to({graphics:mask_graphics_9,x:-478.1,y:250}).wait(1).to({graphics:mask_graphics_10,x:-470,y:250}).wait(1).to({graphics:mask_graphics_11,x:-460.2,y:250}).wait(1).to({graphics:mask_graphics_12,x:-448.3,y:250}).wait(1).to({graphics:mask_graphics_13,x:-434.3,y:250}).wait(1).to({graphics:mask_graphics_14,x:-417.9,y:250}).wait(1).to({graphics:mask_graphics_15,x:-399.1,y:250}).wait(1).to({graphics:mask_graphics_16,x:-377.5,y:250}).wait(1).to({graphics:mask_graphics_17,x:-353.1,y:250}).wait(1).to({graphics:mask_graphics_18,x:-325.6,y:250}).wait(1).to({graphics:mask_graphics_19,x:-294.9,y:250}).wait(1).to({graphics:mask_graphics_20,x:-260.8,y:250}).wait(1).to({graphics:mask_graphics_21,x:-223.1,y:250}).wait(1).to({graphics:mask_graphics_22,x:-181.7,y:250}).wait(1).to({graphics:mask_graphics_23,x:-136.3,y:250}).wait(1).to({graphics:mask_graphics_24,x:-87.8,y:250}).wait(1).to({graphics:mask_graphics_25,x:-42.4,y:250}).wait(1).to({graphics:mask_graphics_26,x:-1,y:250}).wait(1).to({graphics:mask_graphics_27,x:36.7,y:250}).wait(1).to({graphics:mask_graphics_28,x:70.8,y:250}).wait(1).to({graphics:mask_graphics_29,x:101.5,y:250}).wait(1).to({graphics:mask_graphics_30,x:129,y:250}).wait(1).to({graphics:mask_graphics_31,x:153.4,y:250}).wait(1).to({graphics:mask_graphics_32,x:175,y:250}).wait(1).to({graphics:mask_graphics_33,x:193.8,y:250}).wait(1).to({graphics:mask_graphics_34,x:210.2,y:250}).wait(1).to({graphics:mask_graphics_35,x:224.2,y:250}).wait(1).to({graphics:mask_graphics_36,x:236,y:250}).wait(1).to({graphics:mask_graphics_37,x:245.9,y:250}).wait(1).to({graphics:mask_graphics_38,x:254,y:250}).wait(1).to({graphics:mask_graphics_39,x:260.5,y:250}).wait(1).to({graphics:mask_graphics_40,x:265.6,y:250}).wait(1).to({graphics:mask_graphics_41,x:269.4,y:250}).wait(1).to({graphics:mask_graphics_42,x:272.1,y:250}).wait(1).to({graphics:mask_graphics_43,x:273.9,y:250}).wait(1).to({graphics:mask_graphics_44,x:275,y:250}).wait(1).to({graphics:mask_graphics_45,x:275.6,y:250}).wait(1).to({graphics:mask_graphics_46,x:275.8,y:250}).wait(1).to({graphics:mask_graphics_47,x:275.8,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#83B4DC").s().p("A77TuQAFgFADgBQH6ijHMjWQOBmhM6qvQDLipA3gxQCUiBBnhuQEOkfBakjQABgCAHgBQAIgBABgCQg1Cph0CpQhgCKiVCXQkmEplgEUQlPEGlzDmQtCIHvfFCIAIgFg");
	this.shape.setTransform(386.3,272.4,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,500);


(lib.stroke_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOIAnEMAAAhOHMCcRAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOKAnEMAAAhOHMCcVAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhONAnEMAAAhOHMCcbAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOSAnEMAAAhOHMCclAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOYAnEMAAAhOHMCcxAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOhAnEMAAAhOHMCdDAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOsAnEMAAAhOHMCdZAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhO6AnEMAAAhOHMCd1AAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhPLAnEMAAAhOHMCeXAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhPfAnEMAAAhOHMCe/AAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhP4AnEMAAAhOHMCfxAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhQUAnEMAAAhOHMCgpAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhQ0AnEMAAAhOHMChpAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhRZAnEMAAAhOHMCizAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhSDAnEMAAAhOHMCkHAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhSzAnEMAAAhOHMClnAAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhToAnEMAAAhOHMCnRAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhUiAnEMAAAhOHMCpFAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhVjAnEMAAAhOHMCrHAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhWqAnEMAAAhOHMCtVAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhX5AnEMAAAhOHMCvzAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhZMAnEMAAAhOHMCyZAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhaaAnEMAAAhOHMC01AAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhbiAnEMAAAhOHMC3FAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhcjAnEMAAAhOHMC5HAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhddAnEMAAAhOHMC67AAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EheSAnEMAAAhOHMC8lAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhfBAnEMAAAhOHMC+DAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhfrAnEMAAAhOHMC/XAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhgQAnEMAAAhOHMDAhAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhgxAnEMAAAhOHMDBjAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhhNAnEMAAAhOHMDCbAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhhlAnEMAAAhOHMDDLAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("Ehh6AnEMAAAhOHMDD1AAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhiLAnEMAAAhOHMDEXAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhiZAnEMAAAhOHMDEzAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhikAnEMAAAhOHMDFJAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhisAnEMAAAhOHMDFZAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhizAnEMAAAhOHMDFnAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("Ehi4AnEMAAAhOHMDFxAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("Ehi7AnEMAAAhOHMDF3AAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("Ehi9AnEMAAAhOHMDF7AAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("Ehi+AnEMAAAhOHMDF9AAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("Ehi+AnEMAAAhOHMDF9AAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("Ehi/AnEMAAAhOHMDF/AAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.6,y:250}).wait(1).to({graphics:mask_graphics_3,x:-498.8,y:250}).wait(1).to({graphics:mask_graphics_4,x:-497.2,y:250}).wait(1).to({graphics:mask_graphics_5,x:-494.6,y:250}).wait(1).to({graphics:mask_graphics_6,x:-490.6,y:250}).wait(1).to({graphics:mask_graphics_7,x:-485.1,y:250}).wait(1).to({graphics:mask_graphics_8,x:-477.8,y:250}).wait(1).to({graphics:mask_graphics_9,x:-468.4,y:250}).wait(1).to({graphics:mask_graphics_10,x:-456.6,y:250}).wait(1).to({graphics:mask_graphics_11,x:-442.3,y:250}).wait(1).to({graphics:mask_graphics_12,x:-425.2,y:250}).wait(1).to({graphics:mask_graphics_13,x:-404.8,y:250}).wait(1).to({graphics:mask_graphics_14,x:-381.2,y:250}).wait(1).to({graphics:mask_graphics_15,x:-353.8,y:250}).wait(1).to({graphics:mask_graphics_16,x:-322.6,y:250}).wait(1).to({graphics:mask_graphics_17,x:-287.3,y:250}).wait(1).to({graphics:mask_graphics_18,x:-247.4,y:250}).wait(1).to({graphics:mask_graphics_19,x:-202.9,y:250}).wait(1).to({graphics:mask_graphics_20,x:-153.6,y:250}).wait(1).to({graphics:mask_graphics_21,x:-99,y:250}).wait(1).to({graphics:mask_graphics_22,x:-38.9,y:250}).wait(1).to({graphics:mask_graphics_23,x:26.9,y:250}).wait(1).to({graphics:mask_graphics_24,x:97.1,y:250}).wait(1).to({graphics:mask_graphics_25,x:162.9,y:250}).wait(1).to({graphics:mask_graphics_26,x:222.9,y:250}).wait(1).to({graphics:mask_graphics_27,x:277.6,y:250}).wait(1).to({graphics:mask_graphics_28,x:326.9,y:250}).wait(1).to({graphics:mask_graphics_29,x:371.4,y:250}).wait(1).to({graphics:mask_graphics_30,x:411.1,y:250}).wait(1).to({graphics:mask_graphics_31,x:446.5,y:250}).wait(1).to({graphics:mask_graphics_32,x:477.8,y:250}).wait(1).to({graphics:mask_graphics_33,x:505.1,y:250}).wait(1).to({graphics:mask_graphics_34,x:528.7,y:250}).wait(1).to({graphics:mask_graphics_35,x:549.1,y:250}).wait(1).to({graphics:mask_graphics_36,x:566.2,y:250}).wait(1).to({graphics:mask_graphics_37,x:580.6,y:250}).wait(1).to({graphics:mask_graphics_38,x:592.3,y:250}).wait(1).to({graphics:mask_graphics_39,x:601.7,y:250}).wait(1).to({graphics:mask_graphics_40,x:609,y:250}).wait(1).to({graphics:mask_graphics_41,x:614.5,y:250}).wait(1).to({graphics:mask_graphics_42,x:618.5,y:250}).wait(1).to({graphics:mask_graphics_43,x:621.1,y:250}).wait(1).to({graphics:mask_graphics_44,x:622.7,y:250}).wait(1).to({graphics:mask_graphics_45,x:623.6,y:250}).wait(1).to({graphics:mask_graphics_46,x:623.8,y:250}).wait(1).to({graphics:mask_graphics_47,x:623.8,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5443E").s().p("Am8Q9QqKhEqZlDQpJkcpBnWQABABAHgEQAHgFACABQKsI1K+EiQIsDmISA0QI1A3IhiVQFvhlEjinQCuhjCtibQCDh1CwjBQDykJDxlHQDRkcDflfQACgDAHgFQAGgFACgDQkNGmj7FJQklGAkrEtQjmDojzCLQkaCjlaBkQnACBmnAAQiPAAiLgPgEgtpgA8IAAAAIAAAAg");
	this.shape.setTransform(628.5,236.5,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,473.1);


(lib.stroke_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOIAnEMAAAhOHMCcRAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOKAnEMAAAhOHMCcVAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOMAnEMAAAhOHMCcZAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOPAnEMAAAhOHMCcfAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOUAnEMAAAhOHMCcpAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOZAnEMAAAhOHMCczAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOgAnEMAAAhOHMCdBAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOpAnEMAAAhOHMCdTAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOzAnEMAAAhOHMCdnAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhO/AnEMAAAhOHMCd/AAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhPNAnEMAAAhOHMCebAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhPdAnEMAAAhOHMCe7AAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhPvAnEMAAAhOHMCffAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhQEAnEMAAAhOHMCgJAAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhQbAnEMAAAhOHMCg3AAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhQ2AnEMAAAhOHMChtAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhRTAnEMAAAhOHMCinAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhRzAnEMAAAhOHMCjnAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhSWAnEMAAAhOHMCktAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhS9AnEMAAAhOHMCl7AAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhTnAnEMAAAhOHMCnPAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhUNAnEMAAAhOHMCobAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhUxAnEMAAAhOHMCpjAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhVRAnEMAAAhOHMCqjAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhVuAnEMAAAhOHMCrdAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhWIAnEMAAAhOHMCsRAAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhWgAnEMAAAhOHMCtBAAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhW1AnEMAAAhOHMCtrAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhXHAnEMAAAhOHMCuPAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhXXAnEMAAAhOHMCuvAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhXlAnEMAAAhOHMCvLAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhXxAnEMAAAhOHMCvjAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhX7AnEMAAAhOHMCv3AAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhYEAnEMAAAhOHMCwJAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhYKAnEMAAAhOHMCwVAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhYQAnEMAAAhOHMCwhAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhYUAnEMAAAhOHMCwpAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhYYAnEMAAAhOHMCwxAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhYaAnEMAAAhOHMCw1AAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhYbAnEMAAAhOHMCw3AAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhYcAnEMAAAhOHMCw5AAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhYdAnEMAAAhOHMCw7AAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhYdAnEMAAAhOHMCw7AAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhYeAnEMAAAhOHMCw9AAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.6,y:250}).wait(1).to({graphics:mask_graphics_3,x:-498.8,y:250}).wait(1).to({graphics:mask_graphics_4,x:-497.3,y:250}).wait(1).to({graphics:mask_graphics_5,x:-494.8,y:250}).wait(1).to({graphics:mask_graphics_6,x:-491.1,y:250}).wait(1).to({graphics:mask_graphics_7,x:-485.9,y:250}).wait(1).to({graphics:mask_graphics_8,x:-478.9,y:250}).wait(1).to({graphics:mask_graphics_9,x:-470.1,y:250}).wait(1).to({graphics:mask_graphics_10,x:-458.9,y:250}).wait(1).to({graphics:mask_graphics_11,x:-445.3,y:250}).wait(1).to({graphics:mask_graphics_12,x:-429.1,y:250}).wait(1).to({graphics:mask_graphics_13,x:-409.8,y:250}).wait(1).to({graphics:mask_graphics_14,x:-387.4,y:250}).wait(1).to({graphics:mask_graphics_15,x:-361.6,y:250}).wait(1).to({graphics:mask_graphics_16,x:-332,y:250}).wait(1).to({graphics:mask_graphics_17,x:-298.5,y:250}).wait(1).to({graphics:mask_graphics_18,x:-260.8,y:250}).wait(1).to({graphics:mask_graphics_19,x:-218.7,y:250}).wait(1).to({graphics:mask_graphics_20,x:-171.9,y:250}).wait(1).to({graphics:mask_graphics_21,x:-120.2,y:250}).wait(1).to({graphics:mask_graphics_22,x:-63.3,y:250}).wait(1).to({graphics:mask_graphics_23,x:-1,y:250}).wait(1).to({graphics:mask_graphics_24,x:65.5,y:250}).wait(1).to({graphics:mask_graphics_25,x:127.8,y:250}).wait(1).to({graphics:mask_graphics_26,x:184.6,y:250}).wait(1).to({graphics:mask_graphics_27,x:236.3,y:250}).wait(1).to({graphics:mask_graphics_28,x:283.1,y:250}).wait(1).to({graphics:mask_graphics_29,x:325.2,y:250}).wait(1).to({graphics:mask_graphics_30,x:362.9,y:250}).wait(1).to({graphics:mask_graphics_31,x:396.5,y:250}).wait(1).to({graphics:mask_graphics_32,x:425.9,y:250}).wait(1).to({graphics:mask_graphics_33,x:451.9,y:250}).wait(1).to({graphics:mask_graphics_34,x:474.2,y:250}).wait(1).to({graphics:mask_graphics_35,x:493.5,y:250}).wait(1).to({graphics:mask_graphics_36,x:509.8,y:250}).wait(1).to({graphics:mask_graphics_37,x:523.4,y:250}).wait(1).to({graphics:mask_graphics_38,x:534.4,y:250}).wait(1).to({graphics:mask_graphics_39,x:543.4,y:250}).wait(1).to({graphics:mask_graphics_40,x:550.3,y:250}).wait(1).to({graphics:mask_graphics_41,x:555.5,y:250}).wait(1).to({graphics:mask_graphics_42,x:559.2,y:250}).wait(1).to({graphics:mask_graphics_43,x:561.7,y:250}).wait(1).to({graphics:mask_graphics_44,x:563.2,y:250}).wait(1).to({graphics:mask_graphics_45,x:564,y:250}).wait(1).to({graphics:mask_graphics_46,x:564.3,y:250}).wait(1).to({graphics:mask_graphics_47,x:564.3,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#84B4DC").s().p("Ego7AOuQAFgGADgBQD0hMGxg1IFYgrQDCgbCNggQFnhQEGi3QBHgxBRg/IgiAbQhgBKhOAzQhhA/hYAoQjzBuk0A0Qh2AUi1AYIkuAnQlsAzjsBGQADgBAFgHgAMrqEQDIhoEuhcIAUgGQn4CfmbEeIgQALQDWiZDDhlgEAozgLuQighLh7grQhbgghVgTQBpAUByAnQB0AnCNBBQgCgBgGAEQgFADgDAAIgBAAgEAg6gOfQicgYiBAEQBwgGBpANQA3AHA7AOIgugIgAaGukQBGgNBRgCQhjAFhoAUIA0gKg");
	this.shape.setTransform(565.3,204.3,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,408.6);


(lib.stroke_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHA0zMAAAhplMCcPAAAMAAABplg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHA0zMAAAhplMCcPAAAMAAABplg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHA0zMAAAhplMCcPAAAMAAABplg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHA0yMAAAhpjMCcPAAAMAAABpjg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOIA0yMAAAhpjMCcRAAAMAAABpjg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOJA0yMAAAhpjMCcTAAAMAAABpjg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOKA0yMAAAhpjMCcVAAAMAAABpjg");
	var mask_graphics_7 = new cjs.Graphics().p("EhONA0yMAAAhpjMCcbAAAMAAABpjg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOQA0yMAAAhpjMCchAAAMAAABpjg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOTA0yMAAAhpjMCcnAAAMAAABpjg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOYA0yMAAAhpjMCcxAAAMAAABpjg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOeA0yMAAAhpjMCc9AAAMAAABpjg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOlA0yMAAAhpjMCdLAAAMAAABpjg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOtA0yMAAAhpjMCdbAAAMAAABpjg");
	var mask_graphics_14 = new cjs.Graphics().p("EhO3A0xMAAAhphMCdvAAAMAAABphg");
	var mask_graphics_15 = new cjs.Graphics().p("EhPCA0xMAAAhphMCeFAAAMAAABphg");
	var mask_graphics_16 = new cjs.Graphics().p("EhPPA0xMAAAhphMCefAAAMAAABphg");
	var mask_graphics_17 = new cjs.Graphics().p("EhPdA0wMAAAhpfMCe7AAAMAAABpfg");
	var mask_graphics_18 = new cjs.Graphics().p("EhPtA0wMAAAhpfMCfbAAAMAAABpfg");
	var mask_graphics_19 = new cjs.Graphics().p("EhQAA0wMAAAhpfMCgBAAAMAAABpfg");
	var mask_graphics_20 = new cjs.Graphics().p("EhQUA0vMAAAhpdMCgpAAAMAAABpdg");
	var mask_graphics_21 = new cjs.Graphics().p("EhQqA0vMAAAhpdMChVAAAMAAABpdg");
	var mask_graphics_22 = new cjs.Graphics().p("EhRCA0uMAAAhpbMCiFAAAMAAABpbg");
	var mask_graphics_23 = new cjs.Graphics().p("EhRdA0tMAAAhpZMCi7AAAMAAABpZg");
	var mask_graphics_24 = new cjs.Graphics().p("EhR6A0tMAAAhpZMCj1AAAMAAABpZg");
	var mask_graphics_25 = new cjs.Graphics().p("EhSUA0sMAAAhpXMCkpAAAMAAABpXg");
	var mask_graphics_26 = new cjs.Graphics().p("EhStA0rMAAAhpVMClbAAAMAAABpVg");
	var mask_graphics_27 = new cjs.Graphics().p("EhTDA0rMAAAhpVMCmHAAAMAAABpVg");
	var mask_graphics_28 = new cjs.Graphics().p("EhTXA0qMAAAhpTMCmvAAAMAAABpTg");
	var mask_graphics_29 = new cjs.Graphics().p("EhTpA0qMAAAhpTMCnTAAAMAAABpTg");
	var mask_graphics_30 = new cjs.Graphics().p("EhT6A0pMAAAhpRMCn1AAAMAAABpRg");
	var mask_graphics_31 = new cjs.Graphics().p("EhUIA0pMAAAhpRMCoRAAAMAAABpRg");
	var mask_graphics_32 = new cjs.Graphics().p("EhUVA0pMAAAhpRMCorAAAMAAABpRg");
	var mask_graphics_33 = new cjs.Graphics().p("EhUgA0oMAAAhpPMCpBAAAMAAABpPg");
	var mask_graphics_34 = new cjs.Graphics().p("EhUpA0oMAAAhpPMCpTAAAMAAABpPg");
	var mask_graphics_35 = new cjs.Graphics().p("EhUyA0oMAAAhpPMCplAAAMAAABpPg");
	var mask_graphics_36 = new cjs.Graphics().p("EhU5A0oMAAAhpPMCpzAAAMAAABpPg");
	var mask_graphics_37 = new cjs.Graphics().p("EhU+A0oMAAAhpPMCp9AAAMAAABpPg");
	var mask_graphics_38 = new cjs.Graphics().p("EhVDA0oMAAAhpPMCqHAAAMAAABpPg");
	var mask_graphics_39 = new cjs.Graphics().p("EhVHA0nMAAAhpOMCqPAAAMAAABpOg");
	var mask_graphics_40 = new cjs.Graphics().p("EhVKA0nMAAAhpNMCqVAAAMAAABpNg");
	var mask_graphics_41 = new cjs.Graphics().p("EhVMA0nMAAAhpNMCqZAAAMAAABpNg");
	var mask_graphics_42 = new cjs.Graphics().p("EhVOA0nMAAAhpNMCqdAAAMAAABpNg");
	var mask_graphics_43 = new cjs.Graphics().p("EhVPA0nMAAAhpNMCqfAAAMAAABpNg");
	var mask_graphics_44 = new cjs.Graphics().p("EhVQA0nMAAAhpNMCqhAAAMAAABpNg");
	var mask_graphics_45 = new cjs.Graphics().p("EhVQA0nMAAAhpNMCqhAAAMAAABpNg");
	var mask_graphics_46 = new cjs.Graphics().p("EhVQA0nMAAAhpNMCqhAAAMAAABpNg");
	var mask_graphics_47 = new cjs.Graphics().p("EhVRA0nMAAAhpNMCqjAAAMAAABpNg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:337.9}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:337.9}).wait(1).to({graphics:mask_graphics_2,x:-499.6,y:337.9}).wait(1).to({graphics:mask_graphics_3,x:-498.8,y:337.8}).wait(1).to({graphics:mask_graphics_4,x:-497.4,y:337.8}).wait(1).to({graphics:mask_graphics_5,x:-495,y:337.8}).wait(1).to({graphics:mask_graphics_6,x:-491.3,y:337.8}).wait(1).to({graphics:mask_graphics_7,x:-486.1,y:337.8}).wait(1).to({graphics:mask_graphics_8,x:-479.3,y:337.8}).wait(1).to({graphics:mask_graphics_9,x:-470.7,y:337.8}).wait(1).to({graphics:mask_graphics_10,x:-459.7,y:337.8}).wait(1).to({graphics:mask_graphics_11,x:-446.4,y:337.8}).wait(1).to({graphics:mask_graphics_12,x:-430.4,y:337.8}).wait(1).to({graphics:mask_graphics_13,x:-411.6,y:337.8}).wait(1).to({graphics:mask_graphics_14,x:-389.6,y:337.7}).wait(1).to({graphics:mask_graphics_15,x:-364.2,y:337.7}).wait(1).to({graphics:mask_graphics_16,x:-335.2,y:337.7}).wait(1).to({graphics:mask_graphics_17,x:-302.4,y:337.6}).wait(1).to({graphics:mask_graphics_18,x:-265.4,y:337.6}).wait(1).to({graphics:mask_graphics_19,x:-224,y:337.6}).wait(1).to({graphics:mask_graphics_20,x:-178.2,y:337.5}).wait(1).to({graphics:mask_graphics_21,x:-127.5,y:337.5}).wait(1).to({graphics:mask_graphics_22,x:-71.7,y:337.4}).wait(1).to({graphics:mask_graphics_23,x:-10.7,y:337.3}).wait(1).to({graphics:mask_graphics_24,x:54.5,y:337.3}).wait(1).to({graphics:mask_graphics_25,x:115.6,y:337.2}).wait(1).to({graphics:mask_graphics_26,x:171.4,y:337.1}).wait(1).to({graphics:mask_graphics_27,x:222.1,y:337.1}).wait(1).to({graphics:mask_graphics_28,x:268,y:337}).wait(1).to({graphics:mask_graphics_29,x:309.3,y:337}).wait(1).to({graphics:mask_graphics_30,x:346.3,y:336.9}).wait(1).to({graphics:mask_graphics_31,x:379.1,y:336.9}).wait(1).to({graphics:mask_graphics_32,x:408.2,y:336.9}).wait(1).to({graphics:mask_graphics_33,x:433.5,y:336.8}).wait(1).to({graphics:mask_graphics_34,x:455.5,y:336.8}).wait(1).to({graphics:mask_graphics_35,x:474.4,y:336.8}).wait(1).to({graphics:mask_graphics_36,x:490.3,y:336.8}).wait(1).to({graphics:mask_graphics_37,x:503.6,y:336.8}).wait(1).to({graphics:mask_graphics_38,x:514.5,y:336.8}).wait(1).to({graphics:mask_graphics_39,x:523.3,y:336.8}).wait(1).to({graphics:mask_graphics_40,x:530.1,y:336.7}).wait(1).to({graphics:mask_graphics_41,x:535.1,y:336.7}).wait(1).to({graphics:mask_graphics_42,x:538.8,y:336.7}).wait(1).to({graphics:mask_graphics_43,x:541.3,y:336.7}).wait(1).to({graphics:mask_graphics_44,x:542.8,y:336.7}).wait(1).to({graphics:mask_graphics_45,x:543.5,y:336.7}).wait(1).to({graphics:mask_graphics_46,x:543.8,y:336.7}).wait(1).to({graphics:mask_graphics_47,x:543.8,y:336.7}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F9885A").s().p("EgkyAL5QB5mQDAllQCpk4C5jtQDXkSD4i7IA5gqQBZhDBfg8QC3h0DqhUQk9B8kcDLQiCBih1BxQmqGakpKKQklKFhLKuQAAADgIADQgIAEAAACQAwmhB3mEgEAnJgN4Qkvjdl+ibQlgiPmRhQQmUhQk0AJQlOAIllBkIgiAKQAsgOAugMQFshgFVgDQFIgCGnBeQFzBTFKCKQFgCTElDPQgBAAgHAGQgGAEgCAAIgBAAgEAnagOCIAAABIAAgBg");
	this.shape.setTransform(542.5,336.8,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,673.6);


(lib.stroke_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// msk (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_1 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_2 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_3 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_4 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_5 = new cjs.Graphics().p("EhOHAnEMAAAhOHMCcPAAAMAAABOHg");
	var mask_graphics_6 = new cjs.Graphics().p("EhOIAnEMAAAhOHMCcRAAAMAAABOHg");
	var mask_graphics_7 = new cjs.Graphics().p("EhOIAnEMAAAhOHMCcRAAAMAAABOHg");
	var mask_graphics_8 = new cjs.Graphics().p("EhOJAnEMAAAhOHMCcTAAAMAAABOHg");
	var mask_graphics_9 = new cjs.Graphics().p("EhOKAnEMAAAhOHMCcVAAAMAAABOHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhOLAnEMAAAhOHMCcXAAAMAAABOHg");
	var mask_graphics_11 = new cjs.Graphics().p("EhOMAnEMAAAhOHMCcZAAAMAAABOHg");
	var mask_graphics_12 = new cjs.Graphics().p("EhOOAnEMAAAhOHMCcdAAAMAAABOHg");
	var mask_graphics_13 = new cjs.Graphics().p("EhOQAnEMAAAhOHMCchAAAMAAABOHg");
	var mask_graphics_14 = new cjs.Graphics().p("EhOSAnEMAAAhOHMCclAAAMAAABOHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhOVAnEMAAAhOHMCcrAAAMAAABOHg");
	var mask_graphics_16 = new cjs.Graphics().p("EhOYAnEMAAAhOHMCcxAAAMAAABOHg");
	var mask_graphics_17 = new cjs.Graphics().p("EhObAnEMAAAhOHMCc3AAAMAAABOHg");
	var mask_graphics_18 = new cjs.Graphics().p("EhOfAnEMAAAhOHMCc/AAAMAAABOHg");
	var mask_graphics_19 = new cjs.Graphics().p("EhOjAnEMAAAhOHMCdHAAAMAAABOHg");
	var mask_graphics_20 = new cjs.Graphics().p("EhOoAnEMAAAhOHMCdRAAAMAAABOHg");
	var mask_graphics_21 = new cjs.Graphics().p("EhOtAnEMAAAhOHMCdbAAAMAAABOHg");
	var mask_graphics_22 = new cjs.Graphics().p("EhOzAnEMAAAhOHMCdnAAAMAAABOHg");
	var mask_graphics_23 = new cjs.Graphics().p("EhO5AnEMAAAhOHMCdzAAAMAAABOHg");
	var mask_graphics_24 = new cjs.Graphics().p("EhPAAnEMAAAhOHMCeBAAAMAAABOHg");
	var mask_graphics_25 = new cjs.Graphics().p("EhPGAnEMAAAhOHMCeNAAAMAAABOHg");
	var mask_graphics_26 = new cjs.Graphics().p("EhPMAnEMAAAhOHMCeZAAAMAAABOHg");
	var mask_graphics_27 = new cjs.Graphics().p("EhPRAnEMAAAhOHMCejAAAMAAABOHg");
	var mask_graphics_28 = new cjs.Graphics().p("EhPWAnEMAAAhOHMCetAAAMAAABOHg");
	var mask_graphics_29 = new cjs.Graphics().p("EhPaAnEMAAAhOHMCe1AAAMAAABOHg");
	var mask_graphics_30 = new cjs.Graphics().p("EhPeAnEMAAAhOHMCe9AAAMAAABOHg");
	var mask_graphics_31 = new cjs.Graphics().p("EhPhAnEMAAAhOHMCfDAAAMAAABOHg");
	var mask_graphics_32 = new cjs.Graphics().p("EhPkAnEMAAAhOHMCfJAAAMAAABOHg");
	var mask_graphics_33 = new cjs.Graphics().p("EhPnAnEMAAAhOHMCfPAAAMAAABOHg");
	var mask_graphics_34 = new cjs.Graphics().p("EhPpAnEMAAAhOHMCfTAAAMAAABOHg");
	var mask_graphics_35 = new cjs.Graphics().p("EhPrAnEMAAAhOHMCfXAAAMAAABOHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhPtAnEMAAAhOHMCfbAAAMAAABOHg");
	var mask_graphics_37 = new cjs.Graphics().p("EhPuAnEMAAAhOHMCfdAAAMAAABOHg");
	var mask_graphics_38 = new cjs.Graphics().p("EhPvAnEMAAAhOHMCffAAAMAAABOHg");
	var mask_graphics_39 = new cjs.Graphics().p("EhPwAnEMAAAhOHMCfhAAAMAAABOHg");
	var mask_graphics_40 = new cjs.Graphics().p("EhPxAnEMAAAhOHMCfjAAAMAAABOHg");
	var mask_graphics_41 = new cjs.Graphics().p("EhPxAnEMAAAhOHMCfjAAAMAAABOHg");
	var mask_graphics_42 = new cjs.Graphics().p("EhPyAnEMAAAhOHMCflAAAMAAABOHg");
	var mask_graphics_43 = new cjs.Graphics().p("EhPyAnEMAAAhOHMCflAAAMAAABOHg");
	var mask_graphics_44 = new cjs.Graphics().p("EhPyAnEMAAAhOHMCflAAAMAAABOHg");
	var mask_graphics_45 = new cjs.Graphics().p("EhPyAnEMAAAhOHMCflAAAMAAABOHg");
	var mask_graphics_46 = new cjs.Graphics().p("EhPyAnEMAAAhOHMCflAAAMAAABOHg");
	var mask_graphics_47 = new cjs.Graphics().p("EhPzAnEMAAAhOHMCfnAAAMAAABOHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-500,y:250}).wait(1).to({graphics:mask_graphics_1,x:-499.9,y:250}).wait(1).to({graphics:mask_graphics_2,x:-499.6,y:250}).wait(1).to({graphics:mask_graphics_3,x:-498.9,y:250}).wait(1).to({graphics:mask_graphics_4,x:-497.5,y:250}).wait(1).to({graphics:mask_graphics_5,x:-495.1,y:250}).wait(1).to({graphics:mask_graphics_6,x:-491.5,y:250}).wait(1).to({graphics:mask_graphics_7,x:-486.6,y:250}).wait(1).to({graphics:mask_graphics_8,x:-480,y:250}).wait(1).to({graphics:mask_graphics_9,x:-471.5,y:250}).wait(1).to({graphics:mask_graphics_10,x:-461,y:250}).wait(1).to({graphics:mask_graphics_11,x:-448.1,y:250}).wait(1).to({graphics:mask_graphics_12,x:-432.7,y:250}).wait(1).to({graphics:mask_graphics_13,x:-414.4,y:250}).wait(1).to({graphics:mask_graphics_14,x:-393.1,y:250}).wait(1).to({graphics:mask_graphics_15,x:-368.5,y:250}).wait(1).to({graphics:mask_graphics_16,x:-340.4,y:250}).wait(1).to({graphics:mask_graphics_17,x:-308.7,y:250}).wait(1).to({graphics:mask_graphics_18,x:-272.8,y:250}).wait(1).to({graphics:mask_graphics_19,x:-232.9,y:250}).wait(1).to({graphics:mask_graphics_20,x:-188.4,y:250}).wait(1).to({graphics:mask_graphics_21,x:-139.3,y:250}).wait(1).to({graphics:mask_graphics_22,x:-85.3,y:250}).wait(1).to({graphics:mask_graphics_23,x:-26.1,y:250}).wait(1).to({graphics:mask_graphics_24,x:37,y:250}).wait(1).to({graphics:mask_graphics_25,x:96.1,y:250}).wait(1).to({graphics:mask_graphics_26,x:150.2,y:250}).wait(1).to({graphics:mask_graphics_27,x:199.2,y:250}).wait(1).to({graphics:mask_graphics_28,x:243.7,y:250}).wait(1).to({graphics:mask_graphics_29,x:283.7,y:250}).wait(1).to({graphics:mask_graphics_30,x:319.5,y:250}).wait(1).to({graphics:mask_graphics_31,x:351.3,y:250}).wait(1).to({graphics:mask_graphics_32,x:379.4,y:250}).wait(1).to({graphics:mask_graphics_33,x:403.9,y:250}).wait(1).to({graphics:mask_graphics_34,x:425.3,y:250}).wait(1).to({graphics:mask_graphics_35,x:443.5,y:250}).wait(1).to({graphics:mask_graphics_36,x:459,y:250}).wait(1).to({graphics:mask_graphics_37,x:471.8,y:250}).wait(1).to({graphics:mask_graphics_38,x:482.5,y:250}).wait(1).to({graphics:mask_graphics_39,x:490.8,y:250}).wait(1).to({graphics:mask_graphics_40,x:497.4,y:250}).wait(1).to({graphics:mask_graphics_41,x:502.4,y:250}).wait(1).to({graphics:mask_graphics_42,x:506,y:250}).wait(1).to({graphics:mask_graphics_43,x:508.4,y:250}).wait(1).to({graphics:mask_graphics_44,x:509.7,y:250}).wait(1).to({graphics:mask_graphics_45,x:510.6,y:250}).wait(1).to({graphics:mask_graphics_46,x:510.7,y:250}).wait(1).to({graphics:mask_graphics_47,x:510.8,y:250}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FD9982").s().p("EAkxAIXQjejRj+isQj9ipkSh+QnPjUnwhjQoPhqnaApQm8AnmmB7Qn+CWluEsIgJADQgGABgCACQCaibDzh1QC2hYEYhWQGgh/G8guQHcgxHrBgQJaB2HsDlQIdD8GiGKIgIAHQgIAGgBAAIAAAAg");
	this.shape.setTransform(509.8,115.1,2.151,2.151);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,230.3);


(lib.Strokes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// stroke_6
	this.instance = new lib.stroke_6("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(1580.4,135.5,1,1,0,0,0,288.5,135.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off:false},0).wait(71));

	// stroke_5
	this.instance_1 = new lib.stroke_5("synched",0,false);
	this.instance_1.parent = this;
	this.instance_1.setTransform(1678,168.9,1,1,0,0,0,213.7,150.9);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15).to({_off:false},0).wait(64));

	// stroke_7
	this.instance_2 = new lib.stroke_7("synched",0,false);
	this.instance_2.parent = this;
	this.instance_2.setTransform(1701.3,363.8,1,1,0,0,0,116.2,141.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(19).to({_off:false},0).wait(60));

	// stroke_3
	this.instance_3 = new lib.stroke_3("synched",0,false);
	this.instance_3.parent = this;
	this.instance_3.setTransform(1302,644.2,1,1,0,0,0,628.5,236.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(17).to({_off:false},0).wait(62));

	// stroke_0
	this.instance_4 = new lib.stroke_0("synched",0,false);
	this.instance_4.parent = this;
	this.instance_4.setTransform(1144.5,402.5,1,1,0,0,0,509.8,115.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(13).to({_off:false},0).wait(66));

	// stroke_1
	this.instance_5 = new lib.stroke_1("synched",0,false);
	this.instance_5.parent = this;
	this.instance_5.setTransform(807.4,751.2,1,1,0,0,0,542.5,336.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(79));

	// stroke_2
	this.instance_6 = new lib.stroke_2("synched",0,false);
	this.instance_6.parent = this;
	this.instance_6.setTransform(565.2,777.2,1,1,0,0,0,565.2,204.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(3).to({_off:false},0).wait(76));

	// stroke_4
	this.instance_7 = new lib.stroke_4("synched",0,false);
	this.instance_7.parent = this;
	this.instance_7.setTransform(405.3,1040.7,1,1,0,0,0,386.2,272.4);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(5).to({_off:false},0).wait(74));

	// stroke_8
	this.instance_8 = new lib.stroke_8("synched",0,false);
	this.instance_8.parent = this;
	this.instance_8.setTransform(1482.6,485.4,1,1,0,0,0,449.6,224.1);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(9).to({_off:false},0).wait(70));

	// stroke_9
	this.instance_9 = new lib.stroke_9("synched",0,false);
	this.instance_9.parent = this;
	this.instance_9.setTransform(366.1,799.6,1,1,0,0,0,328.8,296.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(79));

	// stroke_10
	this.instance_10 = new lib.stroke_10("synched",0,false);
	this.instance_10.parent = this;
	this.instance_10.setTransform(1042.8,908.5,1,1,0,0,0,310,88);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(6).to({_off:false},0).wait(73));

	// stroke_11
	this.instance_11 = new lib.stroke_11("synched",0,false);
	this.instance_11.parent = this;
	this.instance_11.setTransform(533.4,1153.8,1,1,0,0,0,345.1,94.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(79));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(37.3,414.4,227.7,834.1);


(lib.mov_yellow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask KFramed (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EiW1gdEMEHGg9uMAmlCkaMkHGA9vg");
	var mask_graphics_1 = new cjs.Graphics().p("EiWjgdBMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_2 = new cjs.Graphics().p("EiWjgdBMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_3 = new cjs.Graphics().p("EiWjgdBMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_4 = new cjs.Graphics().p("EiWjgdCMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_5 = new cjs.Graphics().p("EiWjgdDMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_6 = new cjs.Graphics().p("EiWjgdEMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_7 = new cjs.Graphics().p("EiWjgdHMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_8 = new cjs.Graphics().p("EiWjgdJMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_9 = new cjs.Graphics().p("EiWjgdNMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_10 = new cjs.Graphics().p("EiWjgdSMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_11 = new cjs.Graphics().p("EiWjgdYMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_12 = new cjs.Graphics().p("EiWjgdfMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_13 = new cjs.Graphics().p("EiWjgdnMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_14 = new cjs.Graphics().p("EiWjgdwMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_15 = new cjs.Graphics().p("EiWjgd7MEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_16 = new cjs.Graphics().p("EiWjgeIMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_17 = new cjs.Graphics().p("EiWjgeWMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_18 = new cjs.Graphics().p("EiWjgemMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_19 = new cjs.Graphics().p("EiWjge4MEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_20 = new cjs.Graphics().p("EiWjgfLMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_21 = new cjs.Graphics().p("EiWjgfhMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_22 = new cjs.Graphics().p("EiWjgf5MEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_23 = new cjs.Graphics().p("EiWjggUMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_24 = new cjs.Graphics().p("EiWjggwMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_25 = new cjs.Graphics().p("EiWjghQMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_26 = new cjs.Graphics().p("EiWjghyMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_27 = new cjs.Graphics().p("EiWjgiWMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_28 = new cjs.Graphics().p("EiWjgi9MEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_29 = new cjs.Graphics().p("EiWjgjoMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_30 = new cjs.Graphics().p("EiWjgkVMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_31 = new cjs.Graphics().p("EiWjglGMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_32 = new cjs.Graphics().p("EiWjgl5MEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_33 = new cjs.Graphics().p("EiWjgmwMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_34 = new cjs.Graphics().p("EiWjgnrMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_35 = new cjs.Graphics().p("EiWjgopMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_36 = new cjs.Graphics().p("EiWjgpqMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_37 = new cjs.Graphics().p("EiWjgqvMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_38 = new cjs.Graphics().p("EiWjgr5MEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_39 = new cjs.Graphics().p("EiWjgtFMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_40 = new cjs.Graphics().p("EiWjguOMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_41 = new cjs.Graphics().p("EiWjgvTMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_42 = new cjs.Graphics().p("EiWjgwVMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_43 = new cjs.Graphics().p("EiWjgxTMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_44 = new cjs.Graphics().p("EiWjgyNMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_45 = new cjs.Graphics().p("EiWjgzEMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_46 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_47 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_48 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_49 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_50 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_51 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_52 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_53 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_54 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_55 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_56 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_57 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_58 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_59 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_60 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_61 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_62 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_63 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_64 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_65 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_66 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_67 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_68 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_69 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_70 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_71 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_72 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_73 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_74 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_75 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_76 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_77 = new cjs.Graphics().p("EiW1gzWMEHGg9uMAmlCkbMkHGA9ug");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-747.9,y:866.3}).wait(1).to({graphics:mask_graphics_1,x:-749.4,y:866.3}).wait(1).to({graphics:mask_graphics_2,x:-749.3,y:866.3}).wait(1).to({graphics:mask_graphics_3,x:-749,y:866.2}).wait(1).to({graphics:mask_graphics_4,x:-748.5,y:866.2}).wait(1).to({graphics:mask_graphics_5,x:-747.6,y:866}).wait(1).to({graphics:mask_graphics_6,x:-746.2,y:865.9}).wait(1).to({graphics:mask_graphics_7,x:-744.4,y:865.7}).wait(1).to({graphics:mask_graphics_8,x:-741.8,y:865.4}).wait(1).to({graphics:mask_graphics_9,x:-738.7,y:865}).wait(1).to({graphics:mask_graphics_10,x:-734.7,y:864.5}).wait(1).to({graphics:mask_graphics_11,x:-729.8,y:864}).wait(1).to({graphics:mask_graphics_12,x:-724,y:863.3}).wait(1).to({graphics:mask_graphics_13,x:-717,y:862.5}).wait(1).to({graphics:mask_graphics_14,x:-709,y:861.5}).wait(1).to({graphics:mask_graphics_15,x:-699.7,y:860.4}).wait(1).to({graphics:mask_graphics_16,x:-689.1,y:859.2}).wait(1).to({graphics:mask_graphics_17,x:-677,y:857.7}).wait(1).to({graphics:mask_graphics_18,x:-663.5,y:856.1}).wait(1).to({graphics:mask_graphics_19,x:-648.4,y:854.4}).wait(1).to({graphics:mask_graphics_20,x:-631.6,y:852.4}).wait(1).to({graphics:mask_graphics_21,x:-613,y:850.2}).wait(1).to({graphics:mask_graphics_22,x:-592.6,y:847.8}).wait(1).to({graphics:mask_graphics_23,x:-570.2,y:845.2}).wait(1).to({graphics:mask_graphics_24,x:-545.9,y:842.3}).wait(1).to({graphics:mask_graphics_25,x:-519.3,y:839.2}).wait(1).to({graphics:mask_graphics_26,x:-490.6,y:835.8}).wait(1).to({graphics:mask_graphics_27,x:-459.6,y:832.1}).wait(1).to({graphics:mask_graphics_28,x:-426.2,y:828.2}).wait(1).to({graphics:mask_graphics_29,x:-390.3,y:823.9}).wait(1).to({graphics:mask_graphics_30,x:-351.9,y:819.4}).wait(1).to({graphics:mask_graphics_31,x:-310.8,y:814.6}).wait(1).to({graphics:mask_graphics_32,x:-266.9,y:809.4}).wait(1).to({graphics:mask_graphics_33,x:-220.3,y:803.9}).wait(1).to({graphics:mask_graphics_34,x:-170.7,y:798.1}).wait(1).to({graphics:mask_graphics_35,x:-118.1,y:791.9}).wait(1).to({graphics:mask_graphics_36,x:-62.5,y:785.3}).wait(1).to({graphics:mask_graphics_37,x:-3.6,y:778.4}).wait(1).to({graphics:mask_graphics_38,x:58.5,y:771.1}).wait(1).to({graphics:mask_graphics_39,x:123.1,y:763.4}).wait(1).to({graphics:mask_graphics_40,x:185.2,y:756.1}).wait(1).to({graphics:mask_graphics_41,x:244.1,y:749.2}).wait(1).to({graphics:mask_graphics_42,x:299.7,y:742.6}).wait(1).to({graphics:mask_graphics_43,x:352.3,y:736.4}).wait(1).to({graphics:mask_graphics_44,x:401.9,y:730.6}).wait(1).to({graphics:mask_graphics_45,x:448.6,y:725.1}).wait(1).to({graphics:mask_graphics_46,x:492.4,y:718.5}).wait(1).to({graphics:mask_graphics_47,x:533.5,y:708.8}).wait(1).to({graphics:mask_graphics_48,x:571.9,y:699.8}).wait(1).to({graphics:mask_graphics_49,x:607.8,y:691.3}).wait(1).to({graphics:mask_graphics_50,x:641.2,y:683.4}).wait(1).to({graphics:mask_graphics_51,x:672.2,y:676.1}).wait(1).to({graphics:mask_graphics_52,x:701,y:669.3}).wait(1).to({graphics:mask_graphics_53,x:727.5,y:663.1}).wait(1).to({graphics:mask_graphics_54,x:751.9,y:657.3}).wait(1).to({graphics:mask_graphics_55,x:774.2,y:652.1}).wait(1).to({graphics:mask_graphics_56,x:794.7,y:647.3}).wait(1).to({graphics:mask_graphics_57,x:813.2,y:642.9}).wait(1).to({graphics:mask_graphics_58,x:830,y:638.9}).wait(1).to({graphics:mask_graphics_59,x:845.2,y:635.4}).wait(1).to({graphics:mask_graphics_60,x:858.7,y:632.2}).wait(1).to({graphics:mask_graphics_61,x:870.7,y:629.3}).wait(1).to({graphics:mask_graphics_62,x:881.3,y:626.8}).wait(1).to({graphics:mask_graphics_63,x:890.6,y:624.6}).wait(1).to({graphics:mask_graphics_64,x:898.7,y:622.8}).wait(1).to({graphics:mask_graphics_65,x:905.6,y:621.1}).wait(1).to({graphics:mask_graphics_66,x:911.4,y:619.7}).wait(1).to({graphics:mask_graphics_67,x:916.3,y:618.6}).wait(1).to({graphics:mask_graphics_68,x:920.3,y:617.7}).wait(1).to({graphics:mask_graphics_69,x:923.5,y:616.9}).wait(1).to({graphics:mask_graphics_70,x:926,y:616.3}).wait(1).to({graphics:mask_graphics_71,x:927.8,y:615.9}).wait(1).to({graphics:mask_graphics_72,x:929.2,y:615.6}).wait(1).to({graphics:mask_graphics_73,x:930.1,y:615.3}).wait(1).to({graphics:mask_graphics_74,x:930.6,y:615.2}).wait(1).to({graphics:mask_graphics_75,x:930.9,y:615.2}).wait(1).to({graphics:mask_graphics_76,x:931,y:615.1}).wait(1).to({graphics:mask_graphics_77,x:932.5,y:612.8}).wait(1));

	// Layer_1
	this.instance = new lib.th_yellow();
	this.instance.parent = this;
	this.instance.setTransform(893.1,628.1,1,1,0,0,0,893.1,628.1);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(78));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,285.2,217.6,971.2);


(lib.mov_red = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask KFramed (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EisvgzWMEHGg9uMAmlCkbMkHGA9ug");
	var mask_graphics_1 = new cjs.Graphics().p("EiswgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_2 = new cjs.Graphics().p("EiswgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_3 = new cjs.Graphics().p("EisugzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_4 = new cjs.Graphics().p("EissgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_5 = new cjs.Graphics().p("EisngzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_6 = new cjs.Graphics().p("EisggzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_7 = new cjs.Graphics().p("EisXgzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_8 = new cjs.Graphics().p("EisLgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_9 = new cjs.Graphics().p("Eir7gzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_10 = new cjs.Graphics().p("EirngzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_11 = new cjs.Graphics().p("EirOgzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_12 = new cjs.Graphics().p("EiqxgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_13 = new cjs.Graphics().p("EiqOgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_14 = new cjs.Graphics().p("EipmgzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_15 = new cjs.Graphics().p("Eio4gzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_16 = new cjs.Graphics().p("EioDgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_17 = new cjs.Graphics().p("EinHgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_18 = new cjs.Graphics().p("EimDgzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_19 = new cjs.Graphics().p("Eik4gzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_20 = new cjs.Graphics().p("EijjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_21 = new cjs.Graphics().p("EiiHgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_22 = new cjs.Graphics().p("EighgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_23 = new cjs.Graphics().p("EiexgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_24 = new cjs.Graphics().p("Eic3gzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_25 = new cjs.Graphics().p("EiaygzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_26 = new cjs.Graphics().p("EiYigzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_27 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_28 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_29 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_30 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_31 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_32 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_33 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_34 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_35 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_36 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_37 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_38 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_39 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_40 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_41 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_42 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_43 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_44 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_45 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_46 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_47 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_48 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_49 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_50 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_51 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_52 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_53 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_54 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_55 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_56 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_57 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_58 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_59 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_60 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_61 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_62 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_63 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_64 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_65 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_66 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_67 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_68 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_69 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_70 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_71 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_72 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_73 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_74 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_75 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_76 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_77 = new cjs.Graphics().p("EiW1gzWMEHGg9uMAmlCkbMkHGA9ug");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-1105.6,y:702.5}).wait(1).to({graphics:mask_graphics_1,x:-1105.7,y:704.8}).wait(1).to({graphics:mask_graphics_2,x:-1105.7,y:704.8}).wait(1).to({graphics:mask_graphics_3,x:-1105.5,y:704.7}).wait(1).to({graphics:mask_graphics_4,x:-1105.3,y:704.6}).wait(1).to({graphics:mask_graphics_5,x:-1104.8,y:704.3}).wait(1).to({graphics:mask_graphics_6,x:-1104.1,y:704}).wait(1).to({graphics:mask_graphics_7,x:-1103.2,y:703.6}).wait(1).to({graphics:mask_graphics_8,x:-1101.9,y:703}).wait(1).to({graphics:mask_graphics_9,x:-1100.4,y:702.2}).wait(1).to({graphics:mask_graphics_10,x:-1098.4,y:701.3}).wait(1).to({graphics:mask_graphics_11,x:-1095.9,y:700.2}).wait(1).to({graphics:mask_graphics_12,x:-1093,y:698.8}).wait(1).to({graphics:mask_graphics_13,x:-1089.5,y:697.2}).wait(1).to({graphics:mask_graphics_14,x:-1085.5,y:695.3}).wait(1).to({graphics:mask_graphics_15,x:-1080.9,y:693.1}).wait(1).to({graphics:mask_graphics_16,x:-1075.6,y:690.6}).wait(1).to({graphics:mask_graphics_17,x:-1069.5,y:687.7}).wait(1).to({graphics:mask_graphics_18,x:-1062.8,y:684.5}).wait(1).to({graphics:mask_graphics_19,x:-1055.2,y:681}).wait(1).to({graphics:mask_graphics_20,x:-1046.8,y:677}).wait(1).to({graphics:mask_graphics_21,x:-1037.5,y:672.6}).wait(1).to({graphics:mask_graphics_22,x:-1027.3,y:667.8}).wait(1).to({graphics:mask_graphics_23,x:-1016.1,y:662.6}).wait(1).to({graphics:mask_graphics_24,x:-1004,y:656.8}).wait(1).to({graphics:mask_graphics_25,x:-990.7,y:650.6}).wait(1).to({graphics:mask_graphics_26,x:-976.3,y:643.8}).wait(1).to({graphics:mask_graphics_27,x:-958,y:636.5}).wait(1).to({graphics:mask_graphics_28,x:-924.6,y:628.6}).wait(1).to({graphics:mask_graphics_29,x:-888.8,y:620.1}).wait(1).to({graphics:mask_graphics_30,x:-850.3,y:611.1}).wait(1).to({graphics:mask_graphics_31,x:-809.2,y:601.4}).wait(1).to({graphics:mask_graphics_32,x:-765.4,y:591.1}).wait(1).to({graphics:mask_graphics_33,x:-718.7,y:580.1}).wait(1).to({graphics:mask_graphics_34,x:-669.2,y:568.4}).wait(1).to({graphics:mask_graphics_35,x:-616.6,y:556}).wait(1).to({graphics:mask_graphics_36,x:-560.9,y:542.9}).wait(1).to({graphics:mask_graphics_37,x:-502.1,y:529}).wait(1).to({graphics:mask_graphics_38,x:-440,y:514.4}).wait(1).to({graphics:mask_graphics_39,x:-375.3,y:499.1}).wait(1).to({graphics:mask_graphics_40,x:-313.2,y:484.5}).wait(1).to({graphics:mask_graphics_41,x:-254.4,y:470.6}).wait(1).to({graphics:mask_graphics_42,x:-198.7,y:457.5}).wait(1).to({graphics:mask_graphics_43,x:-146.1,y:445.1}).wait(1).to({graphics:mask_graphics_44,x:-96.5,y:433.4}).wait(1).to({graphics:mask_graphics_45,x:-49.9,y:422.4}).wait(1).to({graphics:mask_graphics_46,x:-6.1,y:412.1}).wait(1).to({graphics:mask_graphics_47,x:35,y:402.4}).wait(1).to({graphics:mask_graphics_48,x:73.5,y:393.4}).wait(1).to({graphics:mask_graphics_49,x:109.4,y:384.9}).wait(1).to({graphics:mask_graphics_50,x:142.8,y:377}).wait(1).to({graphics:mask_graphics_51,x:173.8,y:369.7}).wait(1).to({graphics:mask_graphics_52,x:202.5,y:362.9}).wait(1).to({graphics:mask_graphics_53,x:229,y:356.7}).wait(1).to({graphics:mask_graphics_54,x:253.4,y:350.9}).wait(1).to({graphics:mask_graphics_55,x:275.8,y:345.7}).wait(1).to({graphics:mask_graphics_56,x:296.2,y:340.9}).wait(1).to({graphics:mask_graphics_57,x:314.8,y:336.5}).wait(1).to({graphics:mask_graphics_58,x:331.6,y:332.5}).wait(1).to({graphics:mask_graphics_59,x:346.7,y:329}).wait(1).to({graphics:mask_graphics_60,x:360.2,y:325.8}).wait(1).to({graphics:mask_graphics_61,x:372.3,y:323}).wait(1).to({graphics:mask_graphics_62,x:382.9,y:320.5}).wait(1).to({graphics:mask_graphics_63,x:392.2,y:318.3}).wait(1).to({graphics:mask_graphics_64,x:400.2,y:316.4}).wait(1).to({graphics:mask_graphics_65,x:407.1,y:314.8}).wait(1).to({graphics:mask_graphics_66,x:413,y:313.4}).wait(1).to({graphics:mask_graphics_67,x:417.8,y:312.3}).wait(1).to({graphics:mask_graphics_68,x:421.8,y:311.3}).wait(1).to({graphics:mask_graphics_69,x:425,y:310.6}).wait(1).to({graphics:mask_graphics_70,x:427.5,y:310}).wait(1).to({graphics:mask_graphics_71,x:429.4,y:309.5}).wait(1).to({graphics:mask_graphics_72,x:430.7,y:309.2}).wait(1).to({graphics:mask_graphics_73,x:431.6,y:309}).wait(1).to({graphics:mask_graphics_74,x:432.2,y:308.9}).wait(1).to({graphics:mask_graphics_75,x:432.4,y:308.8}).wait(1).to({graphics:mask_graphics_76,x:432.5,y:308.8}).wait(1).to({graphics:mask_graphics_77,x:434.6,y:306.5}).wait(1));

	// Layer_1
	this.instance = new lib.th_red();
	this.instance.parent = this;
	this.instance.setTransform(705.7,224.1,1,1,0,0,0,705.7,224.1);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(78));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.mov_orange = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask KFramed (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EiW1gumMEHGg9vMAmlCkbMkHGA9vg");
	var mask_graphics_1 = new cjs.Graphics().p("EiWjgumMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_2 = new cjs.Graphics().p("EiWjgumMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_3 = new cjs.Graphics().p("EiWjgumMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_4 = new cjs.Graphics().p("EiWjgunMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_5 = new cjs.Graphics().p("EiWjguoMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_6 = new cjs.Graphics().p("EiWjgupMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_7 = new cjs.Graphics().p("EiWjgusMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_8 = new cjs.Graphics().p("EiWjguuMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_9 = new cjs.Graphics().p("EiWjguyMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_10 = new cjs.Graphics().p("EiWjgu3MEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_11 = new cjs.Graphics().p("EiWjgu9MEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_12 = new cjs.Graphics().p("EiWjgvEMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_13 = new cjs.Graphics().p("EiWjgvMMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_14 = new cjs.Graphics().p("EiWjgvVMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_15 = new cjs.Graphics().p("EiWjgvgMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_16 = new cjs.Graphics().p("EiWjgvtMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_17 = new cjs.Graphics().p("EiWjgv7MEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_18 = new cjs.Graphics().p("EiWjgwLMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_19 = new cjs.Graphics().p("EiWjgwdMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_20 = new cjs.Graphics().p("EiWjgwwMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_21 = new cjs.Graphics().p("EiWjgxGMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_22 = new cjs.Graphics().p("EiWjgxeMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_23 = new cjs.Graphics().p("EiWjgx5MEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_24 = new cjs.Graphics().p("EiWjgyVMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_25 = new cjs.Graphics().p("EiWjgy1MEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_26 = new cjs.Graphics().p("EiWjgzXMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_27 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_28 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_29 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_30 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_31 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_32 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_33 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_34 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_35 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_36 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_37 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_38 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_39 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_40 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_41 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_42 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_43 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_44 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_45 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_46 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_47 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_48 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_49 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_50 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_51 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_52 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_53 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_54 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_55 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_56 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_57 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_58 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_59 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_60 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_61 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_62 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_63 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_64 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_65 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_66 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_67 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_68 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_69 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_70 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_71 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_72 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_73 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_74 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_75 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_76 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_77 = new cjs.Graphics().p("EiW1gzWMEHGg9uMAmlCkbMkHGA9ug");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-853.9,y:754.1}).wait(1).to({graphics:mask_graphics_1,x:-855.6,y:753.8}).wait(1).to({graphics:mask_graphics_2,x:-855.5,y:753.8}).wait(1).to({graphics:mask_graphics_3,x:-855.2,y:753.7}).wait(1).to({graphics:mask_graphics_4,x:-854.7,y:753.7}).wait(1).to({graphics:mask_graphics_5,x:-853.8,y:753.5}).wait(1).to({graphics:mask_graphics_6,x:-852.4,y:753.4}).wait(1).to({graphics:mask_graphics_7,x:-850.6,y:753.2}).wait(1).to({graphics:mask_graphics_8,x:-848.1,y:752.9}).wait(1).to({graphics:mask_graphics_9,x:-844.9,y:752.5}).wait(1).to({graphics:mask_graphics_10,x:-840.9,y:752}).wait(1).to({graphics:mask_graphics_11,x:-836,y:751.5}).wait(1).to({graphics:mask_graphics_12,x:-830.2,y:750.8}).wait(1).to({graphics:mask_graphics_13,x:-823.3,y:750}).wait(1).to({graphics:mask_graphics_14,x:-815.2,y:749}).wait(1).to({graphics:mask_graphics_15,x:-805.9,y:747.9}).wait(1).to({graphics:mask_graphics_16,x:-795.3,y:746.7}).wait(1).to({graphics:mask_graphics_17,x:-783.3,y:745.2}).wait(1).to({graphics:mask_graphics_18,x:-769.8,y:743.6}).wait(1).to({graphics:mask_graphics_19,x:-754.7,y:741.9}).wait(1).to({graphics:mask_graphics_20,x:-737.9,y:739.9}).wait(1).to({graphics:mask_graphics_21,x:-719.3,y:737.7}).wait(1).to({graphics:mask_graphics_22,x:-698.9,y:735.3}).wait(1).to({graphics:mask_graphics_23,x:-676.5,y:732.7}).wait(1).to({graphics:mask_graphics_24,x:-652.1,y:729.8}).wait(1).to({graphics:mask_graphics_25,x:-625.6,y:726.7}).wait(1).to({graphics:mask_graphics_26,x:-596.8,y:723.3}).wait(1).to({graphics:mask_graphics_27,x:-565.8,y:717.9}).wait(1).to({graphics:mask_graphics_28,x:-532.4,y:710}).wait(1).to({graphics:mask_graphics_29,x:-496.6,y:701.5}).wait(1).to({graphics:mask_graphics_30,x:-458.1,y:692.5}).wait(1).to({graphics:mask_graphics_31,x:-417,y:682.8}).wait(1).to({graphics:mask_graphics_32,x:-373.2,y:672.5}).wait(1).to({graphics:mask_graphics_33,x:-326.5,y:661.5}).wait(1).to({graphics:mask_graphics_34,x:-277,y:649.8}).wait(1).to({graphics:mask_graphics_35,x:-224.4,y:637.4}).wait(1).to({graphics:mask_graphics_36,x:-168.7,y:624.3}).wait(1).to({graphics:mask_graphics_37,x:-109.9,y:610.4}).wait(1).to({graphics:mask_graphics_38,x:-47.8,y:595.8}).wait(1).to({graphics:mask_graphics_39,x:16.9,y:580.5}).wait(1).to({graphics:mask_graphics_40,x:79,y:565.9}).wait(1).to({graphics:mask_graphics_41,x:137.8,y:552}).wait(1).to({graphics:mask_graphics_42,x:193.5,y:538.9}).wait(1).to({graphics:mask_graphics_43,x:246.1,y:526.5}).wait(1).to({graphics:mask_graphics_44,x:295.7,y:514.8}).wait(1).to({graphics:mask_graphics_45,x:342.3,y:503.8}).wait(1).to({graphics:mask_graphics_46,x:386.1,y:493.5}).wait(1).to({graphics:mask_graphics_47,x:427.2,y:483.8}).wait(1).to({graphics:mask_graphics_48,x:465.7,y:474.8}).wait(1).to({graphics:mask_graphics_49,x:501.6,y:466.3}).wait(1).to({graphics:mask_graphics_50,x:535,y:458.4}).wait(1).to({graphics:mask_graphics_51,x:566,y:451.1}).wait(1).to({graphics:mask_graphics_52,x:594.7,y:444.3}).wait(1).to({graphics:mask_graphics_53,x:621.2,y:438.1}).wait(1).to({graphics:mask_graphics_54,x:645.6,y:432.3}).wait(1).to({graphics:mask_graphics_55,x:668,y:427.1}).wait(1).to({graphics:mask_graphics_56,x:688.4,y:422.3}).wait(1).to({graphics:mask_graphics_57,x:707,y:417.9}).wait(1).to({graphics:mask_graphics_58,x:723.8,y:413.9}).wait(1).to({graphics:mask_graphics_59,x:738.9,y:410.4}).wait(1).to({graphics:mask_graphics_60,x:752.4,y:407.2}).wait(1).to({graphics:mask_graphics_61,x:764.5,y:404.3}).wait(1).to({graphics:mask_graphics_62,x:775.1,y:401.8}).wait(1).to({graphics:mask_graphics_63,x:784.4,y:399.6}).wait(1).to({graphics:mask_graphics_64,x:792.4,y:397.8}).wait(1).to({graphics:mask_graphics_65,x:799.3,y:396.1}).wait(1).to({graphics:mask_graphics_66,x:805.2,y:394.7}).wait(1).to({graphics:mask_graphics_67,x:810,y:393.6}).wait(1).to({graphics:mask_graphics_68,x:814,y:392.7}).wait(1).to({graphics:mask_graphics_69,x:817.2,y:391.9}).wait(1).to({graphics:mask_graphics_70,x:819.7,y:391.3}).wait(1).to({graphics:mask_graphics_71,x:821.6,y:390.9}).wait(1).to({graphics:mask_graphics_72,x:822.9,y:390.6}).wait(1).to({graphics:mask_graphics_73,x:823.8,y:390.3}).wait(1).to({graphics:mask_graphics_74,x:824.4,y:390.2}).wait(1).to({graphics:mask_graphics_75,x:824.6,y:390.2}).wait(1).to({graphics:mask_graphics_76,x:824.7,y:390.1}).wait(1).to({graphics:mask_graphics_77,x:826.5,y:388.4}).wait(1));

	// Layer_1
	this.instance = new lib.th_orange();
	this.instance.parent = this;
	this.instance.setTransform(834.1,565.7,1,1,0,0,0,834.1,565.7);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(78));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,60.7,111.6,1070.7);


(lib.mov_green = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask KFramed (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EiW1gzWMEHGg9uMAmlCkbMkHGA9ug");
	var mask_graphics_1 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_2 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_3 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_4 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_5 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_6 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_7 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_8 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_9 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_10 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_11 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_12 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_13 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_14 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_15 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_16 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_17 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_18 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_19 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_20 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_21 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_22 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_23 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_24 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_25 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_26 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_27 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_28 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_29 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_30 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_31 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_32 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_33 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_34 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_35 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_36 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_37 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_38 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_39 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_40 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_41 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_42 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_43 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_44 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_45 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_46 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_47 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_48 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_49 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_50 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_51 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_52 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_53 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_54 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_55 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_56 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_57 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_58 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_59 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_60 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_61 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_62 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_63 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_64 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_65 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_66 = new cjs.Graphics().p("EiWbgzqMEG/g9CMAmJCkXMkG/A9Cg");
	var mask_graphics_67 = new cjs.Graphics().p("EiWDgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_68 = new cjs.Graphics().p("EiVvgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_69 = new cjs.Graphics().p("EiVfgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_70 = new cjs.Graphics().p("EiVSgzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_71 = new cjs.Graphics().p("EiVJgzqMEG/g9CMAmJCkXMkG/A9Cg");
	var mask_graphics_72 = new cjs.Graphics().p("EiVCgzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_73 = new cjs.Graphics().p("EiU+gzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_74 = new cjs.Graphics().p("EiU7gzqMEG/g9CMAmJCkXMkG/A9Cg");
	var mask_graphics_75 = new cjs.Graphics().p("EiU6gzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_76 = new cjs.Graphics().p("EiU5gzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_77 = new cjs.Graphics().p("EiVMgzWMEHGg9uMAmlCkbMkHGA9ug");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-694,y:624.5}).wait(1).to({graphics:mask_graphics_1,x:-695.4,y:626.4}).wait(1).to({graphics:mask_graphics_2,x:-695.3,y:626.4}).wait(1).to({graphics:mask_graphics_3,x:-695,y:626.3}).wait(1).to({graphics:mask_graphics_4,x:-694.5,y:626.2}).wait(1).to({graphics:mask_graphics_5,x:-693.6,y:626}).wait(1).to({graphics:mask_graphics_6,x:-692.2,y:625.7}).wait(1).to({graphics:mask_graphics_7,x:-690.4,y:625.2}).wait(1).to({graphics:mask_graphics_8,x:-687.9,y:624.7}).wait(1).to({graphics:mask_graphics_9,x:-684.7,y:623.9}).wait(1).to({graphics:mask_graphics_10,x:-680.7,y:623}).wait(1).to({graphics:mask_graphics_11,x:-675.8,y:621.8}).wait(1).to({graphics:mask_graphics_12,x:-670,y:620.4}).wait(1).to({graphics:mask_graphics_13,x:-663.1,y:618.8}).wait(1).to({graphics:mask_graphics_14,x:-655,y:616.9}).wait(1).to({graphics:mask_graphics_15,x:-645.7,y:614.7}).wait(1).to({graphics:mask_graphics_16,x:-635.1,y:612.2}).wait(1).to({graphics:mask_graphics_17,x:-623.1,y:609.4}).wait(1).to({graphics:mask_graphics_18,x:-609.6,y:606.2}).wait(1).to({graphics:mask_graphics_19,x:-594.5,y:602.6}).wait(1).to({graphics:mask_graphics_20,x:-577.7,y:598.7}).wait(1).to({graphics:mask_graphics_21,x:-559.1,y:594.3}).wait(1).to({graphics:mask_graphics_22,x:-538.7,y:589.5}).wait(1).to({graphics:mask_graphics_23,x:-516.3,y:584.2}).wait(1).to({graphics:mask_graphics_24,x:-491.9,y:578.5}).wait(1).to({graphics:mask_graphics_25,x:-465.4,y:572.2}).wait(1).to({graphics:mask_graphics_26,x:-436.6,y:565.4}).wait(1).to({graphics:mask_graphics_27,x:-405.6,y:558.1}).wait(1).to({graphics:mask_graphics_28,x:-372.2,y:550.3}).wait(1).to({graphics:mask_graphics_29,x:-336.4,y:541.8}).wait(1).to({graphics:mask_graphics_30,x:-297.9,y:532.7}).wait(1).to({graphics:mask_graphics_31,x:-256.8,y:523}).wait(1).to({graphics:mask_graphics_32,x:-213,y:512.7}).wait(1).to({graphics:mask_graphics_33,x:-166.3,y:501.7}).wait(1).to({graphics:mask_graphics_34,x:-116.8,y:490}).wait(1).to({graphics:mask_graphics_35,x:-64.2,y:477.6}).wait(1).to({graphics:mask_graphics_36,x:-8.5,y:464.5}).wait(1).to({graphics:mask_graphics_37,x:50.3,y:450.7}).wait(1).to({graphics:mask_graphics_38,x:112.4,y:436}).wait(1).to({graphics:mask_graphics_39,x:177.1,y:420.8}).wait(1).to({graphics:mask_graphics_40,x:239.2,y:406.1}).wait(1).to({graphics:mask_graphics_41,x:298,y:392.3}).wait(1).to({graphics:mask_graphics_42,x:353.7,y:379.2}).wait(1).to({graphics:mask_graphics_43,x:406.3,y:366.8}).wait(1).to({graphics:mask_graphics_44,x:455.9,y:355.1}).wait(1).to({graphics:mask_graphics_45,x:502.5,y:344.1}).wait(1).to({graphics:mask_graphics_46,x:546.3,y:333.8}).wait(1).to({graphics:mask_graphics_47,x:587.4,y:324.1}).wait(1).to({graphics:mask_graphics_48,x:625.9,y:315.1}).wait(1).to({graphics:mask_graphics_49,x:661.8,y:306.6}).wait(1).to({graphics:mask_graphics_50,x:695.2,y:298.7}).wait(1).to({graphics:mask_graphics_51,x:726.2,y:291.4}).wait(1).to({graphics:mask_graphics_52,x:754.9,y:284.6}).wait(1).to({graphics:mask_graphics_53,x:781.4,y:278.4}).wait(1).to({graphics:mask_graphics_54,x:805.8,y:272.6}).wait(1).to({graphics:mask_graphics_55,x:828.2,y:267.4}).wait(1).to({graphics:mask_graphics_56,x:848.6,y:262.6}).wait(1).to({graphics:mask_graphics_57,x:867.2,y:258.2}).wait(1).to({graphics:mask_graphics_58,x:884,y:254.2}).wait(1).to({graphics:mask_graphics_59,x:899.1,y:250.7}).wait(1).to({graphics:mask_graphics_60,x:912.6,y:247.5}).wait(1).to({graphics:mask_graphics_61,x:924.7,y:244.6}).wait(1).to({graphics:mask_graphics_62,x:935.3,y:242.1}).wait(1).to({graphics:mask_graphics_63,x:944.6,y:239.9}).wait(1).to({graphics:mask_graphics_64,x:952.6,y:238.1}).wait(1).to({graphics:mask_graphics_65,x:959.5,y:236.4}).wait(1).to({graphics:mask_graphics_66,x:964.5,y:235}).wait(1).to({graphics:mask_graphics_67,x:966.9,y:233.9}).wait(1).to({graphics:mask_graphics_68,x:968.9,y:233}).wait(1).to({graphics:mask_graphics_69,x:970.5,y:232.2}).wait(1).to({graphics:mask_graphics_70,x:971.8,y:231.6}).wait(1).to({graphics:mask_graphics_71,x:972.7,y:231.2}).wait(1).to({graphics:mask_graphics_72,x:973.4,y:230.9}).wait(1).to({graphics:mask_graphics_73,x:973.8,y:230.6}).wait(1).to({graphics:mask_graphics_74,x:974.1,y:230.5}).wait(1).to({graphics:mask_graphics_75,x:974.2,y:230.5}).wait(1).to({graphics:mask_graphics_76,x:974.3,y:230.4}).wait(1).to({graphics:mask_graphics_77,x:975.9,y:228.5}).wait(1));

	// Layer_1
	this.instance = new lib.th_green();
	this.instance.parent = this;
	this.instance.setTransform(943,321.8,1,1,0,0,0,943,321.8);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(78));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1659.3,-99.2,1930.8,1447.4);


(lib.mov_blue = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask KFramed (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EiW1gzWMEHGg9uMAmlCkbMkHGA9ug");
	var mask_graphics_1 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_2 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_3 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_4 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_5 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_6 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_7 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_8 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_9 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_10 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_11 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_12 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_13 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_14 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_15 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_16 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_17 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_18 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_19 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_20 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_21 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_22 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_23 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_24 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_25 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_26 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_27 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_28 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_29 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_30 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_31 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_32 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_33 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_34 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_35 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_36 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_37 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_38 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_39 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_40 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_41 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_42 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_43 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_44 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_45 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_46 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_47 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_48 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_49 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_50 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_51 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_52 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_53 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_54 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_55 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_56 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_57 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_58 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_59 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_60 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_61 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_62 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_63 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_64 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_65 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_66 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_67 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_68 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_69 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_70 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_71 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_72 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_73 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_74 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_75 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_76 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_77 = new cjs.Graphics().p("EiW1gzWMEHGg9uMAmlCkbMkHGA9ug");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-844.1,y:616.7}).wait(1).to({graphics:mask_graphics_1,x:-845.6,y:618.4}).wait(1).to({graphics:mask_graphics_2,x:-845.5,y:618.4}).wait(1).to({graphics:mask_graphics_3,x:-845.2,y:618.3}).wait(1).to({graphics:mask_graphics_4,x:-844.7,y:618.2}).wait(1).to({graphics:mask_graphics_5,x:-843.8,y:617.9}).wait(1).to({graphics:mask_graphics_6,x:-842.4,y:617.6}).wait(1).to({graphics:mask_graphics_7,x:-840.6,y:617.2}).wait(1).to({graphics:mask_graphics_8,x:-838.1,y:616.6}).wait(1).to({graphics:mask_graphics_9,x:-834.9,y:615.8}).wait(1).to({graphics:mask_graphics_10,x:-830.9,y:614.9}).wait(1).to({graphics:mask_graphics_11,x:-826,y:613.8}).wait(1).to({graphics:mask_graphics_12,x:-820.2,y:612.4}).wait(1).to({graphics:mask_graphics_13,x:-813.3,y:610.8}).wait(1).to({graphics:mask_graphics_14,x:-805.2,y:608.9}).wait(1).to({graphics:mask_graphics_15,x:-795.9,y:606.7}).wait(1).to({graphics:mask_graphics_16,x:-785.3,y:604.2}).wait(1).to({graphics:mask_graphics_17,x:-773.3,y:601.3}).wait(1).to({graphics:mask_graphics_18,x:-759.8,y:598.1}).wait(1).to({graphics:mask_graphics_19,x:-744.7,y:594.6}).wait(1).to({graphics:mask_graphics_20,x:-727.9,y:590.6}).wait(1).to({graphics:mask_graphics_21,x:-709.3,y:586.2}).wait(1).to({graphics:mask_graphics_22,x:-688.9,y:581.4}).wait(1).to({graphics:mask_graphics_23,x:-666.5,y:576.2}).wait(1).to({graphics:mask_graphics_24,x:-642.1,y:570.4}).wait(1).to({graphics:mask_graphics_25,x:-615.6,y:564.2}).wait(1).to({graphics:mask_graphics_26,x:-586.8,y:557.4}).wait(1).to({graphics:mask_graphics_27,x:-555.8,y:550.1}).wait(1).to({graphics:mask_graphics_28,x:-522.4,y:542.2}).wait(1).to({graphics:mask_graphics_29,x:-486.6,y:533.7}).wait(1).to({graphics:mask_graphics_30,x:-448.1,y:524.7}).wait(1).to({graphics:mask_graphics_31,x:-407,y:515}).wait(1).to({graphics:mask_graphics_32,x:-363.2,y:504.7}).wait(1).to({graphics:mask_graphics_33,x:-316.5,y:493.7}).wait(1).to({graphics:mask_graphics_34,x:-267,y:482}).wait(1).to({graphics:mask_graphics_35,x:-214.4,y:469.6}).wait(1).to({graphics:mask_graphics_36,x:-158.7,y:456.5}).wait(1).to({graphics:mask_graphics_37,x:-99.9,y:442.6}).wait(1).to({graphics:mask_graphics_38,x:-37.8,y:428}).wait(1).to({graphics:mask_graphics_39,x:26.9,y:412.7}).wait(1).to({graphics:mask_graphics_40,x:89,y:398.1}).wait(1).to({graphics:mask_graphics_41,x:147.8,y:384.2}).wait(1).to({graphics:mask_graphics_42,x:203.5,y:371.1}).wait(1).to({graphics:mask_graphics_43,x:256.1,y:358.7}).wait(1).to({graphics:mask_graphics_44,x:305.7,y:347}).wait(1).to({graphics:mask_graphics_45,x:352.3,y:336}).wait(1).to({graphics:mask_graphics_46,x:396.1,y:325.8}).wait(1).to({graphics:mask_graphics_47,x:437.2,y:316.1}).wait(1).to({graphics:mask_graphics_48,x:475.7,y:307}).wait(1).to({graphics:mask_graphics_49,x:511.6,y:298.6}).wait(1).to({graphics:mask_graphics_50,x:545,y:290.7}).wait(1).to({graphics:mask_graphics_51,x:576,y:283.4}).wait(1).to({graphics:mask_graphics_52,x:604.7,y:276.6}).wait(1).to({graphics:mask_graphics_53,x:631.2,y:270.3}).wait(1).to({graphics:mask_graphics_54,x:655.6,y:264.6}).wait(1).to({graphics:mask_graphics_55,x:678,y:259.3}).wait(1).to({graphics:mask_graphics_56,x:698.4,y:254.5}).wait(1).to({graphics:mask_graphics_57,x:717,y:250.1}).wait(1).to({graphics:mask_graphics_58,x:733.8,y:246.2}).wait(1).to({graphics:mask_graphics_59,x:748.9,y:242.6}).wait(1).to({graphics:mask_graphics_60,x:762.4,y:239.4}).wait(1).to({graphics:mask_graphics_61,x:774.5,y:236.6}).wait(1).to({graphics:mask_graphics_62,x:785.1,y:234.1}).wait(1).to({graphics:mask_graphics_63,x:794.4,y:231.9}).wait(1).to({graphics:mask_graphics_64,x:802.4,y:230}).wait(1).to({graphics:mask_graphics_65,x:809.3,y:228.4}).wait(1).to({graphics:mask_graphics_66,x:815.2,y:227}).wait(1).to({graphics:mask_graphics_67,x:820,y:225.9}).wait(1).to({graphics:mask_graphics_68,x:824,y:224.9}).wait(1).to({graphics:mask_graphics_69,x:827.2,y:224.2}).wait(1).to({graphics:mask_graphics_70,x:829.7,y:223.6}).wait(1).to({graphics:mask_graphics_71,x:831.6,y:223.1}).wait(1).to({graphics:mask_graphics_72,x:832.9,y:222.8}).wait(1).to({graphics:mask_graphics_73,x:833.8,y:222.6}).wait(1).to({graphics:mask_graphics_74,x:834.4,y:222.5}).wait(1).to({graphics:mask_graphics_75,x:834.6,y:222.4}).wait(1).to({graphics:mask_graphics_76,x:834.7,y:222.4}).wait(1).to({graphics:mask_graphics_77,x:836.3,y:220.7}).wait(1));

	// Layer_1
	this.instance = new lib.th_blue();
	this.instance.parent = this;
	this.instance.setTransform(883.4,327.3,1,1,0,0,0,883.4,327.3);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(78));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,121.3,654.6);


(lib.mov_black = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask KFramed (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EiW1gjyMEHGg9uMAmlCkaMkHGA9vg");
	var mask_graphics_1 = new cjs.Graphics().p("EiWjgjuMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_2 = new cjs.Graphics().p("EiWjgjuMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_3 = new cjs.Graphics().p("EiWjgjvMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_4 = new cjs.Graphics().p("EiWjgjvMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_5 = new cjs.Graphics().p("EiWjgjxMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_6 = new cjs.Graphics().p("EiWjgjyMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_7 = new cjs.Graphics().p("EiWjgj0MEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_8 = new cjs.Graphics().p("EiWjgj3MEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_9 = new cjs.Graphics().p("EiWjgj7MEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_10 = new cjs.Graphics().p("EiWjgkAMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_11 = new cjs.Graphics().p("EiWjgkFMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_12 = new cjs.Graphics().p("EiWjgkMMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_13 = new cjs.Graphics().p("EiWjgkUMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_14 = new cjs.Graphics().p("EiWjgkeMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_15 = new cjs.Graphics().p("EiWjgkpMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_16 = new cjs.Graphics().p("EiWjgk1MEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_17 = new cjs.Graphics().p("EiWjglEMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_18 = new cjs.Graphics().p("EiWjglUMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_19 = new cjs.Graphics().p("EiWjgllMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_20 = new cjs.Graphics().p("EiWjgl5MEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_21 = new cjs.Graphics().p("EiWjgmPMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_22 = new cjs.Graphics().p("EiWjgmnMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_23 = new cjs.Graphics().p("EiWjgnBMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_24 = new cjs.Graphics().p("EiWjgneMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_25 = new cjs.Graphics().p("EiWjgn9MEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_26 = new cjs.Graphics().p("EiWjgofMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_27 = new cjs.Graphics().p("EiWjgpEMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_28 = new cjs.Graphics().p("EiWjgprMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_29 = new cjs.Graphics().p("EiWjgqWMEG+g9CMAmJCkXMkG+A9Dg");
	var mask_graphics_30 = new cjs.Graphics().p("EiWjgrDMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_31 = new cjs.Graphics().p("EiWjgrzMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_32 = new cjs.Graphics().p("EiWjgsnMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_33 = new cjs.Graphics().p("EiWjgteMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_34 = new cjs.Graphics().p("EiWjguYMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_35 = new cjs.Graphics().p("EiWjgvWMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_36 = new cjs.Graphics().p("EiWjgwYMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_37 = new cjs.Graphics().p("EiWjgxdMEG+g9DMAmJCkYMkG+A9Cg");
	var mask_graphics_38 = new cjs.Graphics().p("EiWjgymMEG+g9DMAmJCkXMkG+A9Dg");
	var mask_graphics_39 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_40 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_41 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_42 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_43 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_44 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_45 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_46 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_47 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_48 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_49 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_50 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_51 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_52 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_53 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_54 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_55 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_56 = new cjs.Graphics().p("EiWjgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_57 = new cjs.Graphics().p("EiVugzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_58 = new cjs.Graphics().p("EiUagzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_59 = new cjs.Graphics().p("EiTOgzqMEG/g9CMAmJCkXMkG/A9Cg");
	var mask_graphics_60 = new cjs.Graphics().p("EiSKgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_61 = new cjs.Graphics().p("EiROgzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_62 = new cjs.Graphics().p("EiQZgzqMEG+g9CMAmKCkXMkG/A9Cg");
	var mask_graphics_63 = new cjs.Graphics().p("EiPrgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_64 = new cjs.Graphics().p("EiPCgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_65 = new cjs.Graphics().p("EiOggzqMEG/g9CMAmJCkXMkG/A9Cg");
	var mask_graphics_66 = new cjs.Graphics().p("EiODgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_67 = new cjs.Graphics().p("EiNqgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_68 = new cjs.Graphics().p("EiNWgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_69 = new cjs.Graphics().p("EiNGgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_70 = new cjs.Graphics().p("EiM6gzqMEG/g9CMAmJCkXMkG/A9Cg");
	var mask_graphics_71 = new cjs.Graphics().p("EiMxgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_72 = new cjs.Graphics().p("EiMqgzqMEG/g9CMAmJCkXMkG/A9Cg");
	var mask_graphics_73 = new cjs.Graphics().p("EiMlgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_74 = new cjs.Graphics().p("EiMjgzqMEG/g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_75 = new cjs.Graphics().p("EiMhgzqMEG+g9CMAmJCkXMkG+A9Cg");
	var mask_graphics_76 = new cjs.Graphics().p("EiMhgzqMEG/g9CMAmJCkXMkG/A9Cg");
	var mask_graphics_77 = new cjs.Graphics().p("EiMzgzWMEHGg9uMAmkCkbMkHGA9ug");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-586.7,y:823.3}).wait(1).to({graphics:mask_graphics_1,x:-588.2,y:823.3}).wait(1).to({graphics:mask_graphics_2,x:-588.1,y:823.3}).wait(1).to({graphics:mask_graphics_3,x:-587.8,y:823.2}).wait(1).to({graphics:mask_graphics_4,x:-587.3,y:823.2}).wait(1).to({graphics:mask_graphics_5,x:-586.4,y:823.1}).wait(1).to({graphics:mask_graphics_6,x:-585,y:822.9}).wait(1).to({graphics:mask_graphics_7,x:-583.2,y:822.7}).wait(1).to({graphics:mask_graphics_8,x:-580.6,y:822.4}).wait(1).to({graphics:mask_graphics_9,x:-577.5,y:822}).wait(1).to({graphics:mask_graphics_10,x:-573.5,y:821.6}).wait(1).to({graphics:mask_graphics_11,x:-568.6,y:821}).wait(1).to({graphics:mask_graphics_12,x:-562.8,y:820.3}).wait(1).to({graphics:mask_graphics_13,x:-555.8,y:819.5}).wait(1).to({graphics:mask_graphics_14,x:-547.8,y:818.5}).wait(1).to({graphics:mask_graphics_15,x:-538.5,y:817.4}).wait(1).to({graphics:mask_graphics_16,x:-527.9,y:816.2}).wait(1).to({graphics:mask_graphics_17,x:-515.8,y:814.8}).wait(1).to({graphics:mask_graphics_18,x:-502.3,y:813.2}).wait(1).to({graphics:mask_graphics_19,x:-487.2,y:811.4}).wait(1).to({graphics:mask_graphics_20,x:-470.4,y:809.4}).wait(1).to({graphics:mask_graphics_21,x:-451.8,y:807.2}).wait(1).to({graphics:mask_graphics_22,x:-431.4,y:804.8}).wait(1).to({graphics:mask_graphics_23,x:-409,y:802.2}).wait(1).to({graphics:mask_graphics_24,x:-384.7,y:799.3}).wait(1).to({graphics:mask_graphics_25,x:-358.1,y:796.2}).wait(1).to({graphics:mask_graphics_26,x:-329.4,y:792.8}).wait(1).to({graphics:mask_graphics_27,x:-298.4,y:789.1}).wait(1).to({graphics:mask_graphics_28,x:-265,y:785.2}).wait(1).to({graphics:mask_graphics_29,x:-229.1,y:781}).wait(1).to({graphics:mask_graphics_30,x:-190.7,y:776.4}).wait(1).to({graphics:mask_graphics_31,x:-149.6,y:771.6}).wait(1).to({graphics:mask_graphics_32,x:-105.7,y:766.4}).wait(1).to({graphics:mask_graphics_33,x:-59.1,y:760.9}).wait(1).to({graphics:mask_graphics_34,x:-9.5,y:755.1}).wait(1).to({graphics:mask_graphics_35,x:43.1,y:748.9}).wait(1).to({graphics:mask_graphics_36,x:98.7,y:742.3}).wait(1).to({graphics:mask_graphics_37,x:157.6,y:735.4}).wait(1).to({graphics:mask_graphics_38,x:219.7,y:728.1}).wait(1).to({graphics:mask_graphics_39,x:284.3,y:719.6}).wait(1).to({graphics:mask_graphics_40,x:346.4,y:704.9}).wait(1).to({graphics:mask_graphics_41,x:405.3,y:691.1}).wait(1).to({graphics:mask_graphics_42,x:460.9,y:678}).wait(1).to({graphics:mask_graphics_43,x:513.5,y:665.6}).wait(1).to({graphics:mask_graphics_44,x:563.1,y:653.9}).wait(1).to({graphics:mask_graphics_45,x:609.8,y:642.9}).wait(1).to({graphics:mask_graphics_46,x:653.6,y:632.6}).wait(1).to({graphics:mask_graphics_47,x:694.7,y:622.9}).wait(1).to({graphics:mask_graphics_48,x:733.1,y:613.8}).wait(1).to({graphics:mask_graphics_49,x:769,y:605.4}).wait(1).to({graphics:mask_graphics_50,x:802.4,y:597.5}).wait(1).to({graphics:mask_graphics_51,x:833.4,y:590.2}).wait(1).to({graphics:mask_graphics_52,x:862.2,y:583.4}).wait(1).to({graphics:mask_graphics_53,x:888.7,y:577.1}).wait(1).to({graphics:mask_graphics_54,x:913.1,y:571.4}).wait(1).to({graphics:mask_graphics_55,x:935.4,y:566.1}).wait(1).to({graphics:mask_graphics_56,x:955.9,y:561.3}).wait(1).to({graphics:mask_graphics_57,x:969,y:556.9}).wait(1).to({graphics:mask_graphics_58,x:977.4,y:553}).wait(1).to({graphics:mask_graphics_59,x:985,y:549.4}).wait(1).to({graphics:mask_graphics_60,x:991.7,y:546.2}).wait(1).to({graphics:mask_graphics_61,x:997.8,y:543.4}).wait(1).to({graphics:mask_graphics_62,x:1003.1,y:540.9}).wait(1).to({graphics:mask_graphics_63,x:1007.7,y:538.7}).wait(1).to({graphics:mask_graphics_64,x:1011.7,y:536.8}).wait(1).to({graphics:mask_graphics_65,x:1015.2,y:535.2}).wait(1).to({graphics:mask_graphics_66,x:1018.1,y:533.8}).wait(1).to({graphics:mask_graphics_67,x:1020.5,y:532.7}).wait(1).to({graphics:mask_graphics_68,x:1022.5,y:531.7}).wait(1).to({graphics:mask_graphics_69,x:1024.1,y:531}).wait(1).to({graphics:mask_graphics_70,x:1025.4,y:530.4}).wait(1).to({graphics:mask_graphics_71,x:1026.3,y:529.9}).wait(1).to({graphics:mask_graphics_72,x:1027,y:529.6}).wait(1).to({graphics:mask_graphics_73,x:1027.4,y:529.4}).wait(1).to({graphics:mask_graphics_74,x:1027.7,y:529.3}).wait(1).to({graphics:mask_graphics_75,x:1027.8,y:529.2}).wait(1).to({graphics:mask_graphics_76,x:1027.9,y:529.2}).wait(1).to({graphics:mask_graphics_77,x:1029.5,y:526.8}).wait(1));

	// Layer_1
	this.instance = new lib.th_black();
	this.instance.parent = this;
	this.instance.setTransform(982.6,481.7,1,1,0,0,0,982.6,481.7);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(78));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,199.2,378.7,764.2);


(lib._all_ = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_103 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(103).call(this.frame_103).wait(1));

	// strokes
	this.instance = new lib.Strokes("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(758.3,427.5,1,1,0,0,0,966.1,656.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).wait(92));

	// green
	this.instance_1 = new lib.mov_green("synched",0,false);
	this.instance_1.parent = this;
	this.instance_1.setTransform(756,452.4,1,1,0,0,0,943,321.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5).to({_off:false},0).wait(99));

	// red
	this.instance_2 = new lib.mov_red("synched",0,false);
	this.instance_2.parent = this;
	this.instance_2.setTransform(1072.8,275.9,1,1,0,0,0,705.7,224.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(104));

	// blue
	this.instance_3 = new lib.mov_blue("synched",0,false);
	this.instance_3.parent = this;
	this.instance_3.setTransform(849.4,466.6,1,1,0,0,0,883.4,327.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off:false},0).wait(101));

	// black
	this.instance_4 = new lib.mov_black("synched",0,false);
	this.instance_4.parent = this;
	this.instance_4.setTransform(691.2,312,1,1,0,0,0,982.6,481.7);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(7).to({_off:false},0).wait(97));

	// yellow
	this.instance_5 = new lib.mov_yellow("synched",0,false);
	this.instance_5.parent = this;
	this.instance_5.setTransform(764.4,370.8,1,1,0,0,0,893.1,628.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(5).to({_off:false},0).wait(99));

	// orange
	this.instance_6 = new lib.mov_orange("synched",0,false);
	this.instance_6.parent = this;
	this.instance_6.setTransform(812.8,540.1,1,1,0,0,0,834.1,565.7);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(2).to({_off:false},0).wait(102));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1600,720);


// stage content:
(lib.threads = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// all
	this.all = new lib._all_();
	this.all.name = "all";
	this.all.parent = this;
	this.all.setTransform(481.2,316.9,1,1,0,0,0,481.2,316.9);

	this.timeline.addTween(cjs.Tween.get(this.all).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1044.1,390.6,3622.6,1447.4);
// library properties:
lib.properties = {
	id: 'EF8F6105A24B4C92AA0148B9EA2A23D5',
	width: 1600,
	height: 720,
	fps: 30,
	color: "#CCCCCC",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['EF8F6105A24B4C92AA0148B9EA2A23D5'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;