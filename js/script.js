// Preloader //
$(window).on('load', function () {
   $(".preloader").fadeOut("slow");

});


jQuery(function ($) {


   // Navbar Scroll Function
   var $window = $(window);
   $window.scroll(function () {
      var $scroll = $window.scrollTop();
      var $navbar = $(".navbar");
      if (!$navbar.hasClass("sticky-bottom")) {
         if ($scroll > 200) {
            $navbar.addClass("fixed-menu");
         } else {
            $navbar.removeClass("fixed-menu");
         }
      }
   });

   /*bottom menu fix*/
   if ($("nav.navbar").hasClass("sticky-bottom")) {
      var navHeight = $(".sticky-bottom").offset().top;
      $(window).scroll(function () {
         if ($(window).scrollTop() > navHeight) {
            $('.sticky-bottom').addClass('fixed-menu');
         } else {
            $('.sticky-bottom').removeClass('fixed-menu');
         }
      });
   }



   // Click Scroll Function
   $(".scroll").on('click', function (event) {
      event.preventDefault();
      $("html,body").animate({
         scrollTop: $(this.hash).offset().top
      }, 1000);
   });

   
   
   /*----- SideBar Menu On click -----*/
  var $menu_left = $(".side-nav-left");
  var $menu_right = $(".side-nav-right");
  var $menu_full = $(".full-nav");
  var $toggler = $("#menu_bars");

    if ($("#menu_bars").length) {
       $("body").addClass("side-nav-push");
       
       if($toggler.hasClass("left")){
          $toggler.on("click", function (e) {
         $(this).toggleClass("active");
         var fade_logo = $(".navbar-logo-fade #menu_bars");
         if(!$(".navbar-logo-fade").hasClass("fixed-fade") && fade_logo.hasClass("active")) {
            $(".navbar-brand").addClass("d-none");
         }
             else if ($(".navbar-logo-fade").hasClass("fixed-fade")){
                $(".navbar-brand").addClass("d-none");
             }
         else {
            $(".navbar-brand").removeClass("d-none");
         }
         $(".side-nav-push").toggleClass("side-nav-push-toright");
         $menu_left.toggleClass("side-nav-open");
         e.stopPropagation();
       });
       }
       else if ($toggler.hasClass("right")){
          $toggler.on("click", function (e) {
            $(this).toggleClass("active");
            $(".side-nav-push").toggleClass("side-nav-push-toleft");
            $menu_right.toggleClass("side-nav-open");
            e.stopPropagation();
               $('.page-wrapper > section').on("click",function (e) {
                  if ($toggler.hasClass("active")) {
                     console.log('click')
                     $toggler.toggleClass("active");
                     $(".side-nav-push").toggleClass("side-nav-push-toleft");
                     $menu_right.toggleClass("side-nav-open");
                     e.stopPropagation();
                  }
               })
             
          });
          
       }
       else {
          if($toggler.hasClass("full")){
             $toggler.on("click", function (e) {
               $(this).toggleClass("active");
               $menu_full.toggleClass("side-nav-open");
               e.stopPropagation();
             });
          }
       }  
  }

  //fix the bug on ipad safari when the flip cards won't work until menu btn is pressed
  //$toggler.click();
  //$toggler.click();  
   
   
   
   if($(".navbar-logo-fade").length){
      $window.on("scroll", function () {
      if ($window.scrollTop() >590) { 
         $(".navbar-logo-fade").addClass("fixed-fade");
         $(".navbar-logo-fade .navbar-brand").addClass("d-none");
      } else {
         $(".navbar-logo-fade").removeClass("fixed-fade");
         $(".navbar-logo-fade .navbar-brand").removeClass("d-none");
      }
   });
   }
   

   new Swiper ('.testimonials', {
      loop: true,
      pagination: {
         el: '.swiper-pagination',
         type: 'bullets',
         clickable: true
      },
   });



   
   /*---- Wow Initializing ----*/
    /*new WOW().init();*/
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: false,
        live: true
    });
    new WOW().init();


    $('.list-box li span').click(function(){
       if ($(this).siblings('p').hasClass('shown')) {

         $(this).siblings('p').slideUp().removeClass('shown');
       
       } else {

         $('.list-box').find('p.shown').each(function() {
           $(this).slideUp().removeClass('shown');
         })
       
         $(this).siblings('p').slideDown().addClass('shown');

       }

     });
   



  //intersection observer
  // create config object: rootMargin and threshold
  // are two properties exposed by the interface
  // const config = {
  //   rootMargin: '0px 0px 50px 0px',
  //   threshold: 0
  // };

  // // register the config object with an instance
  // // of intersectionObserver
  // let observer = new IntersectionObserver(function(entries, self) {
  //   // iterate over each entry
  //   entries.forEach(entry => {
  //     // process just the images that are intersecting.
  //     // isIntersecting is a property exposed by the interface
  //     if(entry.isIntersecting) {
  //       // custom function that copies the path to the img
  //       // from data-src to src
  //       preloadImage(entry.target);
  //       // the image is now in place, stop watching
  //       self.unobserve(entry.target);
  //     }
  //   });
  // }, config);

  // const imgs = document.querySelectorAll('[data-src]');
  // imgs.forEach(img => {
  //   observer.observe(img);
  // });

  // function preloadImage(img) {
  //   const src = img.getAttribute('data-src');
  //   if (!src) { return; }
  //   img.src = src;
  // }
   



});