jQuery(function() {

  $.when(

    $.get("images/images_fr/image_by_type.svg", function(svg) {
      $("#type").append(svg.documentElement);
    }),
    $.get("images/images_fr/image_by_type_region.svg", function(svg) {
      $("#type-region").append(svg.documentElement);
    }),
    $.get("images/images_fr/image_by_sex.svg", function(svg) {
      $("#sex").append(svg.documentElement);
    }),
    $.get("images/images_fr/image_by_sex_region.svg", function(svg) {
      $("#region").append(svg.documentElement);
    }),
    $.get("images/images_fr/image_by_sex_region2.svg", function(svg) {
      $("#sex-region").append(svg.documentElement);
    }),
    $.get("images/images_fr/image_by_workforce.svg", function(svg) {
      $("#workforce").append(svg.documentElement);
    }),
    $.get("images/icon_hand.svg", function(svg) {
      $(".icon-hand-here").append(svg.documentElement);
    })
    // $.get("images/icon_hand.svg", function(svg) {
    //   $("#voices p.padding-bottom-50").append(svg.documentElement);
    // })
// #resilience-boxes .padding-bottom-40.desktop
  ).then(initAnimation);


  function initAnimation() {
    // Figure1 Animation
    var imgByType = new TimelineMax(),
      f1Main = [("#img-type-c-main"), ("#img-type-c-text")],
      f1Arrows = [("#img-type-l-arrow"), ("#img-type-r-arrow")],
      f1l = [("#img-type-l1"), ("#img-type-l2")],
      f1r = [("#img-type-r1"), ("#img-type-r2")];

    imgByType.from(f1Main[0], 3, {
        opacity: 0,
        scale: 0,
        rotation: -270,
        transformOrigin: '50% 50%',
        ease: Elastic.easeOut.config(0.6, 0.4)
      })
      .from(f1Main[1], 3, {
        opacity: 0,
        scale: 0,
        transformOrigin: '50% 50%',
        ease: Elastic.easeOut.config(1, 0.4)
      }, '-=2.8')
      .from(f1Arrows[0], 1, {
        opacity: 0,
        scale: 0,
        transformOrigin: '100% 100%',
        ease: Power3.easeOut
      }, '-=2.6')
      .from(f1l[0], 1, {
        opacity: 0,
        x: -20,
        ease: Power3.easeOut
      }, '-=2.3')
      .from(f1l[1], 2, {
        opacity: 0,
        y: -20,
        ease: Power3.easeOut
      }, '-=2')
      .from(f1Arrows[1], 1, {
        opacity: 0,
        scale: 0,
        ease: Power3.easeOut
      }, '-=1.5')
      .from(f1r[0], 1, {
        opacity: 0,
        x: -20,
        ease: Power3.easeOut
      }, '-=1.3')
      .from(f1r[1], 2, {
        opacity: 0,
        y: -20,
        ease: Power3.easeOut
      }, '-=1');

    // Figure2 Animation

    var imgByTypeRegion = new TimelineMax(),
      f2main = [
        "#img-type-rgn-layer1",
        "#img-type-rgn-layer2",
        "#img-type-rgn-layer3",
        "#img-type-rgn-layer11"
      ],
      f2countries = [
        "#img-type-rgn-layer4 > g:first-child",
        "#img-type-rgn-layer5 > g:first-child",
        "#img-type-rgn-layer6 > g:first-child",
        "#img-type-rgn-layer7 > g:first-child",
        "#img-type-rgn-layer8 > g:first-child",
        "#img-type-rgn-layer9 > g:first-child",
        "#img-type-rgn-layer10 > g:first-child"
      ],
      f2stats = [
        "#img-type-rgn-layer4 > g:last-child",
        "#img-type-rgn-layer5 > g:last-child",
        "#img-type-rgn-layer6 > g:last-child",
        "#img-type-rgn-layer7 > g:last-child",
        "#img-type-rgn-layer8 > g:last-child",
        "#img-type-rgn-layer9 > g:last-child",
        "#img-type-rgn-layer10 > g:last-child"
      ];

    imgByTypeRegion.from(f2main[0], 1, {
        opacity: 0,
        y: 20,
        transformOrigin: '50% 50%',
        ease: Power3.easeOut
      })
      .from(f2main[1], 1, {
        opacity: 0,
        y: 20,
        ease: Power3.easeOut
      }, '-=0.5')
      .from(f2main[2], 1, {
        opacity: 0,
        transformOrigin: '50% 50%'
      })
      .staggerFrom(f2countries, 1, {
        opacity: 0,
        transformOrigin: '50% 50%',
        x: -20,
        ease: Power4.easOut
      }, 0.2, '-=3')
      .staggerFrom(f2stats, 1, {
        opacity: 0,
        transformOrigin: '50% 50%',
        x: 20,
        ease: Power4.easOut
      }, 0.2, '-=3');
    // Figure3 Animation

    var imgBySex = new TimelineMax(),
      f3main = ["#img-sex-layer1", "#img-sex-layer2"],
      f3stats1 = [
        "#img-sex-layer3 > g:nth-child(3)",
        "#img-sex-layer4 > g:nth-child(3)",
        "#img-sex-layer5 > g:nth-child(3)",
        "#img-sex-layer6 > g:nth-child(3)",
        "#img-sex-layer7 > g:nth-child(3)",
        "#img-sex-layer8 > g:nth-child(3)",
        "#img-sex-layer9 > g:nth-child(3)"
      ],
      f3countries = [
        "#img-sex-layer3 > g:nth-child(1)",
        "#img-sex-layer4 > g:nth-child(1)",
        "#img-sex-layer5 > g:nth-child(1)",
        "#img-sex-layer6 > g:nth-child(1)",
        "#img-sex-layer7 > g:nth-child(1)",
        "#img-sex-layer8 > g:nth-child(1)",
        "#img-sex-layer9 > g:nth-child(1)"
      ],
      f3stats2 = [
        "#img-sex-layer3 > g:nth-child(2)",
        "#img-sex-layer4 > g:nth-child(2)",
        "#img-sex-layer5 > g:nth-child(2)",
        "#img-sex-layer6 > g:nth-child(2)",
        "#img-sex-layer7 > g:nth-child(2)",
        "#img-sex-layer8 > g:nth-child(2)",
        "#img-sex-layer9 > g:nth-child(2)"
      ];

    imgBySex.from(f3main[0], 1, {
        opacity: 0,
        y: 20,
        transformOrigin: '50% 50%',
        ease: Power3.easeOut
      })
      .from(f3main[1], 1, {
        opacity: 0,
        y: 20,
        transformOrigin: '50% 50%',
        ease: Power3.easeOut
      }, '-=0.2')
      .staggerFrom(f3countries, 1, {
        opacity: 0,
        ease: Power4.easOut
      }, 0.2, "-=1.5")
      .staggerFrom(f3stats1, 1, {
        opacity: 0,
        transformOrigin: '50% 50%',
        x: -20,
        ease: Power4.easOut
      }, 0.2, "-=2.5")
      .staggerFrom(f3stats2, 1, {
        opacity: 0,
        transformOrigin: '50% 50%',
        x: 20,
        ease: Power4.easOut
      }, 0.2, "-=2.5");

    // Figure4 Animation

    var imgByRegion = new TimelineMax(),
      f4countries = [
        "#img-sex-region-layer1 > g:first-child",
        "#img-sex-region-layer2 > g:first-child",
        "#img-sex-region-layer3 > g:first-child",
        "#img-sex-region-layer4 > g:first-child",
        "#img-sex-region-layer5 > g:first-child",
        "#img-sex-region-layer6 > g:first-child",
        "#img-sex-region-layer7 > g:first-child"
      ],
      f4stats = [
        "#img-sex-region-layer1 > g:last-child",
        "#img-sex-region-layer2 > g:last-child",
        "#img-sex-region-layer3 > g:last-child",
        "#img-sex-region-layer4 > g:last-child",
        "#img-sex-region-layer5 > g:last-child",
        "#img-sex-region-layer6 > g:last-child",
        "#img-sex-region-layer7 > g:last-child"
      ];

    imgByRegion.staggerFrom(f4countries, 1, {
        opacity: 0,
        transformOrigin: '50% 50%',
        x: -20,
        ease: Power4.easOut
      }, 0.2)
      .staggerFrom(f4stats, 1, {
        opacity: 0,
        transformOrigin: '50% 50%',
        x: 20,
        ease: Power4.easOut
      }, 0.2, "-=2.5");
    // Figure5 Animation

    var imgByWorkforce = new TimelineMax(),
      f5countries = [
        "#img-workforce-layer1 > g:first-child",
        "#img-workforce-layer2 > g:first-child",
        "#img-workforce-layer3 > g:first-child",
        "#img-workforce-layer4 > g:first-child",
        "#img-workforce-layer5 > g:first-child",
        "#img-workforce-layer6 > g:first-child",
        "#img-workforce-layer7 > g:first-child",
        "#img-workforce-layer8 > g:first-child",
        "#img-workforce-layer9 > g:first-child",
        "#img-workforce-layer10 > g:first-child",
        "#img-workforce-layer11 > g:first-child"
      ],
      f5stats = [
        "#img-workforce-layer1 > g:last-child",
        "#img-workforce-layer2 > g:last-child",
        "#img-workforce-layer3 > g:last-child",
        "#img-workforce-layer4 > g:last-child",
        "#img-workforce-layer5 > g:last-child",
        "#img-workforce-layer6 > g:last-child",
        "#img-workforce-layer7 > g:last-child",
        "#img-workforce-layer8 > g:last-child",
        "#img-workforce-layer9 > g:last-child",
        "#img-workforce-layer10 > g:last-child",
        "#img-workforce-layer11 > g:last-child"
      ];

    imgByWorkforce.staggerFrom(f5countries, 1, {
        opacity: 0,
        transformOrigin: '50% 50%',
        x: -20,
        ease: Power4.easOut
      }, 0.2)
      .staggerFrom(f5stats, 1, {
        opacity: 0,
        transformOrigin: '50% 50%',
        x: 20,
        ease: Power4.easOut
      }, 0.2, "-=3");

    $("#scale-scope").waypoint(function(direction) {
      handler: imgByType.restart(),
      this.destroy();
    }, {
      offset: "80%"
    });

    var imgBySexRegion = new TimelineMax(),
    iSRmain = ["#img-sex-region2-layer1", "#img-sex-region2-layer2"],
    iSRcountries = [
      "#img-sex-region2-layer3 > g:nth-child(2)",
      "#img-sex-region2-layer4 > g:nth-child(2)",
      "#img-sex-region2-layer5 > g:nth-child(2)",
      "#img-sex-region2-layer6 > g:nth-child(2)",
      "#img-sex-region2-layer7 > g:nth-child(2)",
      "#img-sex-region2-layer8 > g:nth-child(2)",
      "#img-sex-region2-layer9 > g:nth-child(2)"
    ],
    iSRstats2 = [
      "#img-sex-region2-layer3 > g:nth-child(1)",
      "#img-sex-region2-layer4 > g:nth-child(1)",
      "#img-sex-region2-layer5 > g:nth-child(1)",
      "#img-sex-region2-layer6 > g:nth-child(1)",
      "#img-sex-region2-layer7 > g:nth-child(1)",
      "#img-sex-region2-layer8 > g:nth-child(1)",
      "#img-sex-region2-layer9 > g:nth-child(1)"
    ];

  imgBySexRegion.from(iSRmain[0], 1, {
      opacity: 0,
      y: 20,
      transformOrigin: '50% 50%',
      ease: Power3.easeOut
    })
    .from(iSRmain[1], 1, {
      opacity: 0,
      y: 20,
      transformOrigin: '50% 50%',
      ease: Power3.easeOut
    }, '-=0.2')
    .staggerFrom(iSRcountries, 1, {
      opacity: 0,
      ease: Power4.easOut,
      x: -20,
    }, 0.2, "-=1.5")
    .staggerFrom(iSRstats2, 1, {
      opacity: 0,
      transformOrigin: '50% 50%',
      x: 20,
      ease: Power4.easOut
    }, 0.2, "-=2.5");


    $('#type-tab').on('click', function() {
      imgByType.restart();
    });
    $('#region-tab').on('click', function() {
      imgByRegion.restart();
    });
    $('#sex-tab').on('click', function() {
      imgBySex.restart();
    });
    $('#type-region-tab').on('click', function() {
      imgByTypeRegion.restart();
    });
    $('#workforce-tab').on('click', function() {
      imgByWorkforce.restart();
    });
    $('#sex-region-tab').on('click', function() {
      imgBySexRegion.restart();
    });

    // Hand animation
    TweenMax.fromTo(".icon-hand-anim", 0.6, {
      opacity: 0.5,
      y: 5,
      repeat: -1,
      yoyo: true,
      ease: Power1.easeInOut
    }, {
      opacity: 1,
      y: -5,
      repeat: -1,
      yoyo: true,
      ease: Power1.easeInOut
    });

    // Circle Animation
    TweenMax.set('#unv-circle', {
      y: '-25%'
    });
    TweenMax.to('#unv-circle', 70, {
      rotation: 360,
      repeat: -1,
      ease: Power0.easeNone
    });

  }

  /// --- end
});
