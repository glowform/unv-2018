<!DOCTYPE html>

<?php

$params = array();
if(count($_GET) > 0) {
    $params = $_GET;
} else {
    $params = $_POST;
}
// defaults
if($params['type'] == "") $params['type'] = "website";
//if($params['locale'] == "") $params['locale'] = "en_US";
if($params['title'] == "") $params['title'] = "Volunteerism and community resilience";
if($params['image'] == "") $params['image'] = "bg_chapter1";
if($params['description'] == "") $params['description'] = "2018 State of the World’s Volunteerism Report";

?>

<html lang="en">

<head>
   <!-- Meta Tags For Seo + Page Optimization -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="2018 State of the world's volunteerism report">
   <meta name="author" content="">

   <meta property="fb:app_id" content="" />
    <meta property="og:site_name" content="meta site name"/>
    <meta property="og:url" content="https://www.unv-swvr2018.org/index.php?type=<?php echo $params['type']; ?>&title=<?php echo $params['title']; ?>&image=<?php echo $params['image']; ?>&description=<?php echo $params['description']; ?>"/>
    <meta property="og:type" content="<?php echo $params['type']; ?>"/>
    <!-- <meta property="og:locale" content="<?php echo $params['locale']; ?>"/> -->
    <meta property="og:title" content="<?php echo $params['title']; ?>"/>
    <meta property="og:image" content="https://www.unv-swvr2018.org/images/<?php echo $params['image']; ?>.jpg"/>
    <meta property="og:description" content="<?php echo $params['description']; ?>"/>

   <!-- Insert Favicon Here -->
   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
   <link rel="icon" href="images/favicon.ico" type="image/x-icon">

   <!-- Page Title(Name)-->
   <title>UNV - 2018 State of the world's volunteerism report</title>


   <!-- Bootstrap CSS Files -->
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/bootstrap-reboot.min.css">

   <!-- Animate CSS File -->
   <link rel="stylesheet" href="css/animate.css">

   <!-- Font-Awesome CSS File -->
   <link rel="stylesheet" href="css/fontawesome-all.min.css">

   <!-- Fancybox CSS File -->
   <link rel="stylesheet" href="css/jquery.fancybox.min.css">

   <!-- Swiper CSS File -->
   <link rel="stylesheet" href="css/swiper.min.css">

   <!-- Custom Style CSS File -->
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="css/custom-animations.css">
   

</head>

<body onload="threadsInit();" data-spy="scroll" data-target=".navbar" data-offset="50" class="index-one">

<!-- Page Loader -->
   <div class="preloader">
      <div class="loader-dot-outer">
         <div class="loader-dot-center"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
      </div>
   </div>
   <!-- Page Loader -->


   <!-- Page Wrapper -->
   <section class="page-wrapper">

      <!-- Header Section Starts -->
      <header class="site-header">
         <nav class="navbar navbar-expand-lg navbar-transparent-white">
            <div class="container nav-logo-detail-outer">
               <!--side menu open button-->



               <a class="navbar-brand" href="index.php">
                  <img src="images/logo.svg" alt="logo">
               </a>

               <div class="collapse navbar-collapse d-sm-0 d-md-0">
                  <ul class="navbar-nav ml-auto">
                     <li class="nav-item">
                        <a class="nav-link scroll active" href="#overview">Overview</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-1">Ch. 1</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-2">Ch. 2</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-3">Ch. 3</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-4">Ch. 4</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#sharing">Findings</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="http://unv.org/swvr" target="_blank">SWVR Project</a>
                     </li>
                  </ul>
               </div>

               <div class="lang" class="right">
                  <a href="index_fr.php">FR </a> | <a href="index_sp.php">ES</a>
               </div>

               <div id="menu_bars" class="right"> <span></span> <span></span> <span></span> </div>

            <div class="sidebar_menu">
               <nav class="side-nav side-nav-right">
                  <span class="title mobile">NAVIGATION</span>
                  <ul class="side-nav-list mobile">
                     <li class="nav-item">
                        <a class="nav-link scroll active" href="#overview">Overview</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-1">Chapter 1</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-2">Chapter 2</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-3">Chapter 3</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-4">Chapter 4</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#sharing">Findings</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="http://unv.org/swvr" target="_blank">SWVR Project</a>
                     </li>
                  </ul>

                  <span class="title">DOWNLOADS</span>
                  <ul class="side-nav-list">
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_WEB.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Full report</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_WEB_OVERVIEW.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Overview</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_WEB_CH1+INTRO.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Introduction and Chapter 1</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_WEB_CH2.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapter 2</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_WEB_CH3.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapter 3</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_WEB_CH4+CONC.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapter 4 and Conclusion</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_WEB_ANNEX+NOTES+REFERENCES.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Annexes</a>
                     </li>

                  </ul>


                  <div class="lang" class="right">
                     <a href="index_fr.php">FR </a> | <a href="index_sp.php">ES</a>
                  </div>

                  <div class="bottom-share">
                     <ul class="social-icons padding-bottom-15">
                        <!-- <li>
                           <a href="https://www.google.com/"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-google"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-twitter"></i></a>
                        </li> -->
                     </ul>
                     <p></p>
                  </div>
               </nav>
            </div>
         </div>
         </nav>

      </header>

      <section id="top">
            <div id="header">
                  <div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:1600px; height:720px">
                        <canvas id="canvas" width="1600" height="720" style="position: absolute; display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                        <div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:1600px; height:720px; position: absolute; left: 0px; top: 0px; display: block;">
                        </div>
                  </div>
                  <div id="docDialog">
                        <p class="dlgHeader">2018 State of the World’s Volunteerism Report </p>
                        <p class="dlgTitle">The <span class="text-color-blue">thread</span> that binds</p>
                        <p class="dlgText">Volunteerism and community resilience</p>
                        <div class="buttons">
                              <a href="files/51692_UNV_SWVR_2018_WEB.pdf" target="_blank" class="button">Download the report</a>
                              <a href="files/51692_UNV_SWVR_2018_WEB_OVERVIEW.pdf" target="_blank" class="button">Download the overview</a>
                        </div>
                  </div>
            </div>


            <script>
                  var H_WIDTH = 1600;
                  var H_HEIGHT = 720;

                  window.addEventListener('resize', onResize);
                  onResize();

                  function onResize() {
                        var header = document.getElementById('header');
                        var container = document.getElementById('animation_container');

                        var w = header.offsetWidth;
                        var scFix = (w < H_WIDTH) ? 1 : w / H_WIDTH;

                        container.style.transform = 'translate(-50%, -50%) scale(' + scFix + ')';
                  }
            </script>
      </section>


      <section class="background-white padding-top-bottom" id="overview">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 o-2">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="paragraph-15 padding-bottom-25 text-color-blue spacing">OVERVIEW</p>
                           <h2 class="font-weight-700 padding-bottom-25"><span class="text-color-blue">Weaving patterns</span> of resilience </h2>
                           <p class="padding-bottom-20">Volunteers are at the forefront of every major conflict, natural disaster and other acute shock. Less visibly, everyday and everywhere, all types of people are volunteering to tackle stresses that test their resilience, such as poor education, ill health and poverty. In many communities, particularly where public support and safety nets are absent, <strong>volunteering emerges as a fundamental survival strategy, because it enables collective strategies for managing risk.</strong> </p>

                           <p class="padding-bottom-15">The <strong>2018 State of the World’s Volunteerism Report, <em>The thread that binds: Volunteerism and community resilience</em></strong>, looks at how the distinctive characteristics of local volunteerism help or hinder communities in crisis. For the first time, the report draws on <a href="http://www.unv.org/swvr/research" target="_blank"><u>field research</u></a> carried out by volunteers with 1200 participants in 15 diverse communities. The report also explores how governments and other development actors can partner with locally owned solutions to strengthen community resilience.</p>

                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-1 col-md-12 offset-md-0 offset-sm-0 padding-top-60 o-1">
                        <div class="wow fadeInUp mobile-margin" data-wow-delay="500ms">
                           <a href="https://www.youtube.com/watch?v=Elb6r2tEEb4" data-fancybox class="">
                              <img src="images/image_video.png" alt="">
                              <!-- <i class="fa fa-play"></i> -->
                           </a>
                           <p class="paragraph-15 text-center padding-top-20"><em>Watch the SWVR 2018 video animation</em></p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-blue padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">

                     <div class="col-lg-7 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp mobile-margin" data-wow-delay="500ms">
                           <img src="images/image_findings.png" width="" alt="">
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="300ms">

                           <p class="text-color-blue paragraph-60 fontface-two padding-bottom-20 padding-top-15">Key Findings</p>

                           <p class="">Read about the key findings of the report, and share them among your networks.</p>

                           <a href="#sharing" class="btn btn-blue padding-top-50">Key Findings</a>

                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-1" data-parallax="scrol" data-image-src="images/bg_chapter1.jpg" id="">
         <div class="container">

            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CHAPTER</p>
                           <p class="number text-color-orange">01</p>
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Volunteerism as a global development asset</h2>
                           <p class="paragraph-17 text-color-white"><em>Who are the world’s volunteers? What do global estimates and trends tell us about volunteerism in 2018? </em></p>

                        </div>
                     </div>

                     <div class="col-lg-4 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_WEB_CH1+INTRO.pdf" target="_blank"" class="btn">Download Introduction and Chapter 1</a>
                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-orange paragraph-38 fontface-two padding-bottom-20">“As we seek to build capacities and to help the new agenda to take root, volunteerism can be another powerful and cross-cutting means of implementation." <a href="http://www.un.org/ga/search/view_doc.asp?symbol=A/69/700&Lang=E" target="_blank"><span class="text-color-dark">- Road to Dignity by 2030 (A/69/700)</span></a></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-orange paragraph-20"><em>Volunteerism forms part of the fabric of all societies. It can be a critical resource for peace and development, yet more evidence is needed to understand the distinctive value of volunteer contributions to the economy and society, particularly in fragile contexts.</em></p>

                           <p class="padding-bottom-10">Analysis by the United Nations Volunteers (UNV) programme shows that the efforts of the more than 1 billion volunteers around the world are equal to that of <a class="scroll" href="#scale-scope"><u>109 million full-time workers</u></a>.</p>
                           <p class="padding-bottom-10">To put this in context, the volunteer workforce is nearly three times the number of those employed globally in financial services, and more than five times of those working in mining and extractive industries.</p>
                           <p class="padding-bottom-10"><strong>Volunteerism is universal and people of all ages and backgrounds volunteer.</strong> Access to different types of volunteering opportunities is likely to differ, however, by nationality, income, gender and social status. The vast majority of volunteer work, 70%, does not involve any organization but happens informally between people in their communities. Globally, women take on the majority of volunteer work, 57%, and an even bigger share of informal volunteering, 59%, often as an extension of unpaid care roles.</p>
                           <p class="padding-bottom-10"><strong>Social, environmental and technological changes are all influencing volunteering in 2018.</strong> More than 90 countries around the world now have policies or legislation on volunteering. Younger generations are developing and engaging in new forms of issue-based volunteering, often using digital technologies to connect. This looks quite different from the regular, local volunteer work that their parents did in their communities. </p>
                           <p class="padding-bottom-10">Though more efforts are needed to increase data and evidence on volunteering by governments and others, these numbers, trends and patterns are a starting point for better understanding the impact of volunteering on global peace and development issues in 2018.</p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="padding-top-bottom" id="scale-scope">
         <div class="container">
            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <p class="padding-bottom-35 paragraph-24 font-weight-700">Scale and scope of volunteerism globally</p>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link  active" id="type-tab" data-toggle="tab" href="#type" role="tab" aria-controls="type" aria-selected="false">By type</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="region-tab" data-toggle="tab" href="#region" role="tab" aria-controls="region" aria-selected="false">By region</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="type-region-tab" data-toggle="tab" href="#type-region" role="tab" aria-controls="type-region" aria-selected="false">By type and region</a>
                           </li>

                          <li class="nav-item">
                              <a class="nav-link" id="sex-tab" data-toggle="tab" href="#sex" role="tab" aria-controls="sex" aria-selected="false">By sex</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="sex-region-tab" data-toggle="tab" href="#sex-region" role="tab" aria-controls="sex-region" aria-selected="false">By sex and type</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="workforce-tab" data-toggle="tab" href="#workforce" role="tab" aria-controls="workforce" aria-selected="true">By workforce</a>
                          </li>



                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade show active" id="type" role="tabpanel" aria-labelledby="type-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Majority of volunteering globally happens informally</p>

                              <a href="files/bytype.pdf" target="_blank" class="dwn">Download figure</a>
                          </div>

                          <div class="tab-pane fade" id="region" role="tabpanel" aria-labelledby="region-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Total full-time equivalent volunteering by region</p>
                              <a href="files/byregion.pdf" target="_blank" class="dwn">Download figure</a>
                          </div>

                          <div class="tab-pane fade" id="type-region" role="tabpanel" aria-labelledby="type-region-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Formal and informal volunteer work as a percentage of total volunteering by region</p>
                              <a href="files/bytypeandregion.pdf" target="_blank" class="dwn">Download figure</a>
                          </div>

                          <div class="tab-pane fade" id="sex" role="tabpanel" aria-labelledby="sex-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Women's share of total volunteering is higher across all regions except Asia and the Pacific</p>
                             <a href="files/bysex.pdf" target="_blank" class="dwn">Download figure</a>
                          </div>

                          <div class="tab-pane fade" id="sex-region" role="tabpanel" aria-labelledby="sex-region-tab">
                              <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Women take on the majority share of informal volunteering across all regions</p>
                             <a href="files/bysexandregion.pdf" target="_blank" class="dwn">Download figure</a>
                          </div>
                          <div class="tab-pane fade" id="workforce" role="tabpanel" aria-labelledby="workforce-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">The global volunteer workforce exceeds the number of people employed in more than half of the 10 most populous countries, 2016</p>
                             <a href="files/byworkforce.pdf" target="_blank" class="dwn">Download figure</a>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-2" data-parallax="scrol" data-image-src="images/bg_chapter2.jpg"  id="">
         <div class="container">

            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CHAPTER</p>
                           <p class="number text-color-red">02</p>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Local volunteerism in communities under strain</h2>
                           <p class="paragraph-17 text-color-white"><em>Volunteering in communities encompasses a broad array of activities to support community resilience. But beyond the immense scale and scope of volunteer efforts, do communities in crisis value volunteering, and why?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms ">
                           <a href="files/51692_UNV_SWVR_2018_WEB_CH2.pdf" target="_blank" class="btn border-red">Download Chapter 2</a>
                        </div>
                     </div>


                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-red paragraph-38 fontface-two padding-bottom-20">“<span class="paragraph-50">C</span>ommunity volunteers were the only ones putting their hands up... there were also a lot who said, ‘No-one else will do this. It has to be us. This is our community’." <span class="text-color-dark">– Red <span class="paragraph-50">C</span>ross aid worker</span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-red paragraph-20"><em>Community members feel that volunteer work has two distinctive values that helps them to be more resilient:</em></p>

                           <p class="padding-bottom-10"> <span class="bg-red">Firstly,</span> all types of communities value volunteering because it gives people the <span class="font-weight-700">ability to <span class="text-color-red">self-organize</span> around their own priorities</span>. By coming together to volunteer, individuals can create collective strategies for dealing with risk. This is important in times of crisis for the speed of response, flexibility, ownership and agency of efforts, particularly in the most isolated communities where other types of support are limited. In other contexts, self-organization is more important for marginalized groups.</p> 
                           <p class="padding-bottom-10"><span class="bg-red">Secondly,</span> by its nature, <span class="font-weight-700">volunteerism is formed around <span class="text-color-red">human connections</span></span>. Research participants highlighted that the solidarity, empathy and connections generated through social action are a protective factor when times are hard. Human connections and networks enable communities to share information on risks, to reach out to their most vulnerable members, and to act based on empathy and shared values. </p>
                           <p class="padding-bottom-10">The vast majority of volunteers are working day in, day out in their own communities. But because volunteerism is part and parcel of communities under strain, it means that it also cannot be romanticized. In some contexts, volunteering might have a less positive impact on resilience.  </p>
                           <p class="padding-bottom-10">For example, local volunteer efforts can be effective at responding to shocks and stresses, but with insufficient resources they may not help communities prevent crises and to adapt. New and emerging risks such as increasing climate variability are also putting traditional volunteering strategies under strain.</p>
                           <p class="padding-bottom-10">Furthermore, because volunteering is rooted in social relationships, volunteering can also be exclusive, exploitative and burden the most vulnerable. Power dynamics within and across communities mean that women, and marginalized groups are often taking on the majority of lower-status volunteering.  At the same time, volunteer roles that increase skills and leadership opportunities are not available to all.  </p>
                           <p class="padding-bottom-10">These findings challenge the assumption that focusing on the local will automatically enhance participation and empower volunteer groups in a transformative way. To help communities bounce back, it is important to create an environment that recognizes and maximizes the most positive characteristics of volunteerism, and addresses its challenges.</p>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>



      <section class="padding-top-bottom bg-setting"  id="foldout">
         <div class="container">

            <div class="">
               <div class="">

                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="paragraph-24 font-weight-700 text-color-darkblue text-center padding-bottom-20">What communities value about volunteerism for resilience</p>
                           <p class="text-center padding-bottom-20">This graphic shows how the human connections and self-organization characteristics of local volunteerism enhance or limit community resilience.</p>
                           <img class="image-hand padding-bottom-70" src="images/icon_hand2.svg" alt="">
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <div class="list-box red text-color-white">
                              <div class="list-box-top">
                                 <h4>Human Connections</h4>
                              </div>
                              <div class="row list-box-bot">
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">Positive Contributions</h5>
                                    <ul>
                                       <li><span>Trust</span>
                                       <p>A high level of trust among volunteers is linked to enhanced collective action.</p></li>
                                       <li><span>Solidarity</span>
                                       <p>Voluntary action can enhance solidarity or “power with others” through mutual assistance.</p></li>
                                       <li><span>Cohesion</span>
                                       <p>Voluntary action helps renegotiate relationships between groups that have been divided and encourages the formation of networks of people with shared causes.</p></li>
                                       <li><span>Emotional support</span>
                                       <p>Community-based volunteers are likely to identify with and help those who are suffering, which can reduce feelings of alienation and isolation.</p></li>
                                       <li><span>Local access</span>
                                       <p>Local volunteers have linkages and access to vulnerable groups.</p></li>
                                       <li><span>Contextual knowledge</span>
                                       <p>Local volunteers can contextualize information about the community for external actors.</p></li>
                                       <li><span>Links to wider networks</span>
                                       <p>When local volunteers are organized, they can play a bridging function between local and national or international actors.</p></li>
                                    </ul>
                                 </div>
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">Limits and threats</h5>
                                    <ul>
                                       <li><span>Short-term vision</span>
                                       <p>Volunteerism based on social solidarity and emotional ties may prioritize immediate and urgent needs over long-term prevention and adaptation.</p></li>
                                       <li><span>Exclusion</span>
                                       <p>Solidarity and collective voluntary action can lead to the exclusion of out-groups.</p></li>
                                       <li><span>Division</span>
                                       <p>Facing stresses, there are few incentives for local volunteer groups to embrace people with different identities or divergent views.</p></li>
                                       <li><span>Neglect of local voices</span>
                                       <p>Voluntary relations are often focused internally, and power imbalances and lack of affiliation can limit the uptake of volunteers’ local knowledge.</p></li>
                                       <li><span>Internal conflict</span>
                                       <p>Volunteer groups composed of marginalized populations can cause intercommunity conflict when they organize against broader community decisions or disrupt the status quo.</p></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <div class="list-box blue text-color-white">
                              <div class="list-box-top">
                                 <h4>Self-organization</h4>
                              </div>
                              <div class="row list-box-bot">
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">Positive Contributions</h5>
                                    <ul>
                                       <li><span>Speed and immediacy</span>
                                       <p>Local volunteers provide frontline and immediate first response in a crisis.</p></li>
                                       <li><span>Scale</span>
                                       <p>Spontaneous volunteering can mobilize large numbers of people during a crisis; wide geographic dispersion of volunteers enables early recognition of threats.</p></li>
                                       <li><span>Availability</span>
                                       <p>Local volunteers are often the only sources of help available in a crisis and can organize when centralized authorities are unavailable to guide and coordinate an emergency response.</p></li>
                                       <li><span>Flexibility</span>
                                       <p>Informal local voluntary action is less tied to standard methods and procedures and can more readily adapt to changing local conditions.</p></li>
                                       <li><span>Innovation</span>
                                       <p>Local volunteers often problem-solve based on immediate needs and resources.</p></li>
                                       <li><span>Ownership</span>
                                       <p>Self-determined priorities and limited control by external actors foster a voluntary response and ownership of solutions.</p></li>
                                       <li><span>Cost-effectiveness</span>
                                       <p>Efforts to organize draw on the available and in-kind resources of volunteers.</p></li>
                                    </ul>
                                 </div>
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">Limits and threats</h5>
                                    <ul>
                                       <li><span>Exploitation</span>
                                          <p>Local volunteers who organize to meet particular needs can be used as low-cost labour with insufficient compensation or support.</p><li>
                                       <li><span>Substitutive</span>
                                          <p>Local volunteers fill gaps in government services, potentially discouraging public investment.</p><li>
                                       <li><span>Compulsory</span>
                                          <p>Some local community resilience strategies require “voluntary participation”, with people who fail to participate being fined, shunned socially or denied access to collectively produced goods or services.</p><li>
                                       <li><span>Scale</span>
                                          <p>In some contexts, self-organization can mean an inability to effectively use large numbers of local volunteers during crises.</p><li>
                                       <li><span>Isolated</span>
                                          <p>Volunteers not connected to mainstream services are dependent on local resources.</p><li>
                                       <li><span>Segmentation</span>
                                          <p>Local volunteering is often a survival strategy for vulnerable or minority groups that self-organize to meet specific needs that are not being met by the wider community. This may not counter processes of marginalization and instead increase the burden on the most vulnerable.</p><li>
                                    </ul>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </div>

                  </div>


               </div>
            </div>
         </div>
      </section>





      <section class="bg-setting height-100 chapter" id="chapter-3" data-parallax="scrol" data-image-src="images/bg_chapter3.jpg" id="">
         <div class="container">
            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CHAPTER</p>
                           <p class="number text-color-green">03</p>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Collaborations with volunteerism for community resilience</h2>
                           <p class="paragraph-17 text-color-white"><em>If local volunteerism is a fundamental resilience strategy, the manner in which external actors engage with it matters. How can governments and their development partners best complement local voluntary action?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_WEB_CH3.pdf" target="_blank" class="btn border-green">Download Chapter 3</a>
                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>



      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-green paragraph-38 fontface-two padding-bottom-20">“As volunteers, we can easily see the limits of what we do. We lack the needed resources; we really need external assistance in case of crisis." <span class="text-color-dark">– Focus group participant, Burundi, SWVR field research</span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-green paragraph-20"><em>When local capacities are pushed to their limits or when shocks and stresses originate from outside their communities, collaboration between external actors and local volunteerism becomes important. </em></p>

                           <p class="padding-bottom-10">Directly or indirectly, many governments and their development partners are already influencing local volunteerism. Policies, programmes, norms and standards all impact on the ability of volunteering to strengthen resilience. </p>
                           <p class="padding-bottom-10">Done well, collaborations can be highly complementary, enabling local volunteers to move from merely coping, to preventing shocks and helping communities to adapt. Partnerships can also help embed more inclusive and equitable norms and standards that allow all types of people to benefit from volunteer opportunities.</p>
                           <p class="padding-bottom-10">Conversely, when actors engage with local volunteerism as simply a cheap and readily available resource, local volunteerism may be undermined. Volunteers may become over-burdened, exploited, and have little opportunity to nurture their distinctive contributions such as local knowledge, human-centred connections, agency, autonomy and flexibility. <strong>Volunteerism itself may become less resilient.</strong></p>
                           <p class="padding-bottom-10">Partnerships between local volunteers and external actors should be structured in a spirit of true collaboration that builds on the strengths of volunteerism for resilience. Giving sufficient space to local volunteers to come together to innovate and solve problems, while linking and supporting their efforts to those of complementary actors, is key.  </p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="padding-top-bottom" id="examples-tabs">
         <div class="container">
            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <p class="padding-bottom-35 paragraph-24 font-weight-700">Examples of effective collaboration with local volunteerism</p>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link  active" id="health-tab" data-toggle="tab" href="#health" role="tab" aria-controls="online" aria-selected="false">Health promotion</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="online-tab" data-toggle="tab" href="#online" role="tab" aria-controls="online" aria-selected="false">Online volunteering</a>
                           </li>

                          <li class="nav-item">
                              <a class="nav-link" id="opensource-tab" data-toggle="tab" href="#opensource" role="tab" aria-controls="opensource" aria-selected="false">Open-source mapping</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="environmental-tab" data-toggle="tab" href="#environmental" role="tab" aria-controls="environmental" aria-selected="false">Environmental protection</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="data" data-toggle="tab" href="#collection" role="tab" aria-controls="collection" aria-selected="true">Data collection</a>
                          </li>



                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade show active" id="health" role="tabpanel" aria-labelledby="health-tab">
                              <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">The critical role of voluntary community health promotion workers</p>
                                    <img src="images/1_health_promotion.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Health promotion volunteers were active in nearly every low-income field research community, particularly in remote and vulnerable areas beyond the reach of state services. These volunteers transmit information about nutrition, maternal and child health, reproductive health and other areas of primary health care and disease prevention. They are often perceived as having a better understanding of the needs and problems of the community than medical professionals from the state health service.</p>

                                    <p class="padding-bottom-10">Despite these benefits, the health promotion volunteers struggled to do their work. Most received initial training and support from the government or development agencies, but they commonly reported having to end their health promotion activities soon after due to a lack of support. Volunteers who managed to continue often did so at considerable personal cost. As one of the many volunteers from the field research community in Guatemala described their situation:</p>

                                    <p><em>Why doesn’t the government give us more support? Imagine that we’re doing this job, saving lives, yet there is no incentive. I pay for my transportation myself. When I started, I bought my scissors, a gabacha [apron], a pot for boiling water and an umbrella because sometimes we have to go out in the rain, a backpack, a pair of boots. We just pay for it ourselves. But what can we do when the mothers themselves come and look for us?</em></p>
                                 </div>
                              </div>
                           </div>

                          <div class="tab-pane fade" id="online" role="tabpanel" aria-labelledby="online-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Linking diverse skills and knowledge through online volunteering</p>
                                    <img src="images/2_online_volunteering.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">The United Nations Volunteers programme manages the <a href="www.onlinevolunteering.org" target="_blank">UN Online Volunteering service</a>, a dedicated platform that mobilizes more than 12,000 online volunteers every year. Online volunteering is a simple, universal and effective way for organizations and volunteers to work together to address sustainable development challenges anywhere in the world from any device.</p>

                                    <p class="padding-bottom-10">Since June 2014, UN Online Volunteers have been providing technical support to Cameroon’s Agriculteurs Professionels du Cameroun, a rural development project in Tayap village in the Congo Basin, an area that has suffered widespread habitat and biodiversity loss. The project aims to promote sustainable livelihoods and community resilience. The UN Online Volunteers include: an information technology expert from Burkina Faso who is creating maps of the village; an agricultural engineer from Togo who analyses satellite images of forest coverage; and a renewable energy expert from France who is developing a solar energy project for the village. The sustained multidisciplinary support provided by these international online volunteers has been critical to the success of the project, which has won several awards and grants.</p>

                                 </div>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="opensource" role="tabpanel" aria-labelledby="opensource-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Using open-source software to monitor and report during crises</p>
                                    <img src="images/3_online_mapping.png" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Open-source mapping software is a powerful tool for volunteers responding to crises. Ushahidi is an open-source platform that has enabled voluntary participation in data mapping for over a decade. Launched in 2007 to track reports of post-election violence in Kenya, Ushahidi has been refined by volunteers and expanded to other uses and contexts. People used the platform to monitor and report on voting during the 2017 general election in Kenya, including reporting on voter suppression, ballot problems and cases of violence. </p>

                                    <p class="padding-bottom-10">Building from this model, open-source software is now increasingly employed in emergencies around the world.  For example, during the 2017 earthquake in Mexico, thousands of volunteers translated thousands of text messages and social media posts from people needing help. Volunteers were able to geolocate these messages, tag their location and communicate the mapped information to responders on the ground. There are similar accounts of how open-source software has helped communities to cope with and recover from other recent crises such as the 2015 earthquake in Nepal, the 2014–16 Ebola outbreak in West Africa, violence in the Syrian civil war and hurricanes Harvey and Irma in 2017.</p>

                                 </div>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="environmental" role="tabpanel" aria-labelledby="envirionmental-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Cross-community volunteering to protect shared natural resources</p>
                                    <img src="images/4_environmental_protection.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Many risks that affect resilience cross community boundaries. Effective management of these risks therefore demands cooperation between communities. Volunteering is one way to achieve this.</p>

                                    <p class="padding-bottom-10">In Sudan, the Wadi El Ku Catchment Management Project works with several communities surrounding the most important water source in arid North Darfur. Initiated by UN Environment together with the Darfur Regional Authority and the Government of North Darfur State, and funded by the European Union, the project has mobilized strong cultural norms of collaboration in working with volunteers from the different communities to assess water levels, provide basic services and advocate for a holistic and cooperative approach to natural resource management. In this way, volunteers help to link and improve relations between neighbouring communities that share such a key natural resource.</p>

                                    <p>In Myanmar, volunteers from six creek-side villages formed the Creek Network to deal with the problem of pollution from illegal gold mining, which was affecting people’s health and livelihoods and the environment. Over two years, the Creek Network worked with local administrations to confront illegal gold miners. With support from non-governmental organizations, volunteers learned how to sample and test creek water, document mining violations and report findings to the authorities. They succeeded in having the illegal mines shut down and subsequently monitored the creek on a regular basis. The Creek Network has now become part of national and regional networks and has shared its experiences with other communities facing similar problems.</p>
                                 </div>
                              </div>
                          </div>
                          <div class="tab-pane fade" id="collection" role="tabpanel" aria-labelledby="collection-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Data collected by volunteers hold polluters to account in China</p>
                                    <img src="images/5_data_collection.jpg" width="745px" alt="" class="desktop">

                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Across the world, communities face severe environmental challenges that threaten human health and livelihoods. The Chinese environmental NGO Friends of Nature works with local volunteers to map and monitor environmental risks at the community level. Friends of Nature has initiated more than 30 legal cases against polluting factories and industries. These legal challenges have built on evidence collected by volunteers that relies on their local knowledge, connections and flexibility and is coordinated through new mobile and smart technologies. This volunteer-led model has inspired other environmental NGOs and demonstrated to policymakers and local authorities the value of working with volunteers on environmental protection.</p>

                                 </div>
                              </div>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-4" data-parallax="scrol" data-image-src="images/bg_chapter4.jpg" id="">
         <div class="container">
            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CHAPTER</p>
                           <p class="number text-color-aqua">04</p>
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Volunteerism as a renewable resource</h2>
                           <p class="paragraph-17 text-color-white"><em>This report demonstrates that the world’s 1 billion volunteers are not just a major resource in times of crisis, but that volunteerism is itself a property of resilient communities. So how can both resilience and volunteerism be nurtured together over the longer-term?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-4 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_WEB_CH4+CONC.pdf" target="_blank" class="btn border-aqua">Download Chapter 4 and Conclusion</a>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-aqua paragraph-38 fontface-two padding-bottom-20">“This work can’t be measured by a financial ruler. We know what we are doing – we value ourselves.” <span class="text-color-dark">– Local volunteer, Myanmar</span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-aqua paragraph-20"><em>Like other forms of civic participation, volunteerism is both a means and an end of development. This duality means that while voluntary action can be a renewable resource and a positive force for inclusive and equitable development, it can also squander the resources of the most vulnerable people or be exploited by external actors. </em></p>

                           <p class="padding-bottom-10">Policies and practice need to focus on ensuring both mutually reinforcing ‘volunteerism for resilience’ and ‘resilient volunteering’.  This can be achieved in several ways: </p> 
                           <p class="padding-bottom-10"><span class="bg-aqua">Firstly,</span> by recognizing that volunteerism is a fundamental resource for resilience in all communities. This means that there is a need to move from ad-hoc and ‘project by project’ approaches that work with volunteers, to a national infrastructure for volunteerism that enables all citizens to contribute and to partner with others.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">Secondly,</span>  by ensuring a fairer distribution of resources between actors. Where volunteers are bearing the brunt of the risks, technical and financial support should be more readily available at the local level to sustain volunteer efforts and to avoid depleting the volunteers’ limited resources.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">Thirdly,</span>  by better integrating volunteerism into national resilience strategies and plans, in ways that give volunteers a voice in decision-making based on their own knowledge and contributions, rather than being expected to implement others’ priorities in a top-down way.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">Finally,</span>  by working to shape a more equitable distribution of volunteer labour within and across communities, including opening up development and leadership opportunities for disadvantaged and marginalized groups.</p>
                           <p class="padding-bottom-10">Without such efforts, local volunteerism is unlikely to be sustainable, especially where the burden of community coping is disproportionately borne by the most vulnerable. This report provides an alternative vision for governments and their development partners – one where volunteerism continues to be a core property and protective factor for resilient communities everywhere.</p>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="resilience-boxes">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 text-center">
                        <div class="" data-wow-delay="300ms">
                           <p class="paragraph-24 text-color-darkblue  font-weight-700 padding-bottom-10">Optimizing the relationship between volunteerism and resilience</p>
                           <p class="padding-bottom-40">The figure below maps the conditions that help or hinder volunteerism and resilience to be mutually reinforcing</p>
                           <p class="padding-bottom-40 desktop icon-hand-here">
                           </p>

                           <img class="mobile" src="images/image_optimizing.svg" width="945px" alt="">
                        </div>
                     </div>

                  </div>
                  <div class="row arrows-top desktop">
                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box1">
                        <div class=" bg-rect-blue1 flip-container" data-wow-delay="300ms" ontouchstart="this.classList.toggle('hover');">
                           <div class="perspective-fix"></div>
                               <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-plus">More resilient <br>communities</span>
                                 <span class="text-color-white font-weight-700 white-minus">Less resilient <br>volunteerism</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-70">
                                    <li><span>The contribution of local volunteers is recognized and integrated into national and sub-national resilience strategies.</span></li>
                                    <li><span>Volunteer work is resourced but on a time-bound, project-by-project basis. </span></li>
                                    <li><span>Speed and availability of local volunteers help communities in crisis.</span></li>
                                    <li><span>Local voluntary efforts are utilized in external actors’ projects and initiatives in a top-down way with minimal voice and upward feedback. </span></li>
                                    <li><span>The space for volunteer action is highly controlled by governments and other actors. </span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box2">
                        <div class=" bg-rect-blue2 flip-container" data-wow-delay="400ms">
                           <div class="perspective-fix"></div>
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-plus">More resilient <br>communities</span>
                                 <span class="text-color-white font-weight-700 white-plus">More resilient <br>volunteerism</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-15">
                                    <li><span>Complementary roles are established between different resilience actors based on their strengths and contributions.</span></li>
                                    <li><span>Partnerships are more strategic and cross-sectoral rather than ad-hoc and time-limited projects that involve local volunteers.</span></li>
                                    <li><span>Approaches are built from volunteers’ own priorities rather than using them as implementers of top-down initiatives.</span></li>
                                    <li><span>The focus of volunteer engagement covers prevention and adaptation, and long-term issues rather than immediate response and mitigation.</span></li>
                                    <li><span>The distribution of resources is commensurate with responsibilities, giving greater control to local volunteers who are taking on the burden of work. </span></li>
                                    <li><span>Inclusive volunteering opportunities are created to benefit marginalized groups, such as specific schemes and initiatives. </span></li>
                                    <li><span>Connections are created across different groups of people volunteering together, rather than with people from the same background.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
                  <div class="row  arrows-bottom desktop">
                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box3">
                        <div class=" bg-rect-blue3 flip-container" data-wow-delay="300ms">
                           <div class="perspective-fix"></div>
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-minus ">Less resilient <br>communities</span>
                                 <span class="text-color-white font-weight-700 white-minus ">Less resilient <br>volunteerism</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-40">
                                    <li><span>Volunteering opportunities are exclusive, for example, the most vulnerable are only involved in lower-status and informal volunteering.</span></li>
                                    <li><span>Volunteering is exploitative, for example, volunteers are expected to carry out roles for other actors but without training, resources, safety and security.</span></li>
                                    <li><span>Volunteering is extractive, for example, external agencies seek inputs and information through early warning systems but do not share back with communities.</span></li>
                                    <li><span>Volunteering is under resourced, further depleting the limited resources of already vulnerable people in communities as they work to manage risks.</span></li>
                                    <li><span>Volunteering is underrecognized and undervalued, and may even be stigmatized.</span></li>
                                    <li><span>Volunteers are focused on short-term actions, such as coping in crises, potentially leading to reduced impact and potential burn-out.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-30 quote-box quote-box4">
                        <div class=" bg-rect-blue4 flip-container" data-wow-delay="400ms">
                           <div class="perspective-fix"></div>
                          
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-minus">Less resilient <br>communities</span>
                                 <span class="text-color-white font-weight-700 white-plus">More resilient <br>volunteerism</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-100">
                                    <li><span>Volunteering creates strong cohesion between people of the same background who volunteer together.</span></li>
                                    <li><span>Communities have the ability to determine priorities but this includes decisions on who they extend help to within and outside the community.</span></li>
                                    <li><span>The lack of coordination between wider actors and local volunteers reduces impact.</span></li>
                                    <li><span>Volunteerism is vibrant but is not integrated into strategies and plans for resilience.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
                  <a href="files/figure4_EN.pdf" target="_blank" class="dwn">Download figure</a>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-top-bottom" id="sharing">
         <div class="container">

            <div class="">
               <div class="section-heading text-center">
                  <div class="row">
                     <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                        <h2 class="text-color-blue fontface-two padding-bottom-5" data-wow-delay="300ms">Share our key findings!</h2>
                     </div>
                  </div>
               </div>
               <div class="padding-top-60 row">

                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card1.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Local volunteerism is a fundamental resilience strategy and a property of resilient communities.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 The scale and scope of volunteer activity in responding to shocks and stresses are unparalleled. Moreover, the contribution of volunteerism goes far beyond its magnitude because, like other types of civic participation, it is both a means to development and an end in itself. </p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Local+volunteerism+is+a+fundamental+resilience+strategy+and+a+property+of+resilient+communities.&image=share_card1'); ?>"><i class="fab fa-linkedin-in"></i></a>

                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Local volunteerism is a fundamental resilience strategy and a property of resilient communities.&description=The scale and scope of volunteer activity in responding to shocks and stresses are unparalleled. Moreover, the contribution of volunteerism goes far beyond its magnitude because, like other types of civic participation, it is both a means to development and an end in itself.&image=share_card1'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Local volunteerism is a fundamental resilience strategy and a property of resilient communities.&description=The scale and scope of volunteer activity in responding to shocks and stresses are unparalleled. Moreover, the contribution of volunteerism goes far beyond its magnitude because, like other types of civic participation, it is both a means to development and an end in itself.&image=share_card1'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Local%20volunteerism%20is%20a%20fundamental%20resilience%20strategy%20and%20a%20property%20of%20resilient%20communities.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card2.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Local volunteerism enables collective strategies for managing risk.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 By bringing together individual actions under a shared purpose, volunteerism expands the choices and opportunities available to communities as they prepare for and respond to crises.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Local+volunteerism+enables+collective+strategies+for+managing+risk.&image=share_card2'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Local volunteerism enables collective strategies for managing risk.&description=By bringing together individual actions under a shared purpose, volunteerism expands the choices and opportunities available to communities as they prepare for and respond to crises.&image=share_card2'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Local volunteerism enables collective strategies for managing risk.&description=By bringing together individual actions under a shared purpose, volunteerism expands the choices and opportunities available to communities as they prepare for and respond to crises.&image=share_card2'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Local%20volunteerism%20enables%20collective%20strategies%20for%20managing%20risk.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card3.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">The characteristics of local volunteerism most valued by communities are the ability to self-organize and to form connections with others. </p>
                              <p class="text-color-darkblue paragraph-15">
                                 Community members appreciate the ability to set their own development priorities and to take ownership of local problems. The networks, trust and empathy generated through social action are acknowledged across all contexts.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The+characteristics+of+local+volunteerism+most+valued+by+communities+are+the+ability+to+self-organize+and+to+form+connections+with+others.&image=share_card3'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The characteristics of local volunteerism most valued by communities are the ability to self-organize and to form connections with others.&description=Community members appreciate the ability to set their own development priorities and to take ownership of local problems. The networks, trust and empathy generated through social action are acknowledged across all contexts.&image=share_card3'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The characteristics of local volunteerism most valued by communities are the ability to self-organize and to form connections with others.&description=Community members appreciate the ability to set their own development priorities and to take ownership of local problems. The networks, trust and empathy generated through social action are acknowledged across all contexts.&image=share_card3'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=The%20characteristics%20of%20local%20volunteerism%20most%20valued%20by%20communities%20are%20the%20ability%20to%20self-organize%20and%20to%20form%20connections%20with%20others.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>


                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card4.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">The distinctive characteristics of local volunteerism can both boost and diminish community resilience under different conditions.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 The duality of volunteering as both a means and an end of development means that each characteristic of volunteerism is potentially positive or negative depending on the context. </p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=These+distinctive+characteristics+of+local+volunteerism+can+both+boost+and+diminish+community+resilience+under+different+conditions.&image=share_card4'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=These distinctive characteristics of local volunteerism can both boost and diminish community resilience under different conditions.&description=The duality of volunteering as both a means and an end of development means that each characteristic of volunteerism is potentially positive or negative depending on the context.&image=share_card4'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=These distinctive characteristics of local volunteerism can both boost and diminish community resilience under different conditions.&description=The duality of volunteering as both a means and an end of development means that each characteristic of volunteerism is potentially positive or negative depending on the context.&image=share_card4'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=These%20distinctive%20characteristics%20of%20local%20volunteerism%20can%20both%20boost%20and%20diminish%20community%20resilience%20under%20different%20conditions.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card5.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Volunteerism is particularly significant for vulnerable and marginalized groups.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Mutual aid, self-help and reciprocity are important coping strategies for isolated and vulnerable communities. Self-organized actions can help marginalized groups meet their own needs in the absence of wider provisions and services.  </p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Volunteerism+is+particularly+significant+for+vulnerable+and+marginalized+groups.&image=share_card5'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Volunteerism is particularly significant for vulnerable and marginalized groups.&description=Mutual aid, self-help and reciprocity are important coping strategies for isolated and vulnerable communities. Self-organized actions can help marginalized groups meet their own needs in the absence of wider provisions and services.&image=share_card5'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Volunteerism is particularly significant for vulnerable and marginalized groups.&description=Mutual aid, self-help and reciprocity are important coping strategies for isolated and vulnerable communities. Self-organized actions can help marginalized groups meet their own needs in the absence of wider provisions and services.&image=share_card5'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Volunteerism%20is%20particularly%20significant%20for%20vulnerable%20and%20marginalized%20groups.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card6.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">The costs and benefits of volunteerism are not always distributed equitably. </p>
                              <p class="text-color-darkblue paragraph-15">
                                 Women are more likely to take on the majority of informal volunteering in their own communities, for example, in an extension of domestic caring roles. Access to formal volunteering opportunities to develop skills, create new connections and access resources is not available for all, particularly those in low-income contexts. </p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The+costs+and+benefits+of+volunteerism+are+not+always+distributed+equitably.&image=share_card6'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The costs and benefits of volunteerism are not always distributed equitably.&description=Women are more likely to take on the majority of informal volunteering in their own communities, for example, in an extension of domestic caring roles. Access to formal volunteering opportunities to develop skills, create new connections and access resources is not available for all, particularly those in low-income contexts.&image=share_card6'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The costs and benefits of volunteerism are not always distributed equitably.&description=Women are more likely to take on the majority of informal volunteering in their own communities, for example, in an extension of domestic caring roles. Access to formal volunteering opportunities to develop skills, create new connections and access resources is not available for all, particularly those in low-income contexts.&image=share_card6'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=The%20costs%20and%20benefits%20of%20volunteerism%20are%20not%20always%20distributed%20equitably.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>



                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card7.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">The manner in which external actors engage with local volunteerism matters.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Collaborations should nurture the positive characteristics of volunteerism valued by communities – its self-organizing and relationship-strengthening properties. Peace and development actors can undermine volunteerism when they engage with people merely as a cheap and proximal resource. Done badly, partnerships with local volunteers can reinforce inequalities.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The+manner+in+which+external+actors+engage+with+local+volunteerism+matters.&image=share_card7'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The manner in which external actors engage with local volunteerism matters.&description=Collaborations should nurture the positive characteristics of volunteerism valued by communities – its self-organizing and relation-shipstrengthening properties. Peace and development actors can undermine volunteerism when they engage with people merely as a cheap and proximal resource. Done badly, partnerships with local volunteers can reinforce inequalities.&image=share_card7'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=The manner in which external actors engage with local volunteerism matters.&description=Collaborations should nurture the positive characteristics of volunteerism valued by communities – its self-organizing and relation-shipstrengthening properties. Peace and development actors can undermine volunteerism when they engage with people merely as a cheap and proximal resource. Done badly, partnerships with local volunteers can reinforce inequalities.&image=share_card7'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=The%20manner%20in%20which%20external%20actors%20engage%20with%20local%20volunteerism%20matters.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card8.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Effective collaboration with volunteers can transform volunteering from a coping mechanism to a strategic resource for community resilience. </p>
                              <p class="text-color-darkblue paragraph-15">
                                 Forming complementary partnerships with communities helps to balance risks more equitably, maximizing the potential of volunteering to positively impact those often left furthest behind. Appropriately pooling resources and capacities across actors enables communities to take longer-term preventative approaches to dealing with risk. </p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Effective+collaboration+with+volunteers+can+transform+volunteering+from+a+coping+mechanism+to+a+strategic+resource+for+community+resilience.&image=share_card8'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Effective collaboration with volunteers can transform volunteering from a coping mechanism to a strategic resource for community resilience.&description=Complementary partnerships with communities helps to balance risks more equitably, maximizing the potential of volunteering to positively impact those often left behind. Appropriately pooling resources and capacities across actors enables communities to take longer-term preventative approaches to resilience-building.&image=share_card8'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Effective collaboration with volunteers can transform volunteering from a coping mechanism to a strategic resource for community resilience.&description=Complementary partnerships with communities helps to balance risks more equitably, maximizing the potential of volunteering to positively impact those often left behind. Appropriately pooling resources and capacities across actors enables communities to take longer-term preventative approaches to resilience-building.&image=share_card8'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Effective%20collaboration%20with%20volunteers%20can%20transform%20volunteering%20from%20a%20coping%20mechanism%20to%20a%20strategic%20resource%20for%20community%20resilience.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card9.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">An enabling environment for volunteerism strengthens community resilience. </p>
                              <p class="text-color-darkblue paragraph-15">
                                 Governments and other stakeholders can strengthen the contribution of volunteerism to resilience-building in two ways: firstly, by nurturing an ecosystem for effective volunteering, and secondly, by forming partnerships based on greater appreciation of the value of communities’ own contributions. This will ensure that localization processes under the 2030 Agenda build on the commitment and innovations of citizens everywhere. </p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=An+enabling+environment+for+volunteerism+strengthens+community+resilience.&image=share_card9'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=An enabling environment for volunteerism strengthens community resilience.&description=Governments and other stakeholders can strengthen the contribution of volunteerism to resilience-building in two ways: firstly, by nurturing an ecosystem for effective volunteering, and secondly, by forming partnerships based on greater appreciation of the value of communities’ own contributions. This will ensure that localization processes under the 2030 Agenda build on the commitment and innovations of citizens everywhere.&image=share_card9'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=An enabling environment for volunteerism strengthens community resilience.&description=Governments and other stakeholders can strengthen the contribution of volunteerism to resilience-building in two ways: firstly, by nurturing an ecosystem for effective volunteering, and secondly, by forming partnerships based on greater appreciation of the value of communities’ own contributions. This will ensure that localization processes under the 2030 Agenda build on the commitment and innovations of citizens everywhere.&image=share_card9'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=An%20enabling%20environment%20for%20volunteerism%20strengthens%20community%20resilience.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>


               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="voices">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 text-center">
                        <h2 class="text-color-blue fontface-two padding-bottom-30" data-wow-delay="300ms">Views from around the world</h2>
                        <p class="padding-bottom-50 icon-hand-here">
                        </p>

                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-green flip-container" data-wow-delay="300ms" ontouchstart="this.classList.toggle('hover');">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VIEW FROM A NATIONAL STATISTICS AGENCY</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Volunteerism and statistical production in the Global South</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Isabel</p>
                              </div>
                              <div class="back">
                                 <p>Tshepiso steps back and admires his handiwork. As part of his contribution to Mandela Day, he has painted the interior walls of a corrugated iron shack that serves as a crèche for young children in an informal settlement in Johannesburg. In the spirit of ubuntu,<sup>a</sup> he regularly ferries his elderly parents, aunts and uncles to hospital or assists them with shopping. Just last weekend he repaired a broken kitchen cabinet door for his neighbour, Mrs Potts.</p>
                                 <p>Enabling people such as Tshepiso to drive their own development priorities and agenda for change is one of the cornerstones of sustainable development. An active civil society is an essential component of a cohesive and well-functioning state. By encouraging an active citizenry, the state can potentially achieve more, using less financial resources, and also achieve greater social cohesion in the process. Volunteerism asks: how can I make a difference in my extended family, in my community, in my country and at a global level?</p>
                                 <p><a href="https://www.unv.org/swvr/volunteerism-and-statistical-production-global-south">Read more</a></p>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-blue flip-container" data-wow-delay="400ms">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VOLUNTEER VOICES</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Women's  solidarity  plants  the  seeds  of  resilience  in  Guatemala</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Roselia</p>
                              </div>
                              <div class="back">
                                 <p>Ten  years  ago,  nobody  cared  about  the  community’s  forests.  We  kept  cutting  down  trees.  Then  I  decided  to  create  a  volunteer  group  to  plant  trees.  I  spoke  with  women  in  the  community  and  many  were  interested  to  participate,  which  motivated  me  more.  At  the  start  we  were  50  women,  and  a  member  of  the  community  lent  us  a  piece  of  land  to  plant  our  trees.  That  is  how  our  group  was  born.</p>
                                 <p> Volunteering  helped  me  a  lot.  Ten  years  ago,  I  was  a  different  person.  I  ignored  my  rights.  Before,  a  man  could  tell  me  that  I  didn’t  know  anything  and  I  used  to  cry  and  think:  “Yes,  he  is  right”.  I  was  afraid  to  say  anything  in  front  of  men,  but not  now.  Now  we  discuss  and  I  am  not  afraid  to  say  what  I  think.  For  example,  one  day  someone  offended  the  women  in  the  community  and  I  defended  them.  The  women  told  me:  “Roselia,  you  are  no  longer  afraid  of  anything”. </p>
                                 <p> In  our  group  women  make  their  own  decisions.  Before  we  had  nowhere  to  go  and  no  way  to  participate.  Before  it  was  only  “casa  y  casa”  (house  and  house).  Now  we  have  a  place  where  we  can  talk,  meet,  relax  and  exercise  our  rights.  In  the  plant  nursery,  we  share  our  joys  and  problems.  We  are  united. </p>
                                 <p> We  want  more  people  to  reforest  their  mountains  across  the  municipality,  so  we  will  spread  the  message  of  our  volunteer  work. </p>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-orange flip-container" data-wow-delay="300ms">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VIEW FROM A MUNICIPAL GOVERNMENT</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Volunteering as a tool for social integration in cities</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Matthew</p>
                              </div>
                              <div class="back">
                                 <p>Together with the Mayor of London, Sadiq Khan, my role is to bring together all Londoners and strengthen our communities. One of the most important lessons I have learned is the power of volunteering in achieving those goals.
                                 Earlier this year, we launched our social integration strategy. Based on considerable research, it sets out a new definition of social integration, emphasizing that it is about more than simply the degree of contact between people but also includes promoting equality and improving people’s levels of activity and participation in their local communities.
                                 But encouraging social integration is a meaningless exercise unless people are provided with opportunities to come together. Volunteering does just that.</p>
                                 <p>Volunteering helps citizens to connect with others in their local communities who may be from entirely different backgrounds. It creates bonds and shared identities that go beyond superficial differences that might otherwise seem important. Volunteering also provides a meaningful way of grappling with social problems – for example, reducing social isolation or improving mental health – for both the volunteer and the person benefiting from the volunteering.</p>
                                 <p><a href="https://www.unv.org/swvr/volunteering-tool-social-integration-cities">Read more</a></p>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-red flip-container" data-wow-delay="400ms">
                           
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VOLUNTEER VOICES</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Volunteers  driving  geospatial  mapping efforts  for  emergency  response</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Rohini</p>
                              </div>
                              <div class="back">
                                 <p>As  a  geospatial  specialist,  during  emergencies  I  volunteer  to  map  affected  areas  using  satellite  data.</p><p>  On  20  Sep  2017,  at  close  to  midnight,  I  received  an  email:</p>
                                 <p>“Dear  GISCorps  Volunteers,  ...seeking  assistance  for  conducting  damage  assessment of  ...  health  center  locations  affected  by  Hurricane  Maria  in  Puerto  Rico...If  you  are  interested  and  available,  please  send  an  email..”</p>
                                 <p>I  immediately  responded,  as  did  five  other  volunteers  fromdifferent  corners  of  the  world.  Working  together  through  an  online  group  we  scanned  through  miles  and  miles  of  data  in  just  a  few  days  -  which  would  have  taken  weeks  to  gather  from  the  field.  Getting  this  kind  of  information  at  the  right  time  can  fast-track  recovery  efforts  and  even  save  lives.  Online  volunteering  is  a  cost-effective  and  efficient  way  to  get  the  important  information  from  the  satellites  to  the  people  on  the  ground.  It  also  gives  me  a  way  to  use  my  technical  skills  meaningfully  and  to  be  part  of  a  bigger  picture.  I  believe  that  future  disaster  response  and  recovery  efforts  will  increasingly  rely  upon  remotely  sensed  data  -  such  as  from  drones.  Analysing  this  information  through  crowd-sourced  geospatial  mapping  platforms,  volunteers  like  me  can  play  a  significant  role.</p>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>



      <section class="background-grey padding-top-bottom" id="">
         <div class="container">

                  <div class="row">
                     <div class="col-lg-9 offset-lg-2 col-md-12 offset-md-0 offset-sm-0">
                        <div class="" data-wow-delay="300ms">
                           <span class="quote-sign">“</span>
                           <!-- Slider main container -->
                           <div class="swiper-container testimonials">
                               <!-- Additional required wrapper -->
                               <div class="swiper-wrapper">
                                   <!-- Slides -->
                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">Volunteerism connects people, enabling them to work together to tackle the pressing issues of our time. To make good on the promise to make the Sustainable Development Goals a reality for all, we need everyone to follow the lead of the current estimated 1 billion volunteers and make a difference in each of our communities. </p>
                                       <span class="author">Achim Steiner</span>
                                       <span class="title">Administrator, United Nations Development Programme </span>
                                   </div>

                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">Volunteering is not the only way to improve social integration, nor does it solve every problem. But it is a hugely important tool that government and local authorities can use to bring people together. We know that all Londoners want to feel like valued members of their community and to play an active role in the decisions that shape our city.</p>
                                       <span class="author">Matthew Ryder </span>
                                       <span class="title">Deputy Mayor, London </span>
                                   </div>

                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">By encouraging an active citizenry, the state can potentially achieve more, using less financial resources, and also achieve greater social cohesion in the process. Volunteerism asks: how can I make a difference in my extended family, in my community, in my country and at a global level? </p>
                                       <span class="author">Isabel Schmidt</span>
                                       <span class="title">Director, South Africa Stats </span>
                                   </div>

                               </div>

                               <div class="swiper-pagination"></div>

                           </div>
                        </div>
                     </div>
                  </div>

         </div>
      </section>


      <section class="padding-top-bottom bg-setting padding-bottom-45" data-image-src="images/bg_cta.jpg" id="call-to-action">
        <img id="unv-circle" src="images/unv-circle.svg" alt=""/>
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="text-center" data-wow-delay="300ms">
                           <h2 class="fontface-two text-color-blue padding-bottom-60">Volunteerism and the 2030 Agenda</h2>
                           <p class="">The 2030 Agenda requires all actors to build on the commitment, agency and innovation of citizens all over the world.</p>

                           <p class="">Understanding and partnering with volunteers based on standards of inclusion and equity drive forward a people-centred approach to development.</p>

                           <p class="">Will communities be the last line of defense or the first line of prevention? </p>

                           <p class="">This will depend on how governments and development partners choose to support the volunteers working day in, day out, to strengthen resilience.</p>

                           <a href="http://www.unv.org/swvr/collaborate" target="_blank" class="btn btn-blue padding-top-50">Get Involved</a>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-bottom-70 " id="footer">
         <div class="footer-top-bg padding-top-70 padding-bottom-45">
            <div class="container">
               <div class="row">
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">ABOUT THE REPORT</p>
                        <p>Every three years, the United Nations Volunteers (UNV) programme produces the State of the World’s Volunteerism Report (SWVR), a flagship UN publication designed to strengthen understanding on volunteerism and demonstrate its universality, scope and reach in the twenty-first century.</p>
                        <div class="footer-logo">
                           <a href="http://www.unv.org/swvr" target="_blank"><img src="images/logo.svg" alt="logo"></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">FIND US ONLINE</p>
                        <ul class="footer-contact">
                           <li>
                              <a target="_blank" href="https://twitter.com/evidenceunv"><i class="fab fa-twitter"></i>@EvidenceUNV</a>
                           </li>
                           <li>
                              <a href="mailto:unv.swvr@unv.org"><i class="fas fa-envelope"></i>unv.swvr@unv.org</a>
                           </li>
                           <li>
                              <a href="http://www.unv.org/swvr"><i class="fas fa-home"></i>www.unv.org/swvr</a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp footer-engage" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">Engage with us</p>
                        <a href="https://www.unv.org/swvr/swvr-policy-challenge" target="_blank">Join or host a policy challenge or launch event</a>
                        <a href="https://www.unv.org/swvr/blogsubmissionguidelines" target="_blank">Contribute to our blog</a>
                        <a href="http://eepurl.com/c9U54H" target="_blank">Sign up to our mailing list</a>
                        <a href="mailto:unv.swvr@unv.org" target="_blank">Share your thoughts and insights</a>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">DOWNLOAD REPORT</p>
                        <ul class="footer-contact">
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_WEB.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Full report</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_WEB_OVERVIEW.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Overview</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_WEB_CH1+INTRO.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Introduction and Chapter 1</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_WEB_CH2.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapter 2</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_WEB_CH3.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapter 3</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_WEB_CH4+CONC.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapter 4 and Conclusion</a>
                           </li>
                           <li>
                              <a class="" href="files/51692_UNV_SWVR_2018_WEB_ANNEX+NOTES+REFERENCES.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Annexes</a>
                           </li>

                        </ul>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </section>



   </section>
   <!-- /Page Wrapper End -->

   <!-- jQuery v3.1.1 -->
   <script defer src="js/jquery.js"></script>

   <!-- Popper Core Javascript -->
   <script defer src="js/popper.min.js"></script>

   <!-- Bootstrap Core JavaScript -->
   <script defer src="js/bootstrap.min.js"></script>

   <!-- Appear Core Javascript -->
   <script defer src="js/jquery.appear.min.js"></script>

   <!-- Header Threads Files -->
   <script defer src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
   <script defer src="js/threads.js"></script>
   <script defer src="js/threads_init.js"></script>


   <!--  Fancybox Core Javascript -->
   <script defer src="js/jquery.fancybox.min.js"></script>

   <!-- Swiper Core Javascript -->
   <script defer src="js/swiper.min.js"></script>

   <!--wow Transitions-->
   <script defer src="js/wow.min.js"></script>

   <!-- Custom JavaScript -->
   <script defer src="js/script.js"></script>
   <script defer src="js/TweenMax.min.js"></script>
   <script defer src="js/CSSRulePlugin.min.js"></script>
   <script defer src="js/jquery.waypoints.min.js"></script>
   <script defer src="js/custom-animations.js"></script>

</body>
</html>
