<!DOCTYPE html>

<?php

$params = array();
if(count($_GET) > 0) {
    $params = $_GET;
} else {
    $params = $_POST;
}
// defaults
if($params['type'] == "") $params['type'] = "website";
//if($params['locale'] == "") $params['locale'] = "en_US";
if($params['title'] == "") $params['title'] = "Voluntariado y resiliencia comunitaria";
if($params['image'] == "") $params['image'] = "bg_chapter1";
if($params['description'] == "") $params['description'] = "Informe sobre el estado del voluntariado en el mundo 2018 ";

?>

<html lang="sp">

<head>
   <!-- Meta Tags For Seo + Page Optimization -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="2018 State of the world's volunteerism report">
   <meta name="author" content="">

   <meta property="fb:app_id" content="" />
    <meta property="og:site_name" content="meta site name"/>
    <meta property="og:url" content="https://www.unv-swvr2018.org/index_sp.php?type=<?php echo $params['type']; ?>&title=<?php echo $params['title']; ?>&image=<?php echo $params['image']; ?>&description=<?php echo $params['description']; ?>"/>
    <meta property="og:type" content="<?php echo $params['type']; ?>"/>
    <!-- <meta property="og:locale" content="<?php echo $params['locale']; ?>"/> -->
    <meta property="og:title" content="<?php echo $params['title']; ?>"/>
    <meta property="og:image" content="https://www.unv-swvr2018.org/images/<?php echo $params['image']; ?>.jpg"/>
    <meta property="og:description" content="<?php echo $params['description']; ?>"/>

   <!-- Insert Favicon Here -->
   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
   <link rel="icon" href="images/favicon.ico" type="image/x-icon">

   <!-- Page Title(Name)-->
   <title>UNV - Informe sobre el estado del voluntariado en el mundo 2018</title>


   <!-- Bootstrap CSS Files -->
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/bootstrap-reboot.min.css">

   <!-- Animate CSS File -->
   <link rel="stylesheet" href="css/animate.css">

   <!-- Font-Awesome CSS File -->
   <link rel="stylesheet" href="css/fontawesome-all.min.css">

   <!-- Fancybox CSS File -->
   <link rel="stylesheet" href="css/jquery.fancybox.min.css">

   <!-- Swiper CSS File -->
   <link rel="stylesheet" href="css/swiper.min.css">

   <!-- Custom Style CSS File -->
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="css/custom-animations.css">


</head>

<body onload="threadsInit();" data-spy="scroll" data-target=".navbar" data-offset="50" class="index-one">

<!-- Page Loader -->
   <div class="preloader">
      <div class="loader-dot-outer">
         <div class="loader-dot-center"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
      </div>
   </div>
   <!-- Page Loader -->


   <!-- Page Wrapper -->
   <section class="page-wrapper">

      <!-- Header Section Starts -->
      <header class="site-header">
         <nav class="navbar navbar-expand-lg navbar-transparent-white">
            <div class="container nav-logo-detail-outer">
               <!--side menu open button-->



               <a class="navbar-brand" href="index_sp.php">
                  <img src="images/logo_sp.svg" alt="logo">
               </a>

               <div class="collapse navbar-collapse d-sm-0 d-md-0">
                  <ul class="navbar-nav ml-auto">
                     <li class="nav-item">
                        <a class="nav-link scroll active" href="#overview">Resumen</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-1">Cap. 1</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-2">Cap. 2</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-3">Cap. 3</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-4">Cap. 4</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#sharing">Resultados</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://www.unv.org/es/node/2865" target="_blank">Proyecto SWVR</a>
                     </li>
                  </ul>
               </div>

               <div class="lang" class="right">
                  <a href="index_fr.php">FR </a> | <a href="index.php">EN</a>
               </div>

               <div id="menu_bars" class="right"> <span></span> <span></span> <span></span> </div>

            <div class="sidebar_menu">
               <nav class="side-nav side-nav-right">
                  <span class="title mobile">Navegación</span>
                  <ul class="side-nav-list mobile">
                     <li class="nav-item">
                        <a class="nav-link scroll active" href="#overview">Resumen</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-1">Cap. 1</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-2">Cap. 2</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-3">Cap. 3</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-4">Cap. 4</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#sharing">Resultados</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://www.unv.org/es/node/2865" target="_blank">Proyecto SWVR</a>
                     </li>
                  </ul>

                  <span class="title">DESCARGAS</span>
                  <ul class="side-nav-list">
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_SP_WEB.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Informe completo</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_SP_WEB_OVERVIEW.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Resumen</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_SP_WEB_CH1+INTRO.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Introducción y capítulo 1</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_SP_WEB_CH2.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Capítulo 2</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_SP_WEB_CH3.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Capítulo 3</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_SP_WEB_CH4+CONC.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Capítulo 4 y conclusión</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_SP_WEB_ANNEX+NOTES+REFERENCES.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Anexidades</a>
                     </li>

                  </ul>


                  <div class="lang" class="right">
                     <a href="index_fr.php">FR </a> | <a href="index.php">EN</a>
                  </div>

                  <div class="bottom-share">
                     <ul class="social-icons padding-bottom-15">
                        <!-- <li>
                           <a href="https://www.google.com/"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-google"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-twitter"></i></a>
                        </li> -->
                     </ul>
                     <p></p>
                  </div>
               </nav>
            </div>
         </div>
         </nav>

      </header>

      <section id="top">
            <div id="header">
                  <div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:1600px; height:720px">
                        <canvas id="canvas" width="1600" height="720" style="position: absolute; display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                        <div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:1600px; height:720px; position: absolute; left: 0px; top: 0px; display: block;">
                        </div>
                  </div>
                  <div id="docDialog">
                        <p class="dlgHeader">Informe sobre el estado del voluntariado en el mundo 2018  </p>
                        <p class="dlgTitle">El <span class="text-color-blue">lazo</span> que nos une</p>
                        <p class="dlgText">Voluntariado y resiliencia comunitaria</p>
                        <div class="buttons">
                              <a href="files/51692_UNV_SWVR_2018_SP_WEB.pdf" target="_blank" class="button">DESCARGAR EL INFORME</a>
                              <a href="files/51692_UNV_SWVR_2018_SP_WEB_OVERVIEW.pdf" target="_blank" class="button">DESCARGAR EL RESUMEN</a>
                        </div>
                  </div>
            </div>


            <script>
                  var H_WIDTH = 1600;
                  var H_HEIGHT = 720;

                  window.addEventListener('resize', onResize);
                  onResize();

                  function onResize() {
                        var header = document.getElementById('header');
                        var container = document.getElementById('animation_container');

                        var w = header.offsetWidth;
                        var scFix = (w < H_WIDTH) ? 1 : w / H_WIDTH;

                        container.style.transform = 'translate(-50%, -50%) scale(' + scFix + ')';
                  }
            </script>
      </section>


      <section class="background-white padding-top-bottom" id="overview">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 o-2">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="paragraph-15 padding-bottom-25 text-color-blue spacing">RESUMEN</p>
                           <h2 class="font-weight-700 padding-bottom-25"><span class="text-color-blue">Creación de modelos</span> de resiliencia</h2>
                           <p class="padding-bottom-20">Los voluntarios están en el frente de todo conflicto importante, catástrofe natural o crisis grave. De forma menos visible, todos los días y en todas partes, todo tipo de gente trabaja como voluntaria para afrontar tensiones que ponen a prueba su resiliencia, tales como una educación deficiente, mala salud y pobreza. En muchas comunidades, especialmente las que carecen de apoyo público y redes de seguridad, <strong>el servicio voluntario surge como estrategia de supervivencia fundamental, ya que favorece estrategias colectivas para gestionar el riesgo</strong>. </p>

                           <p class="padding-bottom-15">The <strong>Informe sobre el estado del voluntariado en el mundo 2018, <em>El lazo que nos une: Voluntariado y resiliencia comunitaria</em></strong>, atiende al modo en que las características distintivas del voluntariado local pueden suponer una ayuda o un obstáculo para las comunidades en crisis. Por primera vez, el informe se basa en la <a href="https://www.unv.org/es/node/2744" target="_blank"><u>investigación de campo</u></a> llevada a cabo por voluntarios con 1200 participantes de 15 comunidades diversas. También estudia de qué forma los gobiernos y otros actores de desarrollo pueden colaborar con soluciones locales para fortalecer la resiliencia comunitaria.</p>

                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-1 col-md-12 offset-md-0 offset-sm-0 padding-top-60 o-1">
                        <div class="wow fadeInUp mobile-margin" data-wow-delay="500ms">
                           <a href="https://www.youtube.com/watch?v=xWpD-cRptf0" data-fancybox class="">
                              <img src="images/image_video_sp.png" alt="">
                              <!-- <i class="fa fa-play"></i> -->
                           </a>
                           <p class="paragraph-15 text-center padding-top-20"><em>En este vídeo se explican los hallazgos y recomendaciones del informe</em></p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-blue padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">

                     <div class="col-lg-7 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp mobile-margin" data-wow-delay="500ms">
                           <img src="images/image_findings_sp.png" width="" alt="">
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="300ms">

                           <p class="text-color-blue paragraph-60 fontface-two padding-bottom-20 padding-top-15">Resultados Principales</p>

                           <p class="">Conozca los resultados principales del informe y compártalos en sus redes.</p>

                           <a href="#sharing" class="btn btn-blue padding-top-50" style="background-position: 13px 18px; padding-left: 43px;">Resultados principales</a>

                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-1" data-parallax="scrol" data-image-src="images/bg_chapter1.jpg" id="">
         <div class="container">

            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CAPITULO</p>
                           <p class="number text-color-orange">01</p>
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">El voluntariado como activo global para el desarrollo</h2>
                           <p class="paragraph-17 text-color-white"><em>¿Quiénes son los voluntarios del mundo? ¿Qué nos indican las estimaciones y tendencias globales sobre el voluntariado en 2018?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-4 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_SP_WEB_CH1+INTRO.pdf" target="_blank"" class="btn">Descargar introducción y capítulo 1</a>
                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-orange paragraph-38 fontface-two padding-bottom-20">“En nuestras aspiraciones de crear capacidades y de contribuir al afianzamiento de la nueva agenda, el voluntariado puede ser otro medio efectivo y transversal de implementación."  <a href="http://www.un.org/ga/search/view_doc.asp?symbol=A/69/700&Lang=E" target="_blank"><span class="text-color-dark">- El camino hacia la dignidad para 2030 (A/69/700)</span></a></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-orange paragraph-20"><em>El voluntariado forma parte del tejido de todas las sociedades. Puede ser un recurso esencial para la paz y el desarrollo, pero se necesita más evidencia para comprender el valor distintivo de las contribuciones voluntarias a la economía y la sociedad, especialmente en contextos frágiles.</em></p>

                           <p class="padding-bottom-10">El análisis del programa de Voluntarios de las Naciones Unidas (UNV) demuestra que los esfuerzos de los más de mil millones de voluntarios que hay en todo el mundo equivalen a los de <a class="scroll" href="#scale-scope"><u>109 millones de trabajadores a tiempo completo</u></a>.</p>
                           <p class="padding-bottom-10">Por ponerlo en contexto, la fuerza de trabajo voluntario casi triplica el número de personas que trabajan en servicios financieros en todo el mundo y supera en más de cinco veces el de quienes se dedican a los sectores de la minería y la extracción.</p>
                           <p class="padding-bottom-10"><strong>El voluntariado es universal y lo ejercen personas de todas las edades y orígenes.</strong> Sin embargo, es probable que el acceso a los distintos tipos de oportunidades de servicio voluntario sea diferente, en virtud de la nacionalidad, los ingresos, el género y el estatus social. La inmensa mayoría del trabajo voluntario, el 70 %, no está relacionada con ninguna organización, sino que se lleva a cabo de manera informal entre los integrantes de las comunidades. En todo el mundo, las mujeres asumen la mayoría del trabajo voluntario, el 57 %, y un porcentaje aún mayor del servicio voluntario informal, el 59 %, a menudo como ampliación de los roles de cuidado no remunerados.</p>
                           <p class="padding-bottom-10"><strong>Los cambios sociales, medioambientales y tecnológicos están influyendo en el servicio voluntario en 2018.</strong> Más de 90 países de todo el mundo tienen ya políticas o legislación sobre el servicio voluntario. Las generaciones más jóvenes están creando nuevas formas de servicio voluntario para cuestiones específicas y participando en ellas; a menudo, se valen de las tecnologías digitales para conectarse. Esta práctica parece bastante distinta del trabajo voluntario local y habitual que llevaban a cabo sus padres en sus comunidades. </p>
                           <p class="padding-bottom-10">Aunque se necesitan más iniciativas, por parte de los gobiernos y otros agentes, para aumentar la información y pruebas disponibles sobre el servicio voluntario, estas cifras, tendencias y patrones son un punto de partida para entender mejor el impacto de este sobre la paz global y los problemas de desarrollo en 2018.</p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="padding-top-bottom" id="scale-scope">
         <div class="container">
            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <p class="padding-bottom-35 paragraph-24 font-weight-700">Escala y ámbito del voluntariado en el mundo</p>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link  active" id="type-tab" data-toggle="tab" href="#type" role="tab" aria-controls="type" aria-selected="false">Por tipo</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="region-tab" data-toggle="tab" href="#region" role="tab" aria-controls="region" aria-selected="false">Por región</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="type-region-tab" data-toggle="tab" href="#type-region" role="tab" aria-controls="type-region" aria-selected="false">Por tipo y región</a>
                           </li>

                          <li class="nav-item">
                              <a class="nav-link" id="sex-tab" data-toggle="tab" href="#sex" role="tab" aria-controls="sex" aria-selected="false">Por sexo</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="sex-region-tab" data-toggle="tab" href="#sex-region" role="tab" aria-controls="sex-region" aria-selected="false">Por sexo y tipo</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="workforce-tab" data-toggle="tab" href="#workforce" role="tab" aria-controls="workforce" aria-selected="true">Por fuerza de trabajo</a>
                          </li>



                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade show active" id="type" role="tabpanel" aria-labelledby="type-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">La mayoría del servicio voluntario a escala mundial tiene lugar de manera informal</p>

                              <a href="files/bytype_sp.pdf" target="_blank" class="dwn">Descargar la figura</a>
                          </div>

                          <div class="tab-pane fade" id="region" role="tabpanel" aria-labelledby="region-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Servicio voluntario equivalente a tiempo completo por región

</p>
                              <a href="files/byregion_sp.pdf" target="_blank" class="dwn">Descargar la figura</a>
                          </div>

                          <div class="tab-pane fade" id="type-region" role="tabpanel" aria-labelledby="type-region-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">El servicio voluntario informal supera al servicio voluntario formal en todas las regiones</p>
                              <a href="files/bytypeandregion_sp.pdf" target="_blank" class="dwn">Descargar la figura</a>
                          </div>

                          <div class="tab-pane fade" id="sex" role="tabpanel" aria-labelledby="sex-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">La proporción de mujeres respecto del servicio voluntario total es más alta en todas las regiones a excepción de Asia y el Pacífico</p>
                             <a href="files/bysex_sp.pdf" target="_blank" class="dwn">Descargar la figura</a>
                          </div>

                          <div class="tab-pane fade" id="sex-region" role="tabpanel" aria-labelledby="sex-region-tab">
                              <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Las mujeres asumen la cuota mayoritaria de servicio voluntario informal en todas las regiones</p>
                             <a href="files/bysexandregion_sp.pdf" target="_blank" class="dwn">Descargar la figura</a>
                          </div>
                          <div class="tab-pane fade" id="workforce" role="tabpanel" aria-labelledby="workforce-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">La fuerza de trabajo voluntario mundial supera el número de personas empleadas en más de la mitad de los 10 países más poblados, 2016</p>
                             <a href="files/byworkforce_sp.pdf" target="_blank" class="dwn">Descargar la figura</a>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-2" data-parallax="scrol" data-image-src="images/bg_chapter2.jpg"  id="">
         <div class="container">

            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CAPITULO</p>
                           <p class="number text-color-red">02</p>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Voluntariado local en las comunidades bajo presión</h2>
                           <p class="paragraph-17 text-color-white"><em>El servicio voluntario en las comunidades, tanto formal como informal, abarca una amplia variedad amplia de actividades para apoyar la resiliencia comunitaria. Pero, más allá de la inmensa escala y ámbito de las iniciativas voluntarias, ¿las comunidades en crisis valoran el servicio voluntario? ¿Por qué?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms ">
                           <a href="files/51692_UNV_SWVR_2018_SP_WEB_CH2.pdf" target="_blank" class="btn border-red">Descargar capítulo 2</a>
                        </div>
                     </div>


                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-red paragraph-38 fontface-two padding-bottom-20">“Los voluntarios de la comunidad fueron los únicos que levantaron la mano... También hubo muchos que dijeron: ‘Nadie lo va a hacer por nosotros. Debemos encargarnos nosotros. Esta es nuestra comunidad’". <span class="text-color-dark">Cooperante de la Cruz Roja</span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-red paragraph-20"><em>Los miembros de las comunidades creen que el trabajo voluntario tiene dos valores distintivos que los ayudan a ser más resilientes:</em></p>

                           <p class="padding-bottom-10"> <span class="bg-red">En primer lugar,</span>  todos los tipos de comunidades valoran el servicio voluntario porque da a la <span class="font-weight-700">capacidad de <span class="text-color-red">autoorganización</span> en torno a sus propias prioridades</span>. Al unir esfuerzos para llevar a cabo el servicio voluntario, los individuos pueden crear estrategias colectivas para gestionar el riesgo. Esto es importante en momentos de crisis por la velocidad de respuesta, la flexibilidad, la apropiación y la agencia de los esfuerzos; sobre todo, en las comunidades más aisladas, donde los demás tipos de apoyo están limitados. En otros contextos, la autoorganización es más importante para los grupos marginados.</p>
                           <p class="padding-bottom-10"><span class="bg-red">En segundo lugar,</span> por su naturaleza, <span class="font-weight-700">el voluntariado se crea en torno a <span class="text-color-red">conexiones humanas</span></span>.  Los participantes en la investigación insistieron en que la solidaridad, la empatía y los vínculos generados mediante la acción social son factores de protección en momentos de dificultad. Las conexiones y redes humanas permiten a las comunidades compartir información sobre riesgos, llegar a sus miembros más vulnerables y actuar sobre la base de la empatía y los valores compartidos.</p>
                           <p class="padding-bottom-10">La inmensa mayoría de los voluntarios trabaja día tras día en sus propias comunidades. Pero, como el voluntariado es parte integral de las comunidades en situaciones de tensión, tampoco puede idealizarse. En algunos contextos, el servicio voluntario puede tener un impacto menos positivo sobre la resiliencia.</p>
                           <p class="padding-bottom-10">Por ejemplo, las iniciativas voluntarias locales pueden ser eficaces a la hora de responder ante crisis y tensiones, pero, al carecer de recursos suficientes, quizá no puedan ayudar a las comunidades a evitar las crisis y adaptarse. Los riesgos nuevos y emergentes, como la mayor variabilidad del clima, también ponen en tensión las estrategias tradicionales del servicio voluntario.</p>
                           <p class="padding-bottom-10">Además, dado que el voluntariado está enraizado en las relaciones sociales, también puede ser exclusivo y explotador y suponer una carga para los más vulnerables. Las dinámicas de poder internas de las comunidades y entre ellas implican que las mujeres y los grupos marginados suelen asumir la mayor parte del voluntariado de estatus más bajo.  Al mismo tiempo, los roles de voluntariado que aumentan las destrezas y las oportunidades de liderazgo no están siempre disponibles para todas las personas.  </p>
                           <p class="padding-bottom-10">Estos hallazgos desafían la suposición de que centrarse en lo local mejorará automáticamente la participación y empoderará a los grupos de voluntarios de una manera transformadora. Para ayudar a las comunidades a recuperarse, es importante crear un entorno que reconozca y aproveche al máximo las características más positivas del voluntariado y aborde sus dificultades.</p>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>



      <section class="padding-top-bottom bg-setting"  id="foldout">
         <div class="container">

            <div class="">
               <div class="">

                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="paragraph-24 font-weight-700 text-color-darkblue text-center padding-bottom-20">Lo que las comunidades valoran del voluntariado para la resiliencia</p>
                           <p class="text-center padding-bottom-20">En este gráfico se muestra de qué forma las relaciones humanas y las características de autoorganización del voluntariado local mejoran o limitan la resiliencia de las comunidades.</p>
                           <img class="image-hand padding-bottom-70" src="images/icon_hand2_sp.svg" alt="">
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <div class="list-box red text-color-white">
                              <div class="list-box-top">
                                 <h4>RELACIONES HUMANAS</h4>
                              </div>
                              <div class="row list-box-bot">
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">CONTRIBUCIONES POSITIVAS</h5>
                                    <ul>
                                       <li><span>Confianza</span>
                                       <p>Un alto nivel de confianza entre los voluntarios lleva a una mejora en la acción colectiva.</p></li>
                                       <li><span>Solidaridad</span>
                                       <p>La acción voluntaria puede mejorar la solidaridad, o el «poder de la unión», a través de la ayuda mutua.</p></li>
                                       <li><span>Cohesión</span>
                                       <p>La acción voluntaria ayuda a renegociar las relaciones entre grupos que se han dividido y fomenta la formación de redes de personas con causas compartidas.</p></li>
                                       <li><span>Soporte emocional</span>
                                       <p>Los voluntarios de la comunidad suelen sentirse identificados con quienes sufren, y les ayudan. Esto puede reducir los sentimientos de alienación y aislamiento.</p></li>
                                       <li><span>Acceso local</span>
                                       <p>Los voluntarios locales tienen vínculos y acceso a grupos vulnerables.</p></li>
                                       <li><span>Conocimiento contextual</span>
                                       <p>Los voluntarios locales pueden contextualizar la información sobre la comunidad para los actores externos.</p></li>
                                       <li><span>Vínculos con redes de mayor alcance</span>
                                       <p>Cuando los voluntarios locales están organizados, pueden desempeñar una función de puente entre los actores locales y los nacionales o internacionales.</p></li>
                                    </ul>
                                 </div>
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">LÍMITES Y AMENAZAS</h5>
                                    <ul>
                                       <li><span>Visión a corto plazo</span>
                                       <p>El voluntariado basado en la solidaridad social y los vínculos emocionales puede priorizar las necesidades inmediatas y urgentes frente a la prevención y la adaptación a largo plazo.</p></li>
                                       <li><span>Exclusión</span>
                                       <p>La solidaridad y la acción voluntaria colectiva pueden llevar a la exclusión de grupos externos.</p></li>
                                       <li><span>División</span>
                                       <p>Frente a las tensiones, existen pocos incentivos para que los grupos de voluntarios locales acojan a personas con identidades diferentes o puntos de vista divergentes.</p></li>
                                       <li><span>Falta de atención a la voz local</span>
                                       <p>Las relaciones voluntarias a menudo se focalizan internamente, y los desequilibrios de poder y la falta de afiliación pueden limitar la absorción del conocimiento local de los voluntarios.</p></li>
                                       <li><span>Conflicto interno</span>
                                       <p>Los grupos de voluntarios integrados por poblaciones marginadas pueden causar conflictos intercomunitarios cuando se organizan en contra de decisiones comunitarias de mayor alcance o desafían el orden establecido.</p></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <div class="list-box blue text-color-white">
                              <div class="list-box-top">
                                 <h4>AUTOORGANIZACIÓN</h4>
                              </div>
                              <div class="row list-box-bot">
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">CONTRIBUCIONES POSITIVAS</h5>
                                    <ul>
                                       <li><span>Velocidad e inmediatez</span>
                                       <p>Los voluntarios locales brindan la respuesta inmediata y de primera línea en una situación de crisis.</p></li>
                                       <li><span>Escala</span>
                                       <p>El servicio voluntario espontáneo puede movilizar a un gran número de personas durante una crisis; la amplia dispersión geográfica de los voluntarios permite una detección temprana de las amenazas.</p></li>
                                       <li><span>Disponibilidad</span>
                                       <p>Los voluntarios locales a menudo son las únicas fuentes de ayuda disponibles en una crisis y pueden organizarse cuando las autoridades centrales no están disponibles para dirigir y coordinar una respuesta de emergencia.</p></li>
                                       <li><span>Flexibilidad</span>
                                       <p>La acción voluntaria local informal está menos ligada a los métodos y procedimientos estándar y puede adaptarse más fácilmente a las condiciones locales cambiantes.</p></li>
                                       <li><span>Innovación</span>
                                       <p>Los voluntarios locales a menudo resuelven problemas sobre la base de las necesidades y recursos inmediatos.</p></li>
                                       <li><span>Apropiación</span>
                                       <p>Las prioridades autodeterminadas y el control limitado de los actores externos fomentan la respuesta voluntaria y la apropiación de las soluciones.</p></li>
                                       <li><span>Rentabilidad</span>
                                       <p>Las iniciativas de organización se basan en los recursos disponibles y en especie de los voluntarios.</p></li>
                                    </ul>
                                 </div>
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">LÍMITES Y AMENAZAS</h5>
                                    <ul>
                                       <li><span>Explotación</span>
                                          <p>Los voluntarios locales organizados para satisfacer necesidades específicas pueden ser utilizados como mano de obra barata, con una compensación o un apoyo insuficiente.</p><li>
                                       <li><span>Substitutivo</span>
                                          <p>Los voluntarios locales llenan las lagunas de los servicios públicos, lo que puede disuadir la inversión pública.</p><li>
                                       <li><span>Obligatorio</span>
                                          <p>Algunas estrategias de resiliencia de las comunidades locales requieren «participación voluntaria», y las personas que no participan son multadas o rechazadas socialmente o se les niega el acceso a bienes o servicios producidos colectivamente.</p><li>
                                       <li><span>Escala</span>
                                          <p>En algunos contextos, la autoorganización puede significar una incapacidad para utilizar eficazmente un gran número de voluntarios locales durante las crisis.</p><li>
                                       <li><span>Aislamiento</span>
                                          <p>Los voluntarios que no están conectados a los servicios generales dependen de los recursos locales.</p><li>
                                       <li><span>Segmentación</span>
                                          <p>El servicio voluntario local suele ser una estrategia de supervivencia para grupos vulnerables o minoritarios que se autoorganizan para satisfacer necesidades específicas no atendidas por la comunidad en general. Esto podría no contrarrestar los procesos de marginación y, en cambio, aumentar la carga sobre los más vulnerables.</p><li>
                                    </ul>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </div>

                  </div>


               </div>
            </div>
         </div>
      </section>





      <section class="bg-setting height-100 chapter" id="chapter-3" data-parallax="scrol" data-image-src="images/bg_chapter3.jpg" id="">
         <div class="container">
            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CAPITULO</p>
                           <p class="number text-color-green">03</p>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Colaboraciones con el voluntariado para la resiliencia comunitaria</h2>
                           <p class="paragraph-17 text-color-white"><em>Si el voluntariado local es una estrategia fundamental de resiliencia, el modo en el que los actores externos se implican en él es relevante. ¿Cuál es la mejor manera en que los gobiernos y sus socios de desarrollo pueden complementar la acción voluntaria local?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_SP_WEB_CH3.pdf" target="_blank" class="btn border-green">Descargar capítulo 3</a>
                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>



      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-green paragraph-38 fontface-two padding-bottom-20">“Como voluntarios, podemos ver fácilmente los límites de nuestra labor. Carecemos de los recursos necesarios; realmente necesitamos ayuda externa en caso de crisis."<span class="text-color-dark">– Participantes del grupo de discusión, Burundi, investigación de campo para el informe SWVR </span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-green paragraph-20"><em>Cuando las capacidades locales se llevan al límite o cuando las crisis y tensiones proceden de fuera de sus comunidades, la colaboración entre los actores externos y el voluntariado local adquiere relevancia. </em></p>

                           <p class="padding-bottom-10">Directa o indirectamente, muchos gobiernos y sus socios de desarrollo ya están influyendo en el voluntariado local. Las políticas, los programas, las normas y los estándares afectan a la capacidad del voluntariado en el fortalecimiento de la resiliencia.  </p>
                           <p class="padding-bottom-10">Si se hacen bien, las colaboraciones pueden tener un elevado valor complementario, al permitir que los voluntarios locales pasen de la mera gestión de las crisis a la prevención y el apoyo a las comunidades a adaptarse a ellas. Las colaboraciones también pueden ayudar a integrar normas y estándares más inclusivos y equitativos que permitan que las personas de todo tipo se beneficien de las oportunidades del voluntariado.</p>
                           <p class="padding-bottom-10">A la inversa, cuando los actores se implican en el voluntariado local como si este fuera un simple recurso barato y próximo, el voluntariado local puede verse debilitado. Los voluntarios pueden verse sobrecargados y explotados y tener pocas oportunidades de alimentar sus contribuciones distintivas, como el conocimiento local, las relaciones humanas, la agencia, la autonomía y la flexibilidad. <strong>El voluntariado en sí puede perder resiliencia.</strong></p>
                           <p class="padding-bottom-10">Las colaboraciones entre voluntarios locales y actores externos deben estructurarse siguiendo una voluntad de auténtica colaboración que aproveche las fortalezas del voluntariado para la resiliencia. Es fundamental conceder a los voluntarios locales espacio suficiente para que se unan con el fin de innovar y resolver problemas, al tiempo que sus iniciativas reciben apoyo y se vinculan a las de los actores complementarios.</p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="padding-top-bottom" id="examples-tabs">
         <div class="container">
            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <p class="padding-bottom-35 paragraph-24 font-weight-700">Ejemplos de colaboración eficaz con el voluntariado local</p>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link  active" id="health-tab" data-toggle="tab" href="#health" role="tab" aria-controls="online" aria-selected="false">Promoción sanitaria</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="online-tab" data-toggle="tab" href="#online" role="tab" aria-controls="online" aria-selected="false">Servicio voluntario en línea</a>
                           </li>

                          <li class="nav-item">
                              <a class="nav-link" id="opensource-tab" data-toggle="tab" href="#opensource" role="tab" aria-controls="opensource" aria-selected="false">Generación de mapas con código abierto</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="environmental-tab" data-toggle="tab" href="#environmental" role="tab" aria-controls="environmental" aria-selected="false">Protección del medio ambiente</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="data" data-toggle="tab" href="#collection" role="tab" aria-controls="collection" aria-selected="true">Recopilación de datos</a>
                          </li>



                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade show active" id="health" role="tabpanel" aria-labelledby="health-tab">
                              <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">El rol esencial de los promotores sanitarios voluntarios de la comunidad</p>
                                    <img src="images/1_health_promotion.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Los voluntarios de promoción sanitaria participaron en casi todas las comunidades de investigación de campo de rentas bajas, especialmente en zonas remotas y vulnerables, fuera del alcance de los servicios estatales. Estos voluntarios transmiten información sobre nutrición, salud maternoinfantil, salud reproductiva y otras áreas de atención médica primaria y prevención de enfermedades. A menudo se considera que comprenden mejor las necesidades y los problemas de la comunidad que los profesionales médicos del servicio de salud público.</p>

                                    <p class="padding-bottom-10">A pesar de estos beneficios, a los voluntarios de promoción sanitaria no les resultó fácil hacer su trabajo. La mayor parte de ellos recibió formación inicial y apoyo del gobierno o de agencias de desarrollo, pero en general informaron que tuvieron que cesar sus actividades de promoción sanitaria poco después, debido a la falta de apoyo. Los voluntarios que lograron continuar a menudo lo hicieron con un coste personal considerable. Uno de los muchos voluntarios de la comunidad de investigación de campo de Guatemala describió su situación:</p>

                                    <p><em>¿Por qué el gobierno no nos da más apoyo? Estamos haciendo este trabajo, salvando vidas, pero no hay incentivos. Me tengo que pagar el transporte. Cuando empecé, me compré unas tijeras, una gabacha [delantal], una olla para hervir agua y un paraguas (porque a veces tenemos que salir cuando está lloviendo), una mochila, un par de botas. Tenemos que pagarlo nosotros. Pero, ¿qué podemos hacer cuando son las madres quienes vienen a buscarnos?</em></p>
                                 </div>
                              </div>
                           </div>

                          <div class="tab-pane fade" id="online" role="tabpanel" aria-labelledby="online-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Vinculación de diversas aptitudes y conocimientos a través del servicio voluntario en línea</p>
                                    <img src="images/2_online_volunteering.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">El programa de Voluntarios de las Naciones Unidas gestiona <a href="www.onlinevolunteering.org" target="_blank">el servicio de voluntariado en línea de las Naciones Unidas</a>, una plataforma que moviliza a más de 12 000 voluntarios en línea cada año. El servicio voluntario en línea es una forma simple, universal y eficaz de que las organizaciones y los voluntarios trabajen juntos para abordar los desafíos del desarrollo sostenible en cualquier parte del mundo, desde cualquier dispositivo.</p>

                                    <p class="padding-bottom-10">Desde junio de 2014, los Voluntarios en línea de las Naciones Unidas han brindado apoyo técnico a los Agriculteurs Professionels du Cameroun (de Camerún), un proyecto de desarrollo rural en la aldea Tayap de la cuenca del Congo, zona que ha sufrido una pérdida generalizada de hábitat y biodiversidad. El proyecto tiene como objetivo promover medios de vida sostenibles y resiliencia comunitaria. Los Voluntarios en línea de la ONU incluyen: Un experto en tecnologías de la información de Burkina Faso que está creando mapas de la aldea; un ingeniero agrícola de Togo que analiza imágenes satelitales de la cobertura forestal; y un experto en energías renovables de Francia que está desarrollando un proyecto de energía solar para la aldea. El apoyo multidisciplinar constante que proporcionan estos voluntarios en línea internacionales ha sido fundamental para el éxito del proyecto, que ha ganado varios premios y subvenciones.</p>

                                 </div>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="opensource" role="tabpanel" aria-labelledby="opensource-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Uso de software libre para supervisar e informar durante las crisis</p>
                                    <img src="images/3_online_mapping.png" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">El software abierto de generación de mapas es una potente herramienta para los voluntarios que participan en la respuesta a una crisis. Ushahidi es una plataforma de software libre que ha permitido la participación voluntaria en la generación de mapas a partir de datos durante más de una década. Lanzado en 2007 para rastrear informes de violencia postelectoral en Kenia, Ushahidi ha sido mejorado por voluntarios y extendido a otros usos y contextos. La gente utilizó la plataforma para supervisar la votación e informar sobre ella durante las elecciones generales de 2017 en Kenia. Entre la información generada se incluían datos sobre represión a votantes, problemas con las urnas y casos de violencia. </p>

                                    <p class="padding-bottom-10">Basándose en este modelo, el software libre se emplea cada vez más para emergencias en todo el mundo.  Por ejemplo, durante el terremoto de 2017 en México, miles de voluntarios tradujeron miles de mensajes de texto y publicaciones en redes sociales de personas que necesitaban ayuda. Los voluntarios pudieron geolocalizar estos mensajes, etiquetar su ubicación y comunicar la información asignada a quienes estaban trabajando sobre el terreno. Hay informes similares sobre cómo ayudó el software libre a las comunidades a afrontar (y recuperarse de) otras crisis recientes, como el terremoto de 2015 en Nepal, el brote de ébola de 2014-16 en África occidental, la violencia en la guerra civil siria y los huracanes Harvey e Irma de 2017.</p>

                                 </div>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="environmental" role="tabpanel" aria-labelledby="envirionmental-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Servicio voluntario intercomunitario para proteger los recursos naturales compartidos</p>
                                    <img src="images/4_environmental_protection.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Muchos riesgos que afectan a la resiliencia traspasan los límites de la comunidad. La gestión eficaz de estos riesgos exige, por lo tanto, la cooperación entre las comunidades. El servicio voluntario es una forma de conseguirlo.</p>

                                    <p class="padding-bottom-10">En Sudán, el proyecto de gestión de captación de Wadi El Ku trabaja con varias comunidades en torno a la fuente de agua más importante en el árido estado de Darfur Norte. Lanzado por el Programa de las Naciones Unidas para el Ambiente, junto con la Autoridad Regional de Darfur y el Gobierno del Estado de Darfur del Norte, y financiado por la Unión Europea, el proyecto ha movilizado fuertes normas culturales de colaboración al trabajar con voluntarios de las diferentes comunidades, para evaluar los niveles de agua, proporcionar servicios básicos y promover un enfoque holístico y cooperativo para la gestión de los recursos naturales. De esta manera, los voluntarios ayudan a vincular y mejorar las relaciones entre las comunidades vecinas que comparten un recurso natural esencial.</p>

                                    <p>En Myanmar, voluntarios de seis aldeas situadas junto al arroyo formaron la Creek Network («red del arroyo») para afrontar el problema de la contaminación por la extracción ilegal de oro, que estaba afectando a la salud y los medios de subsistencia de las personas y al medio ambiente. Durante dos años, la Creek Network trabajó con las administraciones locales para hacer frente a los buscadores de oro ilegales. Con el apoyo de organizaciones no gubernamentales, los voluntarios aprendieron a tomar muestras y a controlar la calidad del agua del arroyo, a documentar las infracciones de la minería y a notificar sus hallazgos a las autoridades. Lograron que se cerraran las minas ilegales y posteriormente controlaron el arroyo de forma regular. Ahora la Creek Network forma parte de redes nacionales y regionales y ha compartido sus experiencias con otras comunidades que afrontan problemas similares.</p>
                                 </div>
                              </div>
                          </div>
                          <div class="tab-pane fade" id="collection" role="tabpanel" aria-labelledby="collection-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Datos recopilados por voluntarios responsabilizan a contaminadores en China</p>
                                    <img src="images/5_data_collection.jpg" width="745px" alt="" class="desktop">

                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">En todo el mundo, las comunidades afrontan graves desafíos ambientales que amenazan la salud humana y los medios de vida. La ONG ambiental china Amigos de la Naturaleza trabaja con voluntarios locales para vigilar los riesgos ambientales para la comunidad y representarlos en mapas. Amigos de la Naturaleza ha presentado más de 30 causas judiciales contra fábricas e industrias contaminantes. Estos desafíos jurídicos se basan en pruebas recopiladas por voluntarios que aportan su conocimiento local, sus relaciones y su flexibilidad, y se coordinan a través de las nuevas tecnologías móviles e inteligentes. Este modelo, dirigido por voluntarios, ha inspirado a otras ONG ambientales y ha demostrado a los responsables políticos y las autoridades locales el valor de trabajar con voluntarios en la protección del medio ambiente.</p>

                                 </div>
                              </div>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-4" data-parallax="scrol" data-image-src="images/bg_chapter4.jpg" id="">
         <div class="container">
            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CAPITULO</p>
                           <p class="number text-color-aqua">04</p>
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">El voluntariado como recurso renovable</h2>
                           <p class="paragraph-17 text-color-white"><em>Este informe demuestra que los mil millones de voluntarios que hay en el mundo no son solo un recurso fundamental en épocas de crisis, sino que el voluntariado es, en sí mismo, un bien de las comunidades resilientes. Así pues, ¿cómo lograr que la resiliencia y el voluntariado se alimenten mutuamente a largo plazo?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-4 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_SP_WEB_CH4+CONC.pdf" target="_blank" class="btn border-aqua">Descargar capítulo 4 y conclusión</a>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-aqua paragraph-38 fontface-two padding-bottom-20">“Este trabajo no puede medirse únicamente en términos financieros. Sabemos lo que hacemos y nos valoramos".<span class="text-color-dark">– Voluntario local en Myanmar</span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-aqua paragraph-20"><em>Al igual que otras formas de participación ciudadana, el voluntariado es tanto un medio como un fin de desarrollo. Esta dualidad implica que, si bien la acción voluntaria puede ser un recurso renovable y una fuerza positiva para el desarrollo inclusivo y equitativo, también puede desperdiciar los recursos de las personas más vulnerables, o ser objeto de explotación por parte de actores externos.</em></p>

                           <p class="padding-bottom-10">Las políticas y la práctica deben centrarse en garantizar que el «voluntariado para la resiliencia» y el «servicio voluntario resiliente» se refuercen mutuamente.  Esto puede conseguirse de distintas formas: </p>
                           <p class="padding-bottom-10"><span class="bg-aqua">En primer lugar,</span> reconociendo que el voluntariado es un recurso fundamental para la resiliencia en todas las comunidades. Esto implica la necesidad de pasar de planteamientos ad hoc y «proyecto por proyecto» que funcionan con voluntarios a una infraestructura nacional de voluntariado que permita a todos los ciudadanos contribuir y colaborar con otros.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">En segundo lugar,</span>  garantizando una distribución más justa de los recursos entre los actores. Allí donde los voluntarios se lleven la peor parte de los riesgos, el apoyo técnico y financiero debe estar más disponible en el ámbito local para apoyar las iniciativas de los voluntarios y evitar que agoten sus limitados recursos.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">En tercer lugar,</span>   integrando mejor el voluntariado en las estrategias y planes nacionales de resiliencia, de formas que den voz a los voluntarios en la toma de decisiones en función de sus propios conocimientos y contribuciones, en lugar de esperar que pongan en práctica las prioridades de los demás de manera descendente.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">Por último,</span>  trabajando para dar forma a una distribución más equitativa del trabajo de los voluntarios dentro de las comunidades y entre ellas, lo que incluye la creación de oportunidades de desarrollo y liderazgo para grupos desfavorecidos y marginados.</p>
                           <p class="padding-bottom-10">Sin estas iniciativas, es poco probable que el voluntariado local sea sostenible, especialmente allí donde la carga de la gestión comunitaria recaiga desproporcionadamente sobre los más vulnerables. Este informe ofrece una visión alternativa para los gobiernos y sus socios de desarrollo, en la que el voluntariado sigue siendo un bien fundamental y un factor de protección para las comunidades resilientes de todo el mundo.</p>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="resilience-boxes">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 text-center">
                        <div class="" data-wow-delay="300ms">
                           <p class="paragraph-24 text-color-darkblue  font-weight-700 padding-bottom-10">Optimización de la relación entre el voluntariado y la resiliencia</p>
                           <p class="padding-bottom-40">En la siguiente figura se esquematizan las condiciones que ayudan o dificultan que el voluntariado y la resiliencia se refuercen mutuamente</p>
                           <!-- <p class="padding-bottom-40 desktop icon-hand-here">
                           </p> -->
                           <img class="image-hand padding-bottom-40" src="images/icon_hand_hoverover_sp.svg" alt="">

                           <img class="mobile" src="images/image_optimizing_sp.svg" width="945px" alt="">
                        </div>
                     </div>

                  </div>
                  <div class="row arrows-top desktop">
                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box1" style="background-image: url(images/more_res_com_sp.svg);background-size: 39px;">
                        <div class=" bg-rect-blue1 flip-container" data-wow-delay="300ms" ontouchstart="this.classList.toggle('hover');">
                           <div class="perspective-fix"></div>
                              <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-plus">Comunidades <br>más resilientes</span>
                                 <span class="text-color-white font-weight-700 white-minus">Voluntariado <br>menos resiliente</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-70">
                                    <li><span>La contribución de los voluntarios locales se reconoce e integra en estrategias de resiliencia nacionales y subnacionales.</span></li>
                                    <li><span>Se proporciona trabajo voluntario, pero de forma temporalmente limitada y proyecto por proyecto.</span></li>
                                    <li><span>La velocidad y disponibilidad de los voluntarios locales llegan fácilmente a las comunidades en crisis.</span></li>
                                    <li><span>Las iniciativas voluntarias locales se utilizan en proyectos e iniciativas de actores externos de forma descendente, con una voz mínima y retroalimentación ascendente.</span></li>
                                    <li><span>El ámbito de actuación de los voluntarios está muy controlado por gobiernos y otros actores.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box2" style="background-image: url(images/more_res_sp.svg);background-size: 485px; background-position: 60px 97.7%;">
                        <div class=" bg-rect-blue2 flip-container" data-wow-delay="400ms">
                           <div class="perspective-fix"></div>
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-plus">Comunidades <br>más resilientes</span>
                                 <span class="text-color-white font-weight-700 white-plus">Voluntariado <br>más resiliente</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-15">
                                    <li><span>Se establecen roles complementarios entre los distintos actores de la resiliencia, en función de sus fortalezas y contribuciones.</span></li>
                                    <li><span>Las colaboraciones son más estratégicas e intersectoriales, en lugar de proyectos ad hoc temporalmente limitados que implican a voluntarios locales.</span></li>
                                    <li><span>Los planteamientos se construyen a partir de las prioridades propias de los voluntarios, en lugar de usar a estos para que pongan en práctica iniciativas de forma descendente.</span></li>
                                    <li><span>El énfasis de la implicación de los voluntarios incluye la prevención y adaptación, así como cuestiones a largo plazo, en lugar de respuesta y mitigación inmediatas.</span></li>
                                    <li><span>La distribución de los recursos es proporcional a las responsabilidades, lo que otorga mayor control a los voluntarios locales, que son quienes asumen la carga del trabajo. </span></li>
                                    <li><span>Se crean oportunidades de servicio voluntario inclusivo para que este genere ventajas, como programas e iniciativas específicos para grupos marginados. </span></li>
                                    <li><span>Se crean vínculos entre distintos grupos de voluntarios que trabajan juntos, en lugar de con personas del mismo entorno.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
                  <div class="row  arrows-bottom desktop">
                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box3" style="background-image: url(images/less_res_vol_sp.svg);background-size: 485px;" >
                        <div class=" bg-rect-blue3 flip-container" data-wow-delay="300ms">
                           <div class="perspective-fix"></div>
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-minus ">Comunidades <br>menos resilientes</span>
                                 <span class="text-color-white font-weight-700 white-minus ">Voluntariado <br>menos resiliente</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-40">
                                    <li><span>Las oportunidades de servicio voluntario son exclusivas; por ejemplo, los más vulnerables solo participan en tareas voluntarias informales y de estatus inferior.</span></li>
                                    <li><span>El servicio voluntario es explotador; por ejemplo, se espera que los voluntarios desempeñen roles para otros actores, pero sin formación, recursos ni seguridad.</span></li>
                                    <li><span>El servicio voluntario es extractivo; por ejemplo, las agencias externas buscan aportaciones e información a través de sistemas de detección precoz, pero no comparten las suyas con las comunidades.</span></li>
                                    <li><span>El voluntariado tiene recursos insuficientes, lo que agota aún más los recursos limitados de personas ya vulnerables de las comunidades en su labor de gestión de los riesgos.</span></li>
                                    <li><span>El servicio de voluntariado está mal reconocido y valorado, e incluso puede llegar a verse estigmatizado.</span></li>
                                    <li><span>Los voluntarios se centran en acciones a corto plazo, como responder ante crisis, lo que puede llevar a un menor impacto y a que las personas se quemen.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-30 quote-box quote-box4" style="background-image: url(images/less_res_sp.svg);background-size: 39px;">
                        <div class=" bg-rect-blue4 flip-container" data-wow-delay="400ms">
                           <div class="perspective-fix"></div>
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-minus">Comunidades <br>menos resilientes</span>
                                 <span class="text-color-white font-weight-700 white-plus">Voluntariado <br>más resiliente</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-100">
                                    <li><span>El servicio voluntario genera una fuerte cohesión entre las personas de igual entorno que actúan juntas como voluntarias.</span></li>
                                    <li><span>Las comunidades tienen la capacidad de determinar las prioridades, pero ello implica decisiones sobre el alcance de su ayuda, tanto dentro como fuera de su comunidad.</span></li>
                                    <li><span>Existe una falta de coordinación entre el conjunto de actores más amplios y los voluntarios locales, lo que reduce el impacto.</span></li>
                                    <li><span>El voluntariado es reseñable, pero no está integrado en las estrategias ni los planes para la resiliencia.</span></li>
                                 </ul>
                              </div>
                           </div>

                        </div>
                     </div>

                  </div>
                  <a href="files/figure4_SP.pdf" target="_blank" class="dwn">Descargar la figura</a>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-top-bottom" id="sharing">
         <div class="container">

            <div class="">
               <div class="section-heading text-center">
                  <div class="row">
                     <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                        <h2 class="text-color-blue fontface-two padding-bottom-5" data-wow-delay="300ms">¡Comparta nuestros resultados principales!</h2>
                     </div>
                  </div>
               </div>
               <div class="padding-top-60 row">

                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card1.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">El voluntariado local es una estrategia de resiliencia fundamental y una propiedad de las comunidades resilientes.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 La escala y el alcance de la actividad de voluntariado para responder a las crisis y las tensiones son incomparables. Además, la contribución del voluntariado va mucho más allá de su magnitud, ya que, al igual que otros tipos de participación ciudadana, es tanto un medio para el desarrollo como un fin en sí mismo.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El+voluntariado+local+es+una+estrategia+de+resiliencia fundamental+y+una+propiedad+de+las+comunidades+resilientes.&image=share_card1'); ?>"><i class="fab fa-linkedin-in"></i></a>

                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El voluntariado local es una estrategia de resiliencia fundamental y una propiedad de las comunidades resilientes.&description=La escala y el alcance de la actividad de voluntariado para responder a las crisis y las tensiones son incomparables. Ademas, la contribucion del voluntariado va mucho mas alla de su magnitud, ya que, al igual que otros tipos de participacion ciudadana, es tanto un medio para el desarrollo como un fin en si mismo.&image=share_card1'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El voluntariado local es una estrategia de resiliencia fundamental y una propiedad de las comunidades resilientes.&description=La escala y el alcance de la actividad de voluntariado para responder a las crisis y las tensiones son incomparables. Ademas, la contribucion del voluntariado va mucho mas alla de su magnitud, ya que, al igual que otros tipos de participacion ciudadana, es tanto un medio para el desarrollo como un fin en si mismo.&image=share_card1'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=El%20voluntariado%20local%20es%20una%20estrategia%20de%20resiliencia%20fundamental%20y%20una%20propiedad%20de%20las%20comunidades%20resilientes.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card2.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">El voluntariado local favorece estrategias colectivas para gestionar el riesgo.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Al reunir acciones individuales bajo un objetivo común, el voluntariado amplía las opciones y oportunidades disponibles para las comunidades a medida que se preparan para las crisis y responden a ellas.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El+voluntariado+local+favorece+estrategias+colectivas+para+gestionar+el+riesgo.&image=share_card2'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El voluntariado local favorece estrategias colectivas para gestionar el riesgo.&description=Al reunir acciones individuales bajo un objetivo común, el voluntariado amplia las opciones y oportunidades disponibles para las comunidades a medida que se preparan para las crisis y responden a ellas.&image=share_card2'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El voluntariado local favorece estrategias colectivas para gestionar el riesgo.&description=Al reunir acciones individuales bajo un objetivo común, el voluntariado amplia las opciones y oportunidades disponibles para las comunidades a medida que se preparan para las crisis y responden a ellas.&image=share_card2'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=El voluntariado local favorece estrategias colectivas para gestionar el riesgo.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card3.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Las características del voluntariado local más valoradas por las comunidades son la capacidad de autoorganizarse y establecer conexiones con otras personas.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Los miembros de las comunidades valoran la capacidad de establecer sus propias prioridades de desarrollo y asumir la responsabilidad de los problemas locales. Las redes, la confianza y la empatía generadas a través de la acción social se reconocen en todos los contextos.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Las+caracteristicas+del+voluntariado+local+mas+valoradas+por+las+comunidades+son+la+capacidad+de+autoorganizarse+y+establecer+conexiones+con+otras+personas.&image=share_card3'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Las caracteristicas del voluntariado local mas valoradas por las comunidades son la capacidad de autoorganizarse y establecer conexiones con otras personas.&description=Los miembros de las comunidades valoran la capacidad de establecer sus propias prioridades de desarrollo y asumir la responsabilidad de los problemas locales. Las redes, la confianza y la empatia generadas a traves de la accion social se reconocen en todos los contextos.&image=share_card3'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Las caracteristicas del voluntariado local mas valoradas por las comunidades son la capacidad de autoorganizarse y establecer conexiones con otras personas.&description=Los miembros de las comunidades valoran la capacidad de establecer sus propias prioridades de desarrollo y asumir la responsabilidad de los problemas locales. Las redes, la confianza y la empatia generadas a traves de la accion social se reconocen en todos los contextos.&image=share_card3'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Las caracteristicas del voluntariado local mas valoradas por las comunidades son la capacidad de autoorganizarse y establecer conexiones con otras personas.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>


                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card4.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Estas características distintivas del voluntariado local pueden impulsar y, al mismo tiempo, disminuir la resiliencia comunitaria bajo diferentes condiciones.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Estas características distintivas del voluntariado local pueden impulsar y, al mismo tiempo, disminuir la resiliencia comunitaria bajo diferentes condiciones.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Estas+caracteristicas+distintivas+del+voluntariado+local+pueden+impulsar+y,+al+mismo+tiempo,+disminuir+la+resiliencia+comunitaria+bajo+diferentes+condiciones.&image=share_card4'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Estas caracteristicas distintivas del voluntariado local pueden impulsar y, al mismo tiempo, disminuir la resiliencia comunitaria bajo diferentes condiciones.&description=Estas caracteristicas distintivas del voluntariado local pueden impulsar y, al mismo tiempo, disminuir la resiliencia comunitaria bajo diferentes condiciones.&image=share_card4'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Estas caracteristicas distintivas del voluntariado local pueden impulsar y, al mismo tiempo, disminuir la resiliencia comunitaria bajo diferentes condiciones.&description=Estas caracteristicas distintivas del voluntariado local pueden impulsar y, al mismo tiempo, disminuir la resiliencia comunitaria bajo diferentes condiciones.&image=share_card4'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Estas caracteristicas distintivas del voluntariado local pueden impulsar y, al mismo tiempo, disminuir la resiliencia comunitaria bajo diferentes condiciones.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card5.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">El voluntariado es especialmente significativo para los grupos vulnerables y marginados.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 La ayuda mutua, la autoayuda y la reciprocidad son estrategias importantes de respuesta para las comunidades aisladas y vulnerables. Las acciones autoorganizadas pueden ayudar a los grupos marginales a satisfacer sus propias necesidades ante la ausencia de un conjunto más amplio de disposiciones y servicios.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El+voluntariado+es+especialmente+significativo+para+los+grupos+vulnerables+y+marginados.&image=share_card5'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El voluntariado es especialmente significativo para los grupos vulnerables y marginados.&description=La ayuda mutua, la autoayuda y la reciprocidad son estrategias importantes de respuesta para las comunidades aisladas y vulnerables. Las acciones autoorganizadas pueden ayudar a los grupos marginales a satisfacer sus propias necesidades ante la ausencia de un conjunto mas amplio de disposiciones y servicios.&image=share_card5'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El voluntariado es especialmente significativo para los grupos vulnerables y marginados.&description=La ayuda mutua, la autoayuda y la reciprocidad son estrategias importantes de respuesta para las comunidades aisladas y vulnerables. Las acciones autoorganizadas pueden ayudar a los grupos marginales a satisfacer sus propias necesidades ante la ausencia de un conjunto mas amplio de disposiciones y servicios.&image=share_card5'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=El voluntariado es especialmente significativo para los grupos vulnerables y marginados.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card6.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Los costes y beneficios del voluntariado no siempre se distribuyen equitativamente.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Es más probable que sean las mujeres quienes asuman la mayoría del servicio voluntario informal en sus propias comunidades (por ejemplo, como ampliación de las funciones de cuidado doméstico). No todo el mundo, especialmente quienes reciben bajos ingresos, puede acceder a oportunidades de voluntariado formal para desarrollar habilidades, crear nuevos vínculos y acceder a recursos.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Los+costes+y+beneficios+del+voluntariado+no+siempre+se+distribuyen+equitativamente.&image=share_card6'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Los costes y beneficios del voluntariado no siempre se distribuyen equitativamente.&description=Es mas probable que sean las mujeres quienes asuman la mayoria del servicio voluntario informal en sus propias comunidades (por ejemplo, como ampliacion de las funciones de cuidado domestico). No todo el mundo, especialmente quienes reciben bajos ingresos, puede acceder a oportunidades de voluntariado formal para desarrollar habilidades, crear nuevos vinculos y acceder a recursos.&image=share_card6'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Los costes y beneficios del voluntariado no siempre se distribuyen equitativamente.&description=Es mas probable que sean las mujeres quienes asuman la mayoria del servicio voluntario informal en sus propias comunidades (por ejemplo, como ampliacion de las funciones de cuidado domestico). No todo el mundo, especialmente quienes reciben bajos ingresos, puede acceder a oportunidades de voluntariado formal para desarrollar habilidades, crear nuevos vinculos y acceder a recursos.&image=share_card6'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Los costes y beneficios del voluntariado no siempre se distribuyen equitativamente.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>



                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card7.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">El modo en que interactúan los actores externos con el voluntariado local es relevante.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Las colaboraciones deben nutrir las características positivas del voluntariado que valoran las comunidades: sus propiedades de autoorganización y fortalecimiento de vínculos. Los actores de paz y desarrollo pueden debilitar el voluntariado si interactúan con las personas como si solo fueran un recurso barato y próximo. Si no se hacen bien, las colaboraciones con voluntarios locales pueden reforzar las desigualdades.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El+modo+en+que+interactúan+los+actores+externos+con+el+voluntariado+local+es+relevante.&image=share_card7'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El modo en que interactúan los actores externos con el voluntariado local es relevante.&description=Las colaboraciones deben nutrir las caracteristicas positivas del voluntariado que valoran las comunidades: sus propiedades de autoorganizacion y fortalecimiento de vinculos. Los actores de paz y desarrollo pueden debilitar el voluntariado si interactúan con las personas como si solo fueran un recurso barato y proximo. Si no se hacen bien, las colaboraciones con voluntarios locales pueden reforzar las desigualdades.&image=share_card7'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=El modo en que interactúan los actores externos con el voluntariado local es relevante.&description=Las colaboraciones deben nutrir las caracteristicas positivas del voluntariado que valoran las comunidades: sus propiedades de autoorganizacion y fortalecimiento de vinculos. Los actores de paz y desarrollo pueden debilitar el voluntariado si interactúan con las personas como si solo fueran un recurso barato y proximo. Si no se hacen bien, las colaboraciones con voluntarios locales pueden reforzar las desigualdades.&image=share_card7'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=El modo en que interactúan los actores externos con el voluntariado local es relevante.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card8.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">La colaboración efectiva con voluntarios puede hacer que el voluntariado pase de ser un mecanismo de respuesta a ser un recurso estratégico para la resiliencia comunitaria.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 La formación de alianzas complementarias con las comunidades ayuda a equilibrar los riesgos de una forma más equitativa, maximizando el potencial del servicio voluntario para tener un impacto positivo en aquellas personas que a menudo se quedan atrás. Reunir los recursos y las capacidades entre los actores de la manera adecuada permite a las comunidades adoptar iniciativas preventivas para lidiar con el riesgo a largo plazo.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=La+colaboracion+efectiva+con+voluntarios+puede+hacer+que+el+voluntariado+pase+de+ser+un+mecanismo+de+respuesta+a+ser+un+recurso+estrategico+para+la+resiliencia+comunitaria.&image=share_card8'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=La colaboracion efectiva con voluntarios puede hacer que el voluntariado pase de ser un mecanismo de respuesta a ser un recurso estrategico para la resiliencia comunitaria.&description=La formacion de alianzas complementarias con las comunidades ayuda a equilibrar los riesgos de una forma mas equitativa, maximizando el potencial del servicio voluntario para tener un impacto positivo en aquellas personas que a menudo se quedan atras. Reunir los recursos y las capacidades entre los actores de la manera adecuada permite a las comunidades adoptar iniciativas preventivas para lidiar con el riesgo a largo plazo.&image=share_card8'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=La colaboracion efectiva con voluntarios puede hacer que el voluntariado pase de ser un mecanismo de respuesta a ser un recurso estrategico para la resiliencia comunitaria.&description=La formacion de alianzas complementarias con las comunidades ayuda a equilibrar los riesgos de una forma mas equitativa, maximizando el potencial del servicio voluntario para tener un impacto positivo en aquellas personas que a menudo se quedan atras. Reunir los recursos y las capacidades entre los actores de la manera adecuada permite a las comunidades adoptar iniciativas preventivas para lidiar con el riesgo a largo plazo.&image=share_card8'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=La colaboracion efectiva con voluntarios puede hacer que el voluntariado pase de ser un mecanismo de respuesta a ser un recurso estrategico para la resiliencia comunitaria.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card9.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Un entorno propicio para el voluntariado fortalece la resiliencia comunitaria.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Los gobiernos y otros asociados pueden fortalecer la contribución del voluntariado al fomento de la resiliencia de dos formas: en primer lugar, promoviendo un ecosistema para el servicio voluntario eficaz y, en segundo lugar, formando colaboraciones basadas en un mayor reconocimiento del valor de las contribuciones propias de las comunidades. Esto garantizará que los procesos de localización realizados en el marco de la Agenda 2030 se construyan sobre la base del compromiso y las innovaciones de los ciudadanos de todo el mundo.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Un+entorno+propicio+para+el+voluntariado+fortalece+la+resiliencia+comunitaria.&image=share_card9'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Un entorno propicio para el voluntariado fortalece la resiliencia comunitaria.&description=Los gobiernos y otros asociados pueden fortalecer la contribucion del voluntariado al fomento de la resiliencia de dos formas: en primer lugar, promoviendo un ecosistema para el servicio voluntario eficaz y, en segundo lugar, formando colaboraciones basadas en un mayor reconocimiento del valor de las contribuciones propias de las comunidades. Esto garantizara que los procesos de localizacion realizados en el marco de la Agenda 2030 se construyan sobre la base del compromiso y las innovaciones de los ciudadanos de todo el mundo.&image=share_card9'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index_sp.php?title=Un entorno propicio para el voluntariado fortalece la resiliencia comunitaria.&description=Los gobiernos y otros asociados pueden fortalecer la contribucion del voluntariado al fomento de la resiliencia de dos formas: en primer lugar, promoviendo un ecosistema para el servicio voluntario eficaz y, en segundo lugar, formando colaboraciones basadas en un mayor reconocimiento del valor de las contribuciones propias de las comunidades. Esto garantizara que los procesos de localizacion realizados en el marco de la Agenda 2030 se construyan sobre la base del compromiso y las innovaciones de los ciudadanos de todo el mundo.&image=share_card9'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Un entorno propicio para el voluntariado fortalece la resiliencia comunitaria.&url=https://www.unv-swvr2018.org/index_sp.php"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>


               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="voices">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 text-center">
                        <h2 class="text-color-blue fontface-two padding-bottom-30" data-wow-delay="300ms">Opiniones de gente de todo el mundo</h2>
                        <!-- <p class="padding-bottom-50 icon-hand-here">
                        </p> -->
                        <img class="image-hand padding-bottom-50" src="images/icon_hand_hoverover_sp.svg" alt="">

                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-green flip-container" data-wow-delay="300ms" ontouchstart="this.classList.toggle('hover');">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VISIÓN DE UN ORGANISMO DE ESTADÍSTICA NACIONAL</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Voluntariado y producción estadística en el sur</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Isabel</p>
                              </div>
                              <div class="back">
                                 <p>Tshepiso retrocede y admira su obra. Como parte de su contribución al Día de Mandela, ha pintado las paredes interiores de una choza de hierro ondulado que sirve como guardería para niños pequeños en un asentamiento informal en Johannesburgo. Siguiendo los preceptos de Ubuntu,<sup>a</sup> suele llevar a sus ancianos padres, tías y tíos al hospital o les ayuda con la compra. Justo el fin de semana pasado, reparó la puerta rota de un mueble de cocina para su vecina, la Sra. Potts.</p>
                                 <p>Permitir que personas como Tshepiso estén al frente de sus propias prioridades de desarrollo y agendas del cambio es una de las piedras angulares del desarrollo sostenible. Una sociedad civil activa es un componente esencial de un estado cohesivo y que funciona bien. Mediante el fomento de una ciudadanía activa, es posible que el estado pueda lograr más cosas con menos recursos financieros y también una mayor cohesión social en el proceso. El voluntariado pregunta: ¿cómo puedo marcar la diferencia en toda mi familia, en mi comunidad, en mi país y a escala global?</p>
                                 <p><a href="https://www.unv.org/es/node/3882">MÁS INFORMACIÓN</a></p>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-blue flip-container" data-wow-delay="400ms">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VOCES DE LOS VOLUNATIOS</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">La acción colectiva siembra resiliencia en Guatemala</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Roselia</p>
                              </div>
                              <div class="back">
                                 <p>Hace diez años, a nadie le importaban los bosques de la comunidad. Seguimos cortando árboles. Entonces decidí crear un grupo de voluntarios para plantar árboles. Hablé con mujeres de la comunidad y muchas estuvieron interesadas en participar, y esto me motivó más todavía. Al principio éramos 50 mujeres, y un miembro de la comunidad nos prestó un terreno para plantar nuestros árboles. Así surgió nuestro grupo.</p>
                                 <p>El servicio voluntario me ayudó mucho. Hace diez años yo era una persona diferente. No conocía mis derechos. Antes, si un hombre me decía que no sabía nada, solía llorar y pensar: «Es verdad. Tiene razón.» Tenía miedo de hablar en presencia de hombres, pero ya no. Ahora discutimos y no tengo miedo de decir lo que pienso. Por ejemplo, un día alguien ofendió a las mujeres de la comunidad y yo las defendí. Las mujeres me dijeron: «Roselia, ya no tienes miedo a nada».</p>
                                 <p>En nuestro grupo, las mujeres toman sus propias decisiones. Antes no teníamos adónde ir y no había forma de participar. Antes solo era «casa y casa». Ahora tenemos un lugar donde podemos hablar, conocernos, relajarnos y ejercer nuestros derechos. En el vivero nos contamos nuestras alegrías y nuestros problemas. Estamos unidas.</p>
                                 <p>Queremos que más personas reforesten sus montañas en todo el municipio, por lo que vamos a difundir el mensaje de nuestro trabajo voluntario. </p>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-orange flip-container" data-wow-delay="300ms">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VISIÓN DE UN GOBIERNO MUNICIPAL</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Servicio voluntario: una herramienta para la integración social en las ciudades</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Matthew</p>
                              </div>
                              <div class="back">
                                 <p>Trabajo con el alcalde de Londres, Sadiq Khan, para unir a todos los londinenses y fortalecer nuestras comunidades. Una de las lecciones más importantes que he aprendido es el poder del servicio voluntario para alcanzar esos objetivos.</p>
                                 <p>A principios de este año lanzamos nuestra estrategia de integración social. A partir de investigaciones previas, se establece una nueva definición de integración social, resaltando que es algo más que simplemente la intensidad del contacto entre las personas, y que también incluye la promoción de la igualdad y la mejora de los niveles de actividad y de participación de las personas en sus comunidades locales.</p>
                                 <p>Pero alentar la integración social es un esfuerzo sin sentido si no se ofrece a las personas la oportunidad de reunirse. Para eso sirve el servicio voluntario.</p>
                                 <p>El servicio voluntario ayuda a los ciudadanos a relacionarse con otras personas de sus comunidades locales que pueden tener orígenes completamente diferentes. Crea vínculos e identidades compartidas que van más allá de las diferencias superficiales que de otro modo podrían parecer importantes. El servicio voluntario también proporciona una manera importante de lidiar con los problemas sociales (por ejemplo, reducir el aislamiento social o mejorar la salud mental), tanto para el voluntario como para la persona beneficiaria.</p>
                                 <p><a href="https://www.unv.org/es/node/3790">MÁS INFORMACIÓN</a></p>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-red flip-container" data-wow-delay="400ms">

                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VOCES DE LOS VOLUNATIOS</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Los voluntarios que trabajan en línea usan el mapeo geoespacial para las iniciativas de respuesta de emergencia</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Rohini</p>
                              </div>
                              <div class="back">
                                 <p>Como especialista geoespacial, durante las emergencias trabajo como voluntario para cartografiar las zonas afectadas a partir de datos de satélite. </p>
                                 <p> El 20 de septiembre de 2017, cerca de la medianoche, recibí un correo electrónico:</p>
                                 <p>«Estimados voluntarios de GISCorps: (...) Se busca ayuda para realizar una evaluación de daños de (...) ubicaciones de centros de salud afectadas por el huracán María en Puerto Rico. (...) Si te interesa y estás disponible, envía un correo electrónico (...)» </p>
                                 <p>Respondí inmediatamente, igual que hicieron otros cinco voluntarios de diferentes rincones del planeta. Trabajando juntos a través de un grupo en línea, analizamos millas y millas de datos en solo unos días, recopilación que habría llevado semanas desde el terreno. Tener acceso a este tipo de información en el momento adecuado puede agilizar las iniciativas de recuperación e incluso salvar vidas. El servicio voluntario en línea es una forma rentable y eficaz de llevar la información importante de los satélites a las personas en el terreno. También me facilita una forma de usar mis habilidades técnicas de manera significativa, formando parte de un panorama más amplio. Creo que las futuras iniciativas de recuperación y respuesta ante desastres dependerán cada vez más de los datos de teledetección, como los de los drones. Analizando esta información a través de plataformas de mapeo geoespacial de diversas fuentes, los voluntarios como yo pueden desempeñar un rol importante.</p>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>



      <section class="background-grey padding-top-bottom" id="">
         <div class="container">

                  <div class="row">
                     <div class="col-lg-9 offset-lg-2 col-md-12 offset-md-0 offset-sm-0">
                        <div class="" data-wow-delay="300ms">
                           <span class="quote-sign">“</span>
                           <!-- Slider main container -->
                           <div class="swiper-container testimonials">
                               <!-- Additional required wrapper -->
                               <div class="swiper-wrapper">
                                   <!-- Slides -->
                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">El voluntariado conecta a las personas, lo que les permite trabajar juntas para abordar las cuestiones apremiantes de nuestro tiempo. Para cumplir la promesa de que los Objetivos de Desarrollo Sostenible sean una realidad para todos, es necesario que sigamos el ejemplo de los mil millones de voluntarios que se calcula que existen actualmente, y que propiciemos el cambio en nuestras respectivas comunidades.</p>
                                       <span class="author">Achim Steiner</span>
                                       <span class="title">Administrador del Programa de las Naciones Unidas para el Desarrollo</span>
                                   </div>

                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">El servicio voluntario no es la única forma de mejorar la integración social, ni resuelve todos los problemas, pero es una herramienta enormemente importante que el gobierno y las autoridades locales pueden usar para unir a las personas. Sabemos que todos los londinenses quieren sentirse miembros valiosos de su comunidad y desempeñar un papel activo en las decisiones que dan forma a nuestra ciudad.</p>
                                       <span class="author">Matthew Ryder </span>
                                       <span class="title">Teniente de alcalde de Londres </span>
                                   </div>

                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">Mediante el fomento de una ciudadanía activa, es posible que el estado pueda lograr más cosas con menos recursos financieros y también una mayor cohesión social en el proceso. El voluntariado pregunta: ¿cómo puedo marcar la diferencia en toda mi familia, en mi comunidad, en mi país y a escala global?</p>
                                       <span class="author">Isabel Schmidt</span>
                                       <span class="title">Directora de South Africa Stats</span>
                                   </div>

                               </div>

                               <div class="swiper-pagination"></div>

                           </div>
                        </div>
                     </div>
                  </div>

         </div>
      </section>


      <section class="padding-top-bottom bg-setting padding-bottom-45" data-image-src="images/bg_cta.jpg" id="call-to-action">
        <img id="unv-circle" src="images/unv-circle.svg" alt=""/>
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="text-center" data-wow-delay="300ms">
                           <h2 class="fontface-two text-color-blue padding-bottom-60">Voluntariado y la Agenda 2030</h2>
                           <p class="">La Agenda 2030 requiere que todos los actores trabajen sobre la base del compromiso, la capacidad y la innovación de los ciudadanos de todo el mundo.</p>

                           <p class="">Entender a los voluntarios y colaborar con ellos de acuerdo con estándares de inclusión e igualdad fomenta un planteamiento del desarrollo centrado en las personas.</p>

                           <p class="">¿Las comunidades serán la última línea de defensa o la primera línea de prevención? </p>

                           <p class="">Eso dependerá del apoyo que los gobiernos y sus socios de desarrollo decidan ofrecer a los voluntarios que trabajan día tras día para fortalecer la resiliencia.</p>

                           <a href="https://www.unv.org/es/node/2866" target="_blank" class="btn btn-blue padding-top-50" style="padding-left: 87px;">Colabore</a>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-bottom-70 " id="footer">
         <div class="footer-top-bg padding-top-70 padding-bottom-45">
            <div class="container">
               <div class="row">
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">ACERCA DEL INFORME</p>
                        <p>Cada tres años, el programa de Voluntarios de las Naciones Unidas (UNV) elabora un informe sobre el estado del voluntariado en todo el mundo (SWVR), una publicación emblemática de la ONU que aspira a que se conozca mejor el voluntariado y a demostrar su universalidad, ámbito y alcance en el siglo veintiuno.</p>
                        <div class="footer-logo">
                           <a href="http://www.unv.org/swvr" target="_blank"><img src="images/logo_sp.svg" alt="logo"></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">PARA CONOCERNOS EN LÍNEA</p>
                        <ul class="footer-contact">
                           <li>
                              <a target="_blank" href="https://twitter.com/evidenceunv"><i class="fab fa-twitter"></i>@EvidenceUNV</a>
                           </li>
                           <li>
                              <a href="mailto:unv.swvr@unv.org"><i class="fas fa-envelope"></i>unv.swvr@unv.org</a>
                           </li>
                           <li>
                              <a href="https://www.unv.org/es/node/2865"><i class="fas fa-home"></i>www.unv.org/swvr</a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp footer-engage" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">PARTICIPE CON NOSOTROS</p>
                        <a href="https://www.unv.org/es/node/2910" target="_blank">Participe en un desafío de políticas o lanzamiento u organícelos</a>
                        <a href="https://www.unv.org/es/swvr/blogsubmissionguidelines" target="_blank">Contribuya a nuestro blog</a>
                        <a href="http://eepurl.com/c9U54H" target="_blank">Suscríbase a nuestra lista de correo</a>
                        <a href="mailto:unv.swvr@unv.org" target="_blank">Comparta sus opiniones y conocimientos</a>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">DESCARGAS</p>
                        <ul class="footer-contact">
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_SP_WEB.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Informe completo</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_SP_WEB_OVERVIEW.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Resumen</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_SP_WEB_CH1+INTRO.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Introducción y capítulo 1</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_SP_WEB_CH2.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Capítulo 2</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_SP_WEB_CH3.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Capítulo 3</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_SP_WEB_CH4+CONC.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Capítulo 4 y conclusión</a>
                           </li>
                           <li>
                              <a class="" href="files/51692_UNV_SWVR_2018_SP_WEB_ANNEX+NOTES+REFERENCES.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Anexidades</a>
                           </li>

                        </ul>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </section>



   </section>
   <!-- /Page Wrapper End -->

   <!-- jQuery v3.1.1 -->
   <script defer src="js/jquery.js"></script>

   <!-- Popper Core Javascript -->
   <script defer src="js/popper.min.js"></script>

   <!-- Bootstrap Core JavaScript -->
   <script defer src="js/bootstrap.min.js"></script>

   <!-- Appear Core Javascript -->
   <script defer src="js/jquery.appear.min.js"></script>

   <!-- Header Threads Files -->
   <script defer src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
   <script defer src="js/threads.js"></script>
   <script defer src="js/threads_init.js"></script>


   <!--  Fancybox Core Javascript -->
   <script defer src="js/jquery.fancybox.min.js"></script>

   <!-- Swiper Core Javascript -->
   <script defer src="js/swiper.min.js"></script>

   <!--wow Transitions-->
   <script defer src="js/wow.min.js"></script>

   <!-- Custom JavaScript -->
   <script defer src="js/script.js"></script>
   <script defer src="js/TweenMax.min.js"></script>
   <script defer src="js/CSSRulePlugin.min.js"></script>
   <script defer src="js/jquery.waypoints.min.js"></script>
   <script defer src="js/custom-animations-sp.js"></script>

</body>
</html>
