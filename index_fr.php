<!DOCTYPE html>

<?php

$params = array();
if(count($_GET) > 0) {
    $params = $_GET;
} else {
    $params = $_POST;
}
// defaults
if($params['type'] == "") $params['type'] = "website";
//if($params['locale'] == "") $params['locale'] = "en_US";
if($params['title'] == "") $params['title'] = "Volontariat et résilience communautaire";
if($params['image'] == "") $params['image'] = "bg_chapter1";
if($params['description'] == "") $params['description'] = "Rapport sur l'état du volontariat dans le monde 2018 ";

?>

<html lang="fr">

<head>
   <!-- Meta Tags For Seo + Page Optimization -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="2018 State of the world's volunteerism report">
   <meta name="author" content="">

   <meta property="fb:app_id" content="" />
    <meta property="og:site_name" content="meta site name"/>
    <meta property="og:url" content="https://www.unv-swvr2018.org/index.php?type=<?php echo $params['type']; ?>&title=<?php echo $params['title']; ?>&image=<?php echo $params['image']; ?>&description=<?php echo $params['description']; ?>"/>
    <meta property="og:type" content="<?php echo $params['type']; ?>"/>
    <!-- <meta property="og:locale" content="<?php echo $params['locale']; ?>"/> -->
    <meta property="og:title" content="<?php echo $params['title']; ?>"/>
    <meta property="og:image" content="https://www.unv-swvr2018.org/images/<?php echo $params['image']; ?>.jpg"/>
    <meta property="og:description" content="<?php echo $params['description']; ?>"/>

   <!-- Insert Favicon Here -->
   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
   <link rel="icon" href="images/favicon.ico" type="image/x-icon">

   <!-- Page Title(Name)-->
   <title>UNV - Rapport sur l'état du volontariat dans le monde 2018 </title>


   <!-- Bootstrap CSS Files -->
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/bootstrap-reboot.min.css">

   <!-- Animate CSS File -->
   <link rel="stylesheet" href="css/animate.css">

   <!-- Font-Awesome CSS File -->
   <link rel="stylesheet" href="css/fontawesome-all.min.css">

   <!-- Fancybox CSS File -->
   <link rel="stylesheet" href="css/jquery.fancybox.min.css">

   <!-- Swiper CSS File -->
   <link rel="stylesheet" href="css/swiper.min.css">

   <!-- Custom Style CSS File -->
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="css/custom-animations.css">
   

</head>

<body onload="threadsInit();" data-spy="scroll" data-target=".navbar" data-offset="50" class="index-one">

<!-- Page Loader -->
   <div class="preloader">
      <div class="loader-dot-outer">
         <div class="loader-dot-center"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
         <div class="loader-dot"></div>
      </div>
   </div>
   <!-- Page Loader -->


   <!-- Page Wrapper -->
   <section class="page-wrapper">

      <!-- Header Section Starts -->
      <header class="site-header">
         <nav class="navbar navbar-expand-lg navbar-transparent-white">
            <div class="container nav-logo-detail-outer">
               <!--side menu open button-->



               <a class="navbar-brand" href="index.php">
                  <img src="images/logo_fr.svg" alt="logo">
               </a>

               <div class="collapse navbar-collapse d-sm-0 d-md-0">
                  <ul class="navbar-nav ml-auto">
                     <li class="nav-item">
                        <a class="nav-link scroll active" href="#overview">Présentation</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-1">Ch. 1</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-2">Ch. 2</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-3">Ch. 3</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-4">Ch. 4</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#sharing">Conclusions</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://www.unv.org/fr/node/2865" target="_blank">Projet SWVR</a>
                     </li>
                  </ul>
               </div>

               <div class="lang" class="right">
                  <a href="index.php">EN </a> | <a href="index_sp.php">ES</a>
               </div>

               <div id="menu_bars" class="right"> <span></span> <span></span> <span></span> </div>

            <div class="sidebar_menu">
               <nav class="side-nav side-nav-right">
                  <span class="title mobile">NAVIGATION</span>
                  <ul class="side-nav-list mobile">
                     <li class="nav-item">
                        <a class="nav-link scroll active" href="#overview">Présentation</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-1">Chapitre 1</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-2">Chapitre 2</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-3">Chapitre 3</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#chapter-4">Chapitre 4</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link scroll" href="#sharing">Conclusions</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://www.unv.org/fr/node/2865" target="_blank">SWVR Project</a>
                     </li>
                  </ul>

                  <span class="title">TÉLÉCHARGEMENTS</span>
                  <ul class="side-nav-list">
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_FR_WEB.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Rapport complet</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_FR_WEB_OVERVIEW.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Présentation</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_FR_WEB_CH1+INTRO.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Introduction et chapitre 1</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_FR_WEB_CH2.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapitre 2</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_FR_WEB_CH3.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapitre 3</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_FR_WEB_CH4+CONC.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapitre 4 et conclusion</a>
                     </li>
                     <li>
                        <a class="" href="files/51692_UNV_SWVR_2018_FR_WEB_ANNEX+NOTES+REFERENCES.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Annexes</a>
                     </li>

                  </ul>


                  <div class="lang" class="right">
                     <a href="index.php">EN </a> | <a href="index_sp.php">ES</a>
                  </div>

                  <div class="bottom-share">
                     <ul class="social-icons padding-bottom-15">
                        <!-- <li>
                           <a href="https://www.google.com/"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-google"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                           <a href="https://www.google.com/"><i class="fab fa-twitter"></i></a>
                        </li> -->
                     </ul>
                     <p></p>
                  </div>
               </nav>
            </div>
         </div>
         </nav>

      </header>

      <section id="top">
            <div id="header">
                  <div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:1600px; height:720px">
                        <canvas id="canvas" width="1600" height="720" style="position: absolute; display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
                        <div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:1600px; height:720px; position: absolute; left: 0px; top: 0px; display: block;">
                        </div>
                  </div>
                  <div id="docDialog">
                        <p class="dlgHeader">Rapport sur l'état du volontariat dans le monde 2018</p>
                        <p class="dlgTitle">Le <span class="text-color-blue">fil</span> qui nous relie</p>
                        <p class="dlgText">Volontariat et résilience communautaire</p>
                        <div class="buttons">
                              <a href="files/51692_UNV_SWVR_2018_FR_WEB.pdf" target="_blank" class="button">TÉLÉCHARGER LE RAPPORT</a>
                              <a href="files/51692_UNV_SWVR_2018_FR_WEB_OVERVIEW.pdf" target="_blank" class="button" style="width:242px;">TÉLÉCHARGER LA PRÉSENTATION</a>
                        </div>
                  </div>
            </div>


            <script>
                  var H_WIDTH = 1600;
                  var H_HEIGHT = 720;

                  window.addEventListener('resize', onResize);
                  onResize();

                  function onResize() {
                        var header = document.getElementById('header');
                        var container = document.getElementById('animation_container');

                        var w = header.offsetWidth;
                        var scFix = (w < H_WIDTH) ? 1 : w / H_WIDTH;

                        container.style.transform = 'translate(-50%, -50%) scale(' + scFix + ')';
                  }
            </script>
      </section>


      <section class="background-white padding-top-bottom" id="overview">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 o-2">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="paragraph-15 padding-bottom-25 text-color-blue spacing uppercase">Présentation</p>
                           <h2 class="font-weight-700 padding-bottom-25"><span class="text-color-blue">Tisser de nouveaux modèles </span> de résilience </h2>
                           <p class="padding-bottom-20">Les volontaires agissent en premier dans tous les conflits majeurs, catastrophes naturelles et autres chocs graves. Moins visibles, chaque jour et partout, des individus de tous horizons se portent volontaires pour s'attaquer aux contraintes qui mettent leur résilience à l'épreuve, telles que l'instruction médiocre, la mauvaise santé et la pauvreté. Dans de nombreuses communautés, en particulier en l'absence de soutien public et de filets de protection, <strong> le volontariat apparaît comme une stratégie fondamentale de survie car il permet de mettre en œuvre des stratégies collectives de gestion des risques.</strong> </p>

                           <p class="padding-bottom-15">Le <strong>Rapport sur l'état du volontariat dans le monde 2018, <em>Le fil qui nous relie: Volontariat et résilience communautaire</em></strong>, examine comment les caractéristiques distinctives du volontariat local aident ou gênent les communautés en crise. Pour la première fois, le rapport s'appuie sur des <a href="https://www.unv.org/fr/node/2744" target="_blank"><u>recherches sur le terrain</u></a> menées par des volontaires, avec 1 200 participants dans 15 communautés différentes. Ce rapport explore également la manière dont les gouvernements et d'autres acteurs du développement peuvent s'associer à des solutions locales pour renforcer la résilience communautaire.</p>

                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-1 col-md-12 offset-md-0 offset-sm-0 padding-top-60 o-1">
                        <div class="wow fadeInUp mobile-margin" data-wow-delay="500ms">
                           <a href="https://youtu.be/stNaYexXJlI" data-fancybox class="">
                              <img src="images/image_video_fr.png" alt="">
                              <!-- <i class="fa fa-play"></i> -->
                           </a>
                           <p class="paragraph-15 text-center padding-top-20"><em>Consultez cette vidéo animée expliquant les découvertes et les recommandations du rapport</em></p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-blue padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">

                     <div class="col-lg-7 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp mobile-margin" data-wow-delay="500ms">
                           <img src="images/image_findings.png" width="" alt="">
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="300ms">

                           <p class="text-color-blue paragraph-60 fontface-two padding-bottom-20 padding-top-15">Principales Conclusions</p>

                           <p class="">Obtenez des informations sur les principales conclusions du rapport et partagez-les avec vos contacts.</p>

                           <a href="#sharing" class="btn btn-blue padding-top-50" style="background-position: 10px 18px; padding-left: 36px;">Principales conclusions</a>

                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-1" data-parallax="scrol" data-image-src="images/bg_chapter1.jpg" id="">
         <div class="container">

            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CHAPITRE</p>
                           <p class="number text-color-orange">01</p>
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Le volontariat est une ressource mondiale pour le développement </h2>
                           <p class="paragraph-17 text-color-white"><em>Qui sont les volontaires de par le monde ? Et que nous révèlent les estimations et tendances mondiales sur le volontariat en 2018 ? </em></p>

                        </div>
                     </div>

                     <div class="col-lg-4 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_FR_WEB_CH1+INTRO.pdf" target="_blank"" class="btn">Télécharger l'introduction et le chapitre 1</a>
                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-orange paragraph-38 fontface-two padding-bottom-20">“Pour développer un potentiel d'action et contribuer à ancrer solidement le nouveau programme, le volontariat apparaît comme un moyen de d’implémentation supplémentaire, puissant et transversal." <a href="http://www.un.org/ga/search/view_doc.asp?symbol=A/69/700&Lang=E" target="_blank"><span class="text-color-dark">- La dignité pour tous d'ici à 2030 (A/69/700)</span></a></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-orange paragraph-20"><em>Le volontariat fait partie du tissu de toutes les sociétés. Il peut être une ressource essentielle pour la paix et le développement, mais nous avons besoin de davantage d'éléments pour comprendre la valeur distinctive des contributions des volontaires à l'économie et à la société, en particulier dans des contextes fragiles.</em></p>

                           <p class="padding-bottom-10">L'analyse du Programme des Volontaires des Nations Unies (VNU) montre que les efforts des plus de 1 milliard de volontaires dans le monde équivalent au travail fourni par <a class="scroll" href="#scale-scope"><u>109 millions de travailleurs à temps plein.</u></a>.</p>
                           <p class="padding-bottom-10">Pour situer cela en contexte, l'effectif des volontaires correspond à près de trois fois le nombre des employés de par le monde dans les services financiers, et plus de cinq fois le chiffre du personnel des industries minières et extractives.</p>
                           <p class="padding-bottom-10"><strong>Le volontariat est universel. Des individus de tous âges et de tous horizons le pratiquent.</strong> L'accès à différents types d'opportunités de volontariat est néanmoins susceptible de varier en fonction de la nationalité, des revenus, du sexe et du statut social. L'immense majorité du travail des volontaires, 70 %, n'implique aucune organisation mais se fait de manière informelle entre les personnes, au sein de leurs communautés. Dans les initiatives de volontariat, les femmes sont généralement majoritaires (57 %) et occupent une part encore plus importante dans les actions non structurées (59 %) souvent dans le cadre de prestations de soins non rémunérées.</p>
                           <p class="padding-bottom-10"><strong>En 2018, les changements sociaux, environnementaux et technologiques influencent tous le volontariat. </strong> Plus de 90 pays dans le monde disposent désormais de politiques ou de législations concernant ce dernier. Les jeunes générations élaborent et pratiquent de nouvelles formes de volontariat contextuel s'appuyant souvent sur des technologies numériques pour se connecter. Tout cela revêt un aspect bien différent des tâches bénévoles locales et régulières que leurs parents effectuaient au sein de leurs communautés. </p>
                           <p class="padding-bottom-10">Si les gouvernements et d'autres entités doivent développer des efforts supplémentaires pour accroître les données et les preuves de volontariat, ces chiffres, tendances et modèles sont un point de départ permettant de mieux comprendre l'impact du volontariat sur les questions de paix et de développement à l'échelle mondiale en 2018.</p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="padding-top-bottom" id="scale-scope">
         <div class="container">
            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <p class="padding-bottom-35 paragraph-24 font-weight-700">Ampleur et contribution du volontariat à l'échelle mondiale</p>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link  active" id="type-tab" data-toggle="tab" href="#type" role="tab" aria-controls="type" aria-selected="false">Par type</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="region-tab" data-toggle="tab" href="#region" role="tab" aria-controls="region" aria-selected="false">Par région</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="type-region-tab" data-toggle="tab" href="#type-region" role="tab" aria-controls="type-region" aria-selected="false">Par type et par région</a>
                           </li>

                          <li class="nav-item">
                              <a class="nav-link" id="sex-tab" data-toggle="tab" href="#sex" role="tab" aria-controls="sex" aria-selected="false">Par sexe</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="sex-region-tab" data-toggle="tab" href="#sex-region" role="tab" aria-controls="sex-region" aria-selected="false">Par sexe et par type</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="workforce-tab" data-toggle="tab" href="#workforce" role="tab" aria-controls="workforce" aria-selected="true">Par effectif</a>
                          </li>



                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade show active" id="type" role="tabpanel" aria-labelledby="type-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">La majorité du volontariat au niveau mondial est non structuré</p>

                              <a href="files/bytype_fr.pdf" target="_blank" class="dwn">Télécharger la figure</a>
                          </div>

                          <div class="tab-pane fade" id="region" role="tabpanel" aria-labelledby="region-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Total du volontariat en équivalent temps plein par région</p>
                              <a href="files/byregion_fr.pdf" target="_blank" class="dwn">Télécharger la figure</a>
                          </div>

                          <div class="tab-pane fade" id="type-region" role="tabpanel" aria-labelledby="type-region-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Le volontariat non structuré dépasse le volontariat structuré dans toutes les régions</p>
                              <a href="files/bytypeandregion_fr.pdf" target="_blank" class="dwn">Télécharger la figure</a>
                          </div>

                          <div class="tab-pane fade" id="sex" role="tabpanel" aria-labelledby="sex-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">La part des femmes dans le volume de volontariat global est plus élevée dans toutes les régions sauf en Asie et dans le Pacifique</p>
                             <a href="files/bysex_fr.pdf" target="_blank" class="dwn">Télécharger la figure</a>
                          </div>

                          <div class="tab-pane fade" id="sex-region" role="tabpanel" aria-labelledby="sex-region-tab">
                              <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">Les femmes assument la plus grosse part du volontariat non structuré dans toutes les régions</p>
                             <a href="files/bysexandregion_fr.pdf" target="_blank" class="dwn">Télécharger la figure</a>
                          </div>
                          <div class="tab-pane fade" id="workforce" role="tabpanel" aria-labelledby="workforce-tab">
                             <p class="text-color-orange paragraph-20 font-weight-700 padding-bottom-35">L’effectif mondial des volontaires dépasse le nombre de personnes employées dans plus de la moitié des 10 pays les plus peuplés, 2016</p>
                             <a href="files/byworkforce_fr.pdf" target="_blank" class="dwn">Télécharger la figure</a>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-2" data-parallax="scrol" data-image-src="images/bg_chapter2.jpg"  id="">
         <div class="container">

            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CHAPITRE</p>
                           <p class="number text-color-red">02</p>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Volontariat local dans les communautés en difficulté</h2>
                           <p class="paragraph-17 text-color-white"><em>Le volontariat au niveau des communautés, à la fois structuré et non structuré, englobe un large éventail d’activités visant à soutenir la résilience communautaire. Mais au-delà de l'ampleur et de la contribution immenses des efforts des volontaires, les communautés en crise valorisent-elles le volontariat, et pourquoi ?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms ">
                           <a href="files/51692_UNV_SWVR_2018_FR_WEB_CH2.pdf" target="_blank" class="btn border-red">Télécharger le chapitre 2</a>
                        </div>
                     </div>


                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-red paragraph-38 fontface-two padding-bottom-20">“Les volontaires de la communauté étaient les seuls à lever la main... il y en avait aussi beaucoup qui disaient: ‘Personne d'autre ne le fera. Il n'y a que nous. C'est notre communauté’." <span class="text-color-dark">– Travailleur humanitaire de la  <span class="paragraph-50">C</span>roix-Rouge</span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-red paragraph-20"><em>Les membres de la communauté ont le sentiment que le travail bénévole renferme deux valeurs distinctives qui les aident à renforcer leur résilience:</em></p>

                           <p class="padding-bottom-10"> <span class="bg-red">Premièrement,</span> tous types de communautés valorisent le volontariat parce qu'il donne aux gens la <span class="font-weight-700">capacité de <span class="text-color-red">self-organize</span> en fonction de leurs propres priorités</span>. En se rassemblant pour faire du bénévolat, les individus peuvent créer des stratégies collectives afin de gérer le risque. Cela est important en temps de crise pour la rapidité de réponse, la flexibilité, l'appropriation et la représentation des efforts, particulièrement dans les communautés les plus isolées au sein desquelles d'autres types de soutien sont limités. Dans d'autres contextes, l'auto-organisation est plus importante pour les groupes marginalisés.</p> 
                           <p class="padding-bottom-10"><span class="bg-red">Deuxièmement,</span> par nature, <span class="font-weight-700">le volontariat s'organise autour des <span class="text-color-red">relations humaines</span></span>. Les participants à la recherche ont souligné le fait que la solidarité, l'empathie et les connexions générées par l'action sociale sont un facteur de protection lorsque les temps sont durs. Les réseaux et les liens humains permettent aux communautés de partager des informations sur les risques, de tendre la main à leurs membres les plus vulnérables et d'agir sur la base de l'empathie et de valeurs partagées. </p>
                           <p class="padding-bottom-10">Dans leurs grande majorité, les volontaires travaillent jour après jour dans leurs propres communautés. Mais le fait même que le volontariat fasse partie intégrante des communautés sous tension implique qu'il ne peut pas être idéalisé. Dans certains contextes, le volontariat peut avoir un impact moins positif sur la résilience.  </p>
                           <p class="padding-bottom-10">Par exemple, les efforts d'un volontaire local peuvent être efficaces en réponse à des chocs et des tensions, mais le manque de ressources peut ne pas leur permettre d'aider les communautés à empêcher les crises et à s'adapter. De nouveaux risques émergents tels que la variabilité climatique croissante mettent également sous tension des stratégies de volontariat traditionnelles.</p>
                           <p class="padding-bottom-10">En outre, du fait que le volontariat est ancré dans les relations sociales, il peut également être exclusif, relever de l'exploitation et accabler les plus vulnérables. La dynamique du pouvoir au sein des communautés, et entre elles, implique que ce sont souvent les femmes et les groupes marginalisés qui se chargent de la majorité des tâches de volontariat moins reconnues.  Dans le même temps, tout le monde n'a pas accès à des rôles de bénévolat augmentant les opportunités de leadership et les compétences.  </p>
                           <p class="padding-bottom-10">Ces constatations remettent en question l’hypothèse selon laquelle une focalisation au niveau local améliorerait automatiquement la participation et habiliterait les groupes de volontaires à œuvrer pour un changement en profondeur. Pour aider les communautés à rebondir, il est important de créer un environnement qui reconnaît et maximise les caractéristiques les plus positives du volontariat, et qui gère ses défis.</p>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>



      <section class="padding-top-bottom bg-setting"  id="foldout">
         <div class="container">

            <div class="">
               <div class="">

                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="paragraph-24 font-weight-700 text-color-darkblue text-center padding-bottom-20">Ce que les communautés apprécient concernant le volontariat pour la résilience</p>
                           <p class="text-center padding-bottom-20">Ce graphique montre de quelle manière les relations humaines et les caractéristiques d’auto-organisation du volontariat local améliorent ou limitent la résilience communautaire.</p>
                           <img class="image-hand padding-bottom-70" src="images/clicktoviewtext_fr.svg" alt="">
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <div class="list-box red text-color-white">
                              <div class="list-box-top">
                                 <h4>RELATIONS HUMAINES</h4>
                              </div>
                              <div class="row list-box-bot">
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">CONTRIBUTIONS POSITIVES</h5>
                                    <ul>
                                       <li><span>Confiance</span>
                                       <p>Lorsque la confiance parmi les volontaires est bien établie, l'action collective est renforcée.</p></li>
                                       <li><span>Solidarité</span>
                                       <p>L'action volontaire peut renforcer la solidarité ou le “pouvoir ensemble” grâce à une assistance mutuelle.</p></li>
                                       <li><span>Cohésion</span>
                                       <p>L'action volontaire permet de renégocier les relations entre les groupes divisés et encourage la formation de réseaux de personnes partageant les mêmes causes.</p></li>
                                       <li><span>Soutien émotionnel</span>
                                       <p>Les volontaires issus de la communauté sont plus susceptibles de s'identifier à ceux qui souffrent et de les aider, ce qui peut réduire les sentiments d’aliénation et d’isolement.</p></li>
                                       <li><span>Accès local</span>
                                       <p>Les volontaires locaux ont des liens avec les groupes vulnérables et y sont bien accueillis.</p></li>
                                       <li><span>Connaissance du contexte</span>
                                       <p>Les volontaires locaux peuvent restituer le contexte des informations sur la communauté pour les acteurs externes.</p></li>
                                       <li><span>Liens avec des réseaux plus larges</span>
                                       <p>Lorsque les volontaires locaux agissent pour le compte d'organisations, ils peuvent jouer un rôle de relais entre les acteurs locaux et nationaux ou internationaux.</p></li>
                                    </ul>
                                 </div>
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">LIMITES ET MENACES</h5>
                                    <ul>
                                       <li><span>Vision à court terme</span>
                                       <p>Le volontariat fondé sur la solidarité sociale et les liens affectifs permet de prioriser les besoins immédiats et urgents par rapport à la prévention et l’adaptation à long terme.</p></li>
                                       <li><span>Exclusion</span>
                                       <p>La solidarité et l'action volontaire collective peuvent conduire à l'exclusion des groupes externes.</p></li>
                                       <li><span>Division</span>
                                       <p>En situation de crise, les groupes de volontaires locaux sont peu enclins à épauler des personnes auxquelles ils ne s'identifient pas ou dont ils ne partagent pas les points de vue.</p></li>
                                       <li><span>Absence d'écoute locale</span>
                                       <p>Les relations des volontaires sont souvent axées sur les besoins internes, et des déséquilibres de pouvoir ainsi qu'un manque d’affiliation peuvent limiter leur prise en compte des connaissances locales.</p></li>
                                       <li><span>Conflit interne</span>
                                       <p>Lorsqu'ils entreprennent des actions qui vont à l'encontre des décisions communautaires plus larges ou qui perturbent le statu quo, les groupes de volontaires composés de populations marginalisées peuvent provoquer un conflit intercommunautaire.</p></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <div class="list-box blue text-color-white">
                              <div class="list-box-top">
                                 <h4>AUTO-ORGANISATION</h4>
                              </div>
                              <div class="row list-box-bot">
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">CONTRIBUTIONS POSITIVES</h5>
                                    <ul>
                                       <li><span>Rapidité et immédiateté</span>
                                       <p>Les volontaires locaux apportent une première réponse immédiate en cas de crise.</p></li>
                                       <li><span>Échelle</span>
                                       <p>Le volontariat spontané peut mobiliser un grand nombre de personnes en temps de crise ; lorsque les volontaires sont très dispersés géographiquement, cela permet de détecter rapidement les menaces.</p></li>
                                       <li><span>Disponibilité</span>
                                       <p>Les volontaires locaux sont souvent les seuls soutiens disponibles en cas de crise et peuvent s'organiser lorsque les autorités centralisées ne sont pas présentes pour guider et coordonner une intervention d'urgence.</p></li>
                                       <li><span>Flexibilité</span>
                                       <p>L'action volontaire locale non structurée est moins dépendante des méthodes et des procédures standards et peut plus facilement être adaptée à l'évolution des conditions locales.</p></li>
                                       <li><span>Innovation</span>
                                       <p>Souvent, les volontaires locaux résolvent les problèmes en fonction des besoins immédiats et des ressources disponibles.</p></li>
                                       <li><span>Appropriation</span>
                                       <p>Les priorités auto-déterminées et le contrôle limité des acteurs externes favorisent une réponse volontaire et l'appropriation des solutions.</p></li>
                                       <li><span>Rentabilité</span>
                                       <p>Les efforts pour s'organiser s’appuient sur les ressources disponibles et les dons en nature des volontaires.</p></li>
                                    </ul>
                                 </div>
                                 <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                                    <h5 class="font-weight-700 padding-bottom-30">LIMITES ET MENACES</h5>
                                    <ul>
                                       <li><span>Exploitation</span>
                                          <p>Les volontaires locaux qui s'organisent pour répondre à des besoins particuliers peuvent être utilisés comme main-d'œuvre à moindre coût insuffisamment rémunérée ou soutenue.</p><li>
                                       <li><span>Substitution</span>
                                          <p>Les volontaires locaux comblent les lacunes des services gouvernementaux, ce qui peut dissuader les États d'investir.</p><li>
                                       <li><span>Obligation</span>
                                          <p>Certaines stratégies de résilience communautaire locales contraignent les individus à une « participation volontaire », les personnes qui ne participent pas à l'activité étant condamnées à une amende, rejetées par la société ou privées de l'accès aux biens ou services collectivement produits.</p><li>
                                       <li><span>Échelle</span>
                                          <p>Dans certains contextes, l'auto-organisation ne permet pas de rassembler efficacement un nombre suffisant de volontaires locaux pendant les crises.</p><li>
                                       <li><span>Isolement</span>
                                          <p>Les volontaires qui n'ont pas de liens avec les services classiques dépendent des ressources locales.</p><li>
                                       <li><span>Segmentation</span>
                                          <p>Le volontariat local est souvent une stratégie de survie pour les groupes vulnérables ou minoritaires qui s’auto-organisent afin de répondre à des besoins spécifiques qui ne sont pas satisfaits par la communauté au sens large. Cela ne permet pas de contrer les schémas de marginalisation, voire alourdit le fardeau des plus vulnérables.</p><li>
                                    </ul>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </div>

                  </div>


               </div>
            </div>
         </div>
      </section>





      <section class="bg-setting height-100 chapter" id="chapter-3" data-parallax="scrol" data-image-src="images/bg_chapter3.jpg" id="">
         <div class="container">
            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CHAPITRE</p>
                           <p class="number text-color-green">03</p>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Collaborations avec le volontariat pour la résilience de la communauté</h2>
                           <p class="paragraph-17 text-color-white"><em>Il est important de considérer la manière dont les acteurs extérieurs s’engagent dans le volontariat local si ce dernier est une stratégie de résilience fondamentale. Comment les gouvernements et leurs partenaires du développement peuvent-ils compléter au mieux l'action volontaire locale ?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_FR_WEB_CH3.pdf" target="_blank" class="btn border-green">Télécharger le chapitre 3</a>
                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </section>



      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-35">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-green paragraph-38 fontface-two padding-bottom-20">“En tant que volontaires, nous pouvons facilement voir les limites de ce que nous faisons. Nous manquons des ressources nécessaires ; nous avons vraiment besoin d'une aide extérieure en cas de crise." <span class="text-color-dark">– Participant à un groupe de discussion, Burundi, recherche sur le terrain dans le cadre du Rapport sur l'état du volontariat dans le monde </span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-green paragraph-20"><em>Lorsque les capacités locales sont poussées dans leurs retranchements ou lorsque les chocs et les tensions proviennent de l'extérieur de leurs communautés, la collaboration entre les acteurs externes et le volontariat local devient importante. </em></p>

                           <p class="padding-bottom-10">Directement ou indirectement, de nombreux gouvernements et leurs partenaires de développement influencent déjà le volontariat local. Les politiques, les programmes, les normes et standards ont tous un impact sur la capacité du volontariat à renforcer la résilience.</p>
                           <p class="padding-bottom-10">Bien gérées, les collaborations peuvent être extrêmement complémentaires, permettant aux volontaires locaux de passer d'une simple prise en charge à une prévention des chocs et un soutien aux communautés pour qu'elles s'adaptent. Les partenariats peuvent également contribuer à intégrer des normes et des standards plus inclusifs et équitables permettant à toutes sortes de personnes de bénéficier d'opportunités de volontariat.</p>
                           <p class="padding-bottom-10">Inversement, lorsque des acteurs considèrent le volontariat local comme une simple ressource bon marché et disponible, le volontariat local peut être mis à mal. Les volontaires peuvent alors être surchargés, exploités et bénéficier de peu d'opportunités de cultiver leurs contributions distinctives telles que les connaissances locales, les relations centrées sur l'humain, la représentation, l'autonomie et la flexibilité. <strong>Le volontariat peut lui-même devenir moins résilient.</strong></p>
                           <p class="padding-bottom-10">Les partenariats entre les volontaires locaux et les acteurs externes doivent être structurés dans un esprit de vraie collaboration s'appuyant sur les forces du volontariat pour la résilience. Il est essentiel d'accorder une place suffisante aux volontaires locaux pour leur permettre de se rassembler en vue d'innover et de résoudre des problèmes, tout en soutenant leurs efforts et en les associant à ceux d'acteurs complémentaires. </p>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="padding-top-bottom" id="examples-tabs">
         <div class="container">
            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <p class="padding-bottom-35 paragraph-24 font-weight-700">Exemples de collaboration efficace avec le volontariat local</p>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link  active" id="health-tab" data-toggle="tab" href="#health" role="tab" aria-controls="online" aria-selected="false">Promotion de la santé</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="online-tab" data-toggle="tab" href="#online" role="tab" aria-controls="online" aria-selected="false">Volontariat en ligne</a>
                           </li>

                          <li class="nav-item">
                              <a class="nav-link" id="opensource-tab" data-toggle="tab" href="#opensource" role="tab" aria-controls="opensource" aria-selected="false">Cartographie open source</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="environmental-tab" data-toggle="tab" href="#environmental" role="tab" aria-controls="environmental" aria-selected="false">Protection de l'environnement</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" id="data" data-toggle="tab" href="#collection" role="tab" aria-controls="collection" aria-selected="true">Collecte de données</a>
                          </li>



                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade show active" id="health" role="tabpanel" aria-labelledby="health-tab">
                              <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Le rôle critique des agents bénévoles chargés de la promotion de la santé de la communauté</p>
                                    <img src="images/1_health_promotion.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Les volontaires chargés de la promotion de la santé sont actifs dans presque toutes les communautés à faible revenu de recherche sur le terrain, en particulier dans les zones reculées et vulnérables, hors de portée des services publics. Ces volontaires transmettent des informations sur la nutrition, la santé des femmes et des enfants, la santé reproductive et d’autres domaines ayant trait aux soins de santé primaires et à la prévention des maladies. Ils sont souvent perçus comme ayant une meilleure connaissance des besoins et des problèmes de la communauté que les professionnels du service de santé public.</p>

                                    <p class="padding-bottom-10">Mais malgré cela, ils ont eu du mal à faire leur travail. La plupart ont reçu une formation initiale et un soutien du gouvernement ou des agences de développement, mais ont généralement indiqué qu'ils souhaitaient mettre fin à leurs activités de promotion de la santé peu après, faute de soutien. Les volontaires qui choisissent de poursuivre leur mission le font souvent à un coût considérable d'un point de vue personnel. L'un des nombreux volontaires de la communauté de recherche sur le terrain au Guatemala décrit bien leur situation:</p>

                                    <p><em>Pourquoi le gouvernement ne nous soutient-il pas davantage ? Imaginez un peu... nous faisons notre travail, en sauvant des vies... mais nous ne recevons aucune incitation. Je paie moi-même mon transport. Quand j'ai commencé, j'ai acheté mes ciseaux, un gabacha [blouse], une casserole pour faire bouillir de l'eau, un parapluie, car il faut parfois sortir sous la pluie, un sac à dos, une paire de bottes. Nous achetons tout cela nous-mêmes. Mais nous n'avons pas le choix lorsque les mères elles-mêmes viennent nous chercher !</em></p>
                                 </div>
                              </div>
                           </div>

                          <div class="tab-pane fade" id="online" role="tabpanel" aria-labelledby="online-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Relier diverses compétences et connaissances par le biais du volontariat en ligne</p>
                                    <img src="images/2_online_volunteering.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Le programme des Volontaires des Nations Unies gère le <a href="www.onlinevolunteering.org" target="_blank">service de volontariat en ligne des Nations Unies</a>, une plateforme dédiée qui mobilise plus de 12 000 volontaires en ligne chaque année. Le volontariat en ligne est un moyen simple, universel et efficace pour les organisations et les bénévoles de travailler ensemble pour relever des défis du développement durable partout dans le monde, depuis n'importe quel appareil.</p>

                                    <p class="padding-bottom-10">Depuis juin 2014, les Volontaires en ligne des Nations Unies apportent un soutien technique au groupe Agriculteurs Professionnels du Cameroun, un projet de développement rural dans le village de Tayap, dans le bassin du Congo, une région qui a beaucoup subi de lourdes pertes en termes d'habitat et de biodiversité. Le projet vise à promouvoir des moyens de subsistance durables et la résilience des communautés. Les Volontaires en ligne des Nations Unies forment une équipe incluant: un expert en informatique du Burkina Faso qui crée des cartes du village, un ingénieur agricole du Togo qui analyse des images satellites de la couverture forestière et un expert français en énergies renouvelables qui développe un projet d’énergie solaire pour le village. Le soutien multidisciplinaire constant fourni en ligne par ces volontaires internationaux a été déterminant pour la réussite du projet, qui a remporté plusieurs prix et bourses.</p>

                                 </div>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="opensource" role="tabpanel" aria-labelledby="opensource-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Utiliser des logiciels open source pour surveiller et signaler les crises</p>
                                    <img src="images/3_online_mapping.png" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Les logiciels de cartographie open source sont un outil puissant pour les volontaires qui répondent aux crises. Ushahidi est une plateforme open source de participation volontaire à la cartographie des données active depuis plus de 10 ans. Lancée en 2007 pour le suivi des rapports sur les violences post-électorales au Kenya, Ushahidi a été affinée par des volontaires et étendue à d'autres utilisations et contextes. Les volontaires ont utilisé la plateforme pour surveiller et consigner les événements lors des élections générales de 2017 au Kenya, y compris la radiation de certains électeurs, les irrégularités de scrutin et les cas de violence. </p>

                                    <p class="padding-bottom-10">Fondés sur ce modèle, les logiciels open source sont de plus en plus utilisés en situation d'urgence dans le monde entier.  Par exemple, lors du séisme de 2017 au Mexique, des milliers de volontaires ont traduit des milliers de messages texte et de publications sur les réseaux sociaux envoyés par des personnes ayant besoin d'aide. Ils ont pu géolocaliser ces messages, étiqueter leur emplacement et communiquer les informations cartographiées aux intervenants sur le terrain. Des exemples similaires illustrent la manière dont les logiciels libres ont aidé les communautés à faire face à d’autres crises récentes telles que le séisme de 2015 au Népal, l’épidémie d’Ebola en Afrique de l’Ouest en 2014-16, les violences de la guerre civile en Syrie et les ouragans Harvey et Irma en 2017.</p>

                                 </div>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="environmental" role="tabpanel" aria-labelledby="envirionmental-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Le volontariat intercommunautaire pour protéger les ressources naturelles partagées</p>
                                    <img src="images/4_environmental_protection.jpg" width="745px" alt="" class="desktop">
                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">De nombreux dangers risquant d'affecter la résilience traversent les frontières de la communauté. Une gestion efficace de ces risques exige donc une coopération entre les communautés. Le volontariat est un moyen de la mettre en œuvre.</p>

                                    <p class="padding-bottom-10">Au Soudan, le projet de gestion du bassin versant du Wadi El Ku implique plusieurs communautés vivant autour de la ressource hydrique la plus importante du nord aride du Darfour. Lancé par UN Environment avec l'Autorité régionale du Darfour et le gouvernement de l'État du Darfour-Nord et financé par l'Union européenne, le projet a établi des normes de collaboration culturelle solides entre les volontaires des différentes communautés afin qu'ils puissent travailler ensemble pour évaluer les niveaux d'eau, fournir des services de base et préconiser une approche globale et coopérative de la gestion des ressources naturelles. De cette façon, les volontaires contribuent à tisser des liens et à améliorer les relations entre les communautés voisines qui partagent cette ressource si importante.</p>

                                    <p>Au Myanmar, des volontaires de six villages situés au bord d'un ruisseau ont créé le Creek Network afin de résoudre le problème de la pollution dû à l'extraction illégale de l'or qui affectait la santé, les moyens de subsistance et l'environnement. Pendant deux ans, le Creek Network a collaboré avec les administrations locales pour lutter contre l'extraction illégale d'or. Avec le soutien d'organisations non gouvernementales, des volontaires ont appris à échantillonner et à tester l'eau des ruisseaux, à documenter les violations des droits miniers et à communiquer les résultats aux autorités. Ils ont réussi à faire fermer les mines abusives et ont ensuite surveillé régulièrement le ruisseau. Le Creek Network fait désormais partie des réseaux nationaux et régionaux et partage ses expériences avec d'autres communautés confrontées à des problèmes similaires.</p>
                                 </div>
                              </div>
                          </div>
                          <div class="tab-pane fade" id="collection" role="tabpanel" aria-labelledby="collection-tab">
                             <div class="row">
                                 <div class="col-lg-3 offset-lg-0 col-md-3 offset-md-0 offset-sm-0">
                                    <p class="text-color-green paragraph-20 font-weight-700 padding-bottom-35">Les données collectées par les volontaires obligent les pollueurs chinois à rendre des comptes</p>
                                    <img src="images/5_data_collection.jpg" width="745px" alt="" class="desktop">

                                 </div>
                                 <div class="col-lg-9 offset-lg-0 col-md-9 offset-md-0 offset-sm-0">
                                    <p class="padding-bottom-10">Dans le monde entier, les communautés sont confrontées à de graves problèmes environnementaux qui menacent la santé humaine et les moyens de subsistance. L'ONG environnementale chinoise Friends of Nature fait appel à des volontaires locaux pour cartographier et surveiller les risques environnementaux au niveau communautaire. Friends of Nature a lancé plus de 30 procédures judiciaires contre des usines et des industries polluantes. Ses accusations reposent sur des preuves collectées par des bénévoles locaux qui tirent parti de leurs connaissances, de leurs relations et de leur souplesse d'action et échangent ces informations par le biais de nouvelles technologies mobiles et intelligentes. Ce modèle axé sur les volontaires est devenu une source d'inspiration pour d'autres ONG environnementales et a prouvé aux décideurs politiques et aux autorités locales l'intérêt de collaborer avec des volontaires pour la protection de l'environnement.</p>

                                 </div>
                              </div>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="bg-setting height-100 chapter" id="chapter-4" data-parallax="scrol" data-image-src="images/bg_chapter4.jpg" id="">
         <div class="container">
            <div class="container">
               <div class="">
                  <div class="row">
                     <div class="col-lg-2 offset-lg-1 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp text-center" data-wow-delay="100ms">
                           <p class="paragraph-22 text-color-white padding-bottom-10">CHAPITRE</p>
                           <p class="number text-color-aqua">04</p>
                        </div>
                     </div>

                     <div class="col-lg-5 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="chapter-title" data-wow-delay="100ms">
                           <h2 class="padding-bottom-30">Le volontariat en tant que ressource renouvelable</h2>
                           <p class="paragraph-17 text-color-white"><em>Ce rapport démontre que le milliard de bénévoles de par le monde n'est pas juste une ressource essentielle en temps de crise, mais que le volontariat est lui-même une propriété des communautés résilientes. Ainsi, comment la résilience et le volontariat peuvent-ils tous deux être cultivés ensemble sur le long terme ?</em></p>

                        </div>
                     </div>

                     <div class="col-lg-4 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 relative">
                        <div class="wow fadeIn" data-wow-delay="100ms">
                           <a href="files/51692_UNV_SWVR_2018_FR_WEB_CH4+CONC.pdf" target="_blank" class="btn border-aqua">Télécharger le chapitre 4 et la conclusion</a>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-top-bottom" id="">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-3 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp" data-wow-delay="300ms">
                           <p class="text-color-aqua paragraph-38 fontface-two padding-bottom-20">“Ce travail ne peut pas être mesuré selon des critères financiers. Nous savons ce que nous faisons: nous connaissons notre valeur.” <span class="text-color-dark">– Volontaire local, Myanmar</span></p>
                        </div>
                     </div>

                     <div class="col-lg-9 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="wow fadeInUp pull-right" data-wow-delay="500ms">

                           <p class="padding-bottom-40 text-color-aqua paragraph-20"><em>Comme d'autres formes de participation civique, le volontariat est à la fois un moyen et une fin pour le développement. Cette dualité signifie que si l’action volontaire peut être une ressource renouvelable et une force positive pour un développement inclusif et équitable, elle peut aussi gaspiller les ressources des personnes les plus vulnérables ou être exploitée par des acteurs extérieurs. </em></p>

                           <p class="padding-bottom-10">Les politiques et la pratique doivent essentiellement s'assurer de renforcer mutuellement le ‘volontariat pour la résilience’ et le ‘volontariat résilient ’.   Cela peut être obtenu de différentes manières: </p> 
                           <p class="padding-bottom-10"><span class="bg-aqua">Premièrement,</span> en reconnaissant que le volontariat est une ressource fondamentale pour la résilience dans toutes les communautés. Cela signifie le besoin de passer d'approches ad-hoc et ‘projet par projet’ qui fonctionnent avec les volontaires à une infrastructure nationale pour le volontariat permettant à tous les citoyens d'apporter leur contribution et de former des partenariats.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">Deuxièmement,</span>  en garantissant une répartition plus juste des ressources entre les acteurs. Lorsque les volontaires essuient la majorité des risques, un soutien technique et financier devrait être plus facilement accessible au niveau local afin de soutenir leurs efforts et d'éviter d'épuiser leurs ressources limitées.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">Troisièmement,</span>   en intégrant mieux le volontariat dans des plans et des stratégies de résilience nationale, de manières qui permettent aux volontaires d'avoir voix au chapitre en fonction de leurs propres connaissances et de leurs contributions, au lieu d'attendre d'eux qu'ils mettent en place les priorités de tiers suivant une approche hiérarchique descendante.</p>
                           <p class="padding-bottom-10"><span class="bg-aqua">Enfin,</span>  en s'efforçant de mettre en place une répartition plus équitable du bénévolat au sein des communautés et entre elles, y compris en ouvrant des opportunités de développement et de leadership pour des groupes marginalisés et défavorisés.</p>
                           <p class="padding-bottom-10">Sans de tels efforts, le volontariat local est peu susceptible d’être durable, particulièrement lorsque le fardeau de l’adaptation par la communauté repose de manière disproportionnée sur les plus vulnérables. Ce rapport fournit un autre point de vue aux gouvernements et à leurs partenaires de développement: une vision selon laquelle le volontariat demeure une propriété exclusive et un facteur de protection pour les communautés résilientes, en tout lieu.</p>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="resilience-boxes">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 text-center">
                        <div class="" data-wow-delay="300ms">
                           <p class="paragraph-24 text-color-darkblue  font-weight-700 padding-bottom-10">Optimiser la relation entre le volontariat et la résilience</p>
                           <p class="padding-bottom-40">La figue ci-dessous établit le profil des conditions qui favorisent ou entravent le renforcement mutuel du volontariat et de la résilience</p>
                           <!-- <p class="padding-bottom-40 desktop icon-hand-here">
                           </p> -->
                           <img class="image-hand padding-bottom-40" src="images/hoverovermouse_fr.svg" alt="">

                           <img class="mobile" src="images/image_optimizing_fr.svg" width="945px" alt="">
                        </div>
                     </div>

                  </div>
                  <div class="row arrows-top desktop">
                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box1" style="background-image: url(images/more_res_com_fr.svg);background-size: 39px;">
                        <div class=" bg-rect-blue1 flip-container" data-wow-delay="300ms" ontouchstart="this.classList.toggle('hover');">
                           <div class="perspective-fix"></div>
                               <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-plus">Des communautés <br>plus résilientes</span>
                                 <span class="text-color-white font-weight-700 white-minus">Un volontariat <br>moins résilient</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-70">
                                    <li><span>La contribution des volontaires locaux est reconnue et intégrée dans des stratégies de résilience nationales et infranationales.</span></li>
                                    <li><span>Le travail bénévole bénéficie de ressources mais sur une base projet par projet, avec une limite dans le temps.</span></li>
                                    <li><span>La rapidité et la disponibilité des volontaires locaux sont déployées facilement pour aider les communautés en crise.</span></li>
                                    <li><span>Les efforts volontaires locaux sont utilisés dans des initiatives et des projets d'acteurs externes suivant un schéma hiérarchique descendant avec une rétroaction ascendante et une voix au chapitre a minima.</span></li>
                                    <li><span>Les gouvernements et d'autres acteurs contrôlent sévèrement la place accordée à l'action des bénévoles.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box2" style="background-image: url(images/more_res_fr.svg);background-size: 485px; background-position: 60px 97.7%;">
                        <div class=" bg-rect-blue2 flip-container" data-wow-delay="400ms">
                           <div class="perspective-fix"></div>
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-plus">Des communautés <br>plus résilientes</span>
                                 <span class="text-color-white font-weight-700 white-plus">Un volontariat <br>plus résilient</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-15">
                                    <li><span>Des rôles complémentaires sont établis entre différents acteurs de résilience en fonction de leurs forces et de leurs contributions.</span></li>
                                    <li><span> Les partenariats sont plus stratégiques et intersectoriels au lieu de projets ad hoc limités dans le temps qui impliquent des volontaires locaux.</span></li>
                                    <li><span>Les approches s'appuient sur les propres priorités des volontaires au lieu de les utiliser pour mettre en œuvre des initiatives suivant un schéma hiérarchique descendant.</span></li>
                                    <li><span>L'axe d'engagement du volontaire englobe la prévention, l’adaptation et les problèmes à long terme, au lieu d'apporter une atténuation et une réponse immédiates.</span></li>
                                    <li><span>La répartition des ressources est à la mesure des responsabilités, offrant un meilleur contrôle aux volontaires locaux qui assument la charge de travail. </span></li>
                                    <li><span> Des opportunités de volontariat inclusif sont créées afin de bénéficier du bénévolat, telles que des initiatives et des schémas spécifiques pour des groupes marginalisés.</span></li>
                                    <li><span>Des liens sont créés entre différents groupes de personnes assumant ensemble des tâches de volontariat, et non avec des personnes issues du même milieu.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
                  <div class="row  arrows-bottom desktop">
                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0  padding-bottom-30 quote-box quote-box3" style="background-image: url(images/less_res_vol_fr.svg);background-size: 485px;">
                        <div class=" bg-rect-blue3 flip-container" data-wow-delay="300ms">
                           <div class="perspective-fix"></div>
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-minus ">Des communautés <br>moins résilientes</span>
                                 <span class="text-color-white font-weight-700 white-minus ">Un volontariat <br>moins résilient</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-40">
                                    <li><span>Les opportunités de volontariat sont exclusives. Par ex., les plus vulnérables sont impliqués uniquement dans le bénévolat non structuré et les tâches moins reconnues.</span></li>
                                    <li><span>Le volontariat relève de l'exploitation. Par ex., les volontaires sont supposés exercer des rôles pour d'autres acteurs mais sans formation, ressources, ni sécurité.</span></li>
                                    <li><span>Le volontariat conduit au désengagement. Par ex., des agences externes sont à la recherche de contributions et d'informations par l'intermédiaire de systèmes d'alerte précoce mais ne les partagent pas en retour avec les communautés.</span></li>
                                    <li><span>Le volontariat manque de ressources, épuisant davantage encore les ressources limitées d'individus déjà vulnérables dans les communautés lorsqu'ils travaillent à la gestion des risques.</span></li>
                                    <li><span>Le volontariat est mal reconnu et valorisé, et peut même être stigmatisé.</span></li>
                                    <li><span>Les bénévoles se concentrent sur des actions à court terme, telles que la gestion des crises, ce qui risque d'en réduire l'impact et de les conduire au surmenage.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-30 quote-box quote-box4" style="background-image: url(images/less_res_fr.svg);background-size: 39px;">
                        <div class=" bg-rect-blue4 flip-container" data-wow-delay="400ms">
                           <div class="perspective-fix"></div>
                          
                           <div class="flipper">
                              <div class="front">
                                 <span class="text-color-darkblue font-weight-700 dark-minus">Des communautés <br>moins résilientes</span>
                                 <span class="text-color-white font-weight-700 white-plus">Un volontariat <br>plus résilient</span>
                              </div>
                              <div class="back">
                                 <ul class="padding-top-100">
                                    <li><span>Le volontariat crée une forte cohésion entre les individus du même milieu qui font du bénévolat ensemble.</span></li>
                                    <li><span>Les communautés sont capables de déterminer les priorités, mais cela inclut de décider des destinataires de leur aide au sein de la communauté et à l'extérieur de cette dernière.</span></li>
                                    <li><span>Un manque de coordination entre les acteurs plus larges et les volontaires locaux réduit l'impact.</span></li>
                                    <li><span>Le volontariat est vivant mais n'est pas intégré dans des stratégies et des plans de résilience.</span></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
                  <a href="files/figure4_FR.pdf" target="_blank" class="dwn">Télécharger la figure</a>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-top-bottom" id="sharing">
         <div class="container">

            <div class="">
               <div class="section-heading text-center">
                  <div class="row">
                     <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                        <h2 class="text-color-blue fontface-two padding-bottom-5" data-wow-delay="300ms">Partagez nos principales conclusions!</h2>
                     </div>
                  </div>
               </div>
               <div class="padding-top-60 row">

                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card1.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Le volontariat local est une stratégie de résilience fondamentale et une caractéristique des communautés résilientes.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 L'échelle et la portée de l'activité volontaire en réponse aux chocs et aux stress sont sans précédent. De plus, la contribution du volontariat dépasse largement son ampleur, car à l'instar d'autres formes de participation civique, elle constitue à la fois un moyen de développement et une fin en soi.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le+volontariat+local+est+une+strategie+de+resilience+fondamentale+et+une+caracteristique+des+communautes+resilientes.&image=share_card1'); ?>"><i class="fab fa-linkedin-in"></i></a>

                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le volontariat local est une strategie de resilience fondamentale et une caracteristique des communautes resilientes.&description=L’echelle et la portee de l’activite volontaire en reponse aux chocs et aux stress sont sans precedent. De plus, la contribution du volontariat depasse largement son ampleur, car a l’instar d’autres formes de participation civique, elle constitue a la fois un moyen de developpement et une fin en soi.&image=share_card1'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le volontariat local est une strategie de resilience fondamentale et une caracteristique des communautes resilientes.&description=L’echelle et la portee de l’activite volontaire en reponse aux chocs et aux stress sont sans precedent. De plus, la contribution du volontariat depasse largement son ampleur, car a l’instar d’autres formes de participation civique, elle constitue a la fois un moyen de developpement et une fin en soi.&image=share_card1'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Le%20volontariat%20local%20est%20une%20strategie%20de%20resilience%20fondamentale%20et%20une%20caracteristique%20des%20communautes%20resilientes.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card2.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Le volontariat local permet de mettre en œuvre des stratégies collectives de gestion des risques.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 En unissant les initiatives individuelles dans un objectif commun, le volontariat élargit les choix et les opportunités qui s'offrent aux communautés dans leur préparation et réaction aux crises.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le+volontariat+local+permet+de+mettre+en+œuvre+des+strategies+collectives+de+gestion+des+risques.&image=share_card2'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le volontariat local permet de mettre en œuvre des strategies collectives de gestion des risques.&description=En unissant les initiatives individuelles dans un objectif commun, le volontariat elargit les choix et les opportunites qui s’offrent aux communautes dans leur preparation et reaction aux crises.&image=share_card2'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le volontariat local permet de mettre en œuvre des strategies collectives de gestion des risques.&description=En unissant les initiatives individuelles dans un objectif commun, le volontariat elargit les choix et les opportunites qui s’offrent aux communautes dans leur preparation et reaction aux crises.&image=share_card2'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Le%20volontariat%20local%20permet%20de%20mettre%20en%20œuvre%20des%20strategies%20collectives%20de%20gestion%20des%20risques.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card3.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Les aspects du volontariat local les plus appréciés par les communautés sont la capacité à s'auto-organiser et à créer des liens avec les autres.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Les membres de la communauté apprécient de pouvoir définir leurs propres priorités de développement et s'approprier les problèmes locaux. Les opportunités de développement de réseaux, de la confiance et de l'empathie grâce à l'action sociale sont reconnues dans tous les contextes.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Les+aspects+du+volontariat+local+les+plus+apprecies+par+les+communautes+sont+la+capacite+a+s’auto-organiser+et+a+creer+des+liens+avec+les+autres.&image=share_card3'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Les aspects du volontariat local les plus apprecies par les communautes sont la capacite a s’auto-organiser et a creer des liens avec les autres.&description=Les membres de la communaute apprecient de pouvoir definir leurs propres priorites de developpement et s’approprier les problèmes locaux. Les opportunites de developpement de reseaux, de la confiance et de l’empathie grâce a l’action sociale sont reconnues dans tous les contextes.&image=share_card3'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Les aspects du volontariat local les plus apprecies par les communautes sont la capacite a s’auto-organiser et a creer des liens avec les autres.&description=Les membres de la communaute apprecient de pouvoir definir leurs propres priorites de developpement et s’approprier les problèmes locaux. Les opportunites de developpement de reseaux, de la confiance et de l’empathie grâce a l’action sociale sont reconnues dans tous les contextes.&image=share_card3'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Les%20aspects%20du%20volontariat%20local%20les%20plus%20apprecies%20par%20les%20communautes%20sont%20la%20capacite%20a%20s’auto-organiser%20et%20a%20creer%20des%20liens%20avec%20les%20autres.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>


                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card4.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Selon les conditions, ces caractéristiques distinctives du volontariat local peuvent à la fois stimuler et diminuer la résilience communautaire.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 La dualité du volontariat à la fois comme une fin et un moyen de développement signifie que chacun de ses aspects inhérents est potentiellement positif ou négatif, selon le contexte.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Selon+les+conditions,+ces+caracteristiques+distinctives+du+volontariat+local+peuvent+a+la+fois+stimuler+et+diminuer+la+resilience+communautaire.&image=share_card4'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Selon les conditions, ces caracteristiques distinctives du volontariat local peuvent a la fois stimuler et diminuer la resilience communautaire.&description=La dualite du volontariat a la fois comme une fin et un moyen de developpement signifie que chacun de ses aspects inherents est potentiellement positif ou negatif, selon le contexte.&image=share_card4'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Selon les conditions, ces caracteristiques distinctives du volontariat local peuvent a la fois stimuler et diminuer la resilience communautaire.&description=La dualite du volontariat a la fois comme une fin et un moyen de developpement signifie que chacun de ses aspects inherents est potentiellement positif ou negatif, selon le contexte.&image=share_card4'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Selon%20les%20conditions,%20ces%20caracteristiques%20distinctives%20du%20volontariat%20local%20peuvent%20a%20la%20fois%20stimuler%20et%20diminuer%20la%20resilience%20communautaire.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card5.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Le volontariat revêt une importance particulière pour les groupes vulnérables et marginalisés.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 L’entraide, l’auto-assistance et la réciprocité sont des stratégies d’adaptation importantes pour les communautés isolées et vulnérables. Les activités auto-organisées peuvent aider les groupes marginalisés à satisfaire leurs propres besoins en l'absence de dispositions et de services plus conséquents.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le+volontariat+revêt+une+importance+particulière+pour+les+groupes+vulnerables+et+marginalises.&image=share_card5'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le volontariat revêt une importance particulière pour les groupes vulnerables et marginalises.&description=L’entraide, l’auto-assistance et la reciprocite sont des strategies d’adaptation importantes pour les communautes isolees et vulnerables. Les activites auto-organisees peuvent aider les groupes marginalises a satisfaire leurs propres besoins en l’absence de dispositions et de services plus consequents.&image=share_card5'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Le volontariat revêt une importance particulière pour les groupes vulnerables et marginalises.&description=L’entraide, l’auto-assistance et la reciprocite sont des strategies d’adaptation importantes pour les communautes isolees et vulnerables. Les activites auto-organisees peuvent aider les groupes marginalises a satisfaire leurs propres besoins en l’absence de dispositions et de services plus consequents.&image=share_card5'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Le%20volontariat%20revêt%20une%20importance%20particulière%20pour%20les%20groupes%20vulnerables%20et%20marginalises.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card6.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Les coûts et les avantages du volontariat ne sont pas toujours répartis équitablement.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Les femmes sont plus susceptibles d'assumer la plus grande part des tâches de volontariat non structuré au sein de leur propre communauté, par exemple, dans le prolongement de leur rôle de soutien domestique. L'accès à des opportunités officielles de volontariat pour développer des compétences, créer des liens et accéder à des ressources n'est pas donné à tous, surtout aux populations à faible revenu.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Les+coûts+et+les+avantages+du+volontariat+ne+sont+pas+toujours+repartis+equitablement.&image=share_card6'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Les coûts et les avantages du volontariat ne sont pas toujours repartis equitablement.&description=Les femmes sont plus susceptibles d’assumer la plus grande part des tâches de volontariat non structure au sein de leur propre communaute, par exemple, dans le prolongement de leur rôle de soutien domestique. L’accès a des opportunites officielles de volontariat pour developper des competences, creer des liens et acceder a des ressources n’est pas donne a tous, surtout aux populations a faible revenu.&image=share_card6'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Les coûts et les avantages du volontariat ne sont pas toujours repartis equitablement.&description=Les femmes sont plus susceptibles d’assumer la plus grande part des tâches de volontariat non structure au sein de leur propre communaute, par exemple, dans le prolongement de leur rôle de soutien domestique. L’accès a des opportunites officielles de volontariat pour developper des competences, creer des liens et acceder a des ressources n’est pas donne a tous, surtout aux populations a faible revenu.&image=share_card6'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Les%20coûts%20et%20les%20avantages%20du%20volontariat%20ne%20sont%20pas%20toujours%20repartis%20equitablement.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>



                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="300ms">
                           <img src="images/share_card7.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Il est important de considérer la manière dont les acteurs extérieurs s’engagent dans le volontariat local.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Les partenariats sont appelés à valoriser les aspects positifs du volontariat salués par les communautés, ceux qui permettent de renforcer la capacité d’auto-organisation et les liens relationnels. Les acteurs de la paix et du développement peuvent nuire au volontariat s’ils considèrent les populations comme de simples ressources disponibles et bon marché. Lorsqu’ils sont mal structurés, les partenariats avec les volontaires locaux peuvent accroître les inégalités.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Il+est+important+de+considerer+la+manière+dont+les+acteurs+exterieurs+s’engagent+dans+le+volontariat+local.&image=share_card7'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Il est important de considerer la manière dont les acteurs exterieurs s’engagent dans le volontariat local.&description=Les partenariats sont appeles a valoriser les aspects positifs du volontariat salues par les communautes, ceux qui permettent de renforcer la capacite d’auto-organisation et les liens relationnels. Les acteurs de la paix et du developpement peuvent nuire au volontariat s’ils considèrent les populations comme de simples ressources disponibles et bon marche. Lorsqu’ils sont mal structures, les partenariats avec les volontaires locaux peuvent accroître les inegalites.&image=share_card7'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Il est important de considerer la manière dont les acteurs exterieurs s’engagent dans le volontariat local.&description=Les partenariats sont appeles a valoriser les aspects positifs du volontariat salues par les communautes, ceux qui permettent de renforcer la capacite d’auto-organisation et les liens relationnels. Les acteurs de la paix et du developpement peuvent nuire au volontariat s’ils considèrent les populations comme de simples ressources disponibles et bon marche. Lorsqu’ils sont mal structures, les partenariats avec les volontaires locaux peuvent accroître les inegalites.&image=share_card7'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Il%20est%20important%20de%20considerer%20la%20manière%20dont%20les%20acteurs%20exterieurs%20s’engagent%20dans%20le%20volontariat%20local.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="400ms">
                           <img src="images/share_card8.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Tisser des partenariats efficaces avec les volontaires peut faire évoluer le volontariat d’un mécanisme d’adaptation à une ressource stratégique pour la résilience communautaire.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Les partenariats complémentaires avec les communautés aident à équilibrer les risques de manière plus équitable, maximisant potentiellement l’impact positif du volontariat sur ceux qui sont souvent laissés pour compte. Le partage équilibré des ressources et des capacités entre les acteurs permet aux communautés d’adopter des approches préventives, à plus long terme, pour faire face aux risques.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Tisser+des+partenariats+efficaces+avec+les+volontaires peut+faire+evoluer+le+volontariat+d’un+mecanisme+d’adaptation+a+une+ressource+strategique+pour+la+resilience+communautaire.&image=share_card8'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Tisser des partenariats efficaces avec les volontaires peut faire evoluer le volontariat d’un mecanisme d’adaptation a une ressource strategique pour la resilience communautaire.&description=Les partenariats complementaires avec les communautes aident a equilibrer les risques de manière plus equitable, maximisant potentiellement l’impact positif du volontariat sur ceux qui sont souvent laisses pour compte. Le partage equilibre des ressources et des capacites entre les acteurs permet aux communautes d’adopter des approches preventives, a plus long terme, pour faire face aux risques.&image=share_card8'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Tisser des partenariats efficaces avec les volontaires peut faire evoluer le volontariat d’un mecanisme d’adaptation a une ressource strategique pour la resilience communautaire.&description=Les partenariats complementaires avec les communautes aident a equilibrer les risques de manière plus equitable, maximisant potentiellement l’impact positif du volontariat sur ceux qui sont souvent laisses pour compte. Le partage equilibre des ressources et des capacites entre les acteurs permet aux communautes d’adopter des approches preventives, a plus long terme, pour faire face aux risques.&image=share_card8'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Tisser%20des%20partenariats%20efficaces%20avec%20les%20volontaires%20peut%20faire%20evoluer%20le%20volontariat%20d’un%20mecanisme%20d’adaptation%20a%20une%20ressource%20strategique%20pour%20la%20resilience%20communautaire.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-0 offset-sm-0 padding-bottom-60">
                        <div class="wow fadeInUp share-box" data-wow-delay="500ms">
                           <img src="images/share_card9.jpg" alt="">
                           <div class="background-white share-text">
                              <p class="fontface-one text-color-blue padding-bottom-15 paragraph-18">Un environnement favorable au volontariat renforce la résilience communautaire.</p>
                              <p class="text-color-darkblue paragraph-15">
                                 Les gouvernements et autres parties prenantes peuvent consolider la contribution du volontariat au renforcement de la résilience de deux manières: D’abord, par la promotion d’un écosystème propice au volontariat et, ensuite, en formant des partenariats fondés sur une meilleure perception de la valeur des contributions faites par les communautés. Cela garantira que les processus de localisation dans le cadre du Programme 2030 s’appuieront sur l’engagement et les innovations des citoyens du monde entier.</p>
                           </div>
                           <div class="sharer clearfix">
                              <ul>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Un+environnement+favorable+au+volontariat+renforce+la+resilience+communautaire.&image=share_card9'); ?>"><i class="fab fa-linkedin-in"></i></a>
                                 </li>
                                 <!-- <li>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Un environnement favorable au volontariat renforce la resilience communautaire.&description=Les gouvernements et autres parties prenantes peuvent consolider la contribution du volontariat au renforcement de la resilience de deux manières: D’abord, par la promotion d’un ecosystème propice au volontariat et, ensuite, en formant des partenariats fondes sur une meilleure perception de la valeur des contributions faites par les communautes. Cela garantira que les processus de localisation dans le cadre du Programme 2030 s’appuieront sur l’engagement et les innovations des citoyens du monde entier.&image=share_card9'); ?>"><i class="fab fa-google"></i></a>
                                 </li> -->
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode('https://www.unv-swvr2018.org/index.php?title=Un environnement favorable au volontariat renforce la resilience communautaire.&description=Les gouvernements et autres parties prenantes peuvent consolider la contribution du volontariat au renforcement de la resilience de deux manières: D’abord, par la promotion d’un ecosystème propice au volontariat et, ensuite, en formant des partenariats fondes sur une meilleure perception de la valeur des contributions faites par les communautes. Cela garantira que les processus de localisation dans le cadre du Programme 2030 s’appuieront sur l’engagement et les innovations des citoyens du monde entier.&image=share_card9'); ?>"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 <li>
                                    <a onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" href="https://twitter.com/intent/tweet?text=Un%20environnement%20favorable%20au%20volontariat%20renforce%20la%20resilience%20communautaire.&url=https://www.unv-swvr2018.org/"><i class="fab fa-twitter"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>


               </div>
            </div>
         </div>
      </section>


      <section class="background-white padding-top-bottom" id="voices">
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 text-center">
                        <h2 class="text-color-blue fontface-two padding-bottom-30" data-wow-delay="300ms">Points de vue du monde entier</h2>
                        <!-- <p class="padding-bottom-50 icon-hand-here">
                        </p> -->
                        <img class="image-hand padding-bottom-40" src="images/hoverovermouse_fr.svg" alt="">

                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-green flip-container" data-wow-delay="300ms" ontouchstart="this.classList.toggle('hover');">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">LE POINT DE VUE D’UNE AGENCE DE STATISTIQUES NATIONALE
</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Volontariat et statistiques dans les pays du Sud</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Isabel</p>
                              </div>
                              <div class="back">
                                 <p>Tshepiso recule de quelques pas et admire son travail. Dans le cadre de sa contribution à la Journée Mandela, il a peint les murs intérieurs d'une cabane en tôle ondulée qui sert de crèche pour les jeunes enfants dans une institution informelle à Johannesburg. Selon la tradition Ubuntu,<sup>a</sup> il transporte régulièrement ses tantes, oncles et parents âgés à l'hôpital ou les aide à faire leurs courses. Le week-end dernier, il a réparé une porte de placard de cuisine cassée pour sa voisine, Mme Potts.</p>
                                 <p>Permettre à des personnes telles que Tshepiso de décider de leurs propres priorités de développement et de leurs actions en faveur du changement est l’une des pierres angulaires du développement durable. Une société civile dynamique est une composante essentielle d'un État solidaire et fonctionnel. En encourageant une citoyenneté engagée, l’État peut potentiellement atteindre des objectifs plus ambitieux, en investissant moins d'argent et en obtenant une plus grande cohésion sociale dans la foulée. Un volontaire se demande: comment puis-je aider ma famille élargie, ma communauté, mon pays et la planète tout entière?</p>
                                 <p><a href="https://www.unv.org/fr/node/3882">PLUS D'INFORMATIONS</a></p>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-blue flip-container" data-wow-delay="400ms">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VOIX DE VOLONTAIRES</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">L'action collective sape la résilience au Guatemala</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Roselia</p>
                              </div>
                              <div class="back">
                                 <p>Il y a dix ans, personne ne se souciait des forêts de la communauté. Nous avons continué à couper des arbres. Puis, j'ai décidé de créer un groupe de volontaires pour en planter. J'ai parlé avec des femmes de la communauté et beaucoup souhaitaient participer, ce qui m'a motivée davantage. Au début, nous étions 50 femmes et quelqu'un de la communauté nous a prêté un morceau de terre pour planter nos arbres. C'est comme ça que notre groupe est né.</p>
                                 <p>Le volontariat m'a beaucoup aidé. Il y a dix ans, j'étais quelqu'un d'autre. Je ne connaissais pas mes droits. Avant, lorsqu'un homme me disait que je ne savais rien, je pleurais et pensais: « Oui, il a raison ». J'avais peur de dire quoi que ce soit devant des hommes, mais plus maintenant. Maintenant, nous discutons et je n'ai pas peur de dire ce que je pense. Par exemple, un jour, quelqu'un a offensé les femmes de la communauté et je les ai défendues. Elles m'ont dit: « Roselia, tu n'as plus peur de rien ».</p>
                                 <p>Dans notre groupe, les femmes prennent des décisions autonomes. Avant nous n'avions nulle part où aller et aucun moyen de participer. Avant c'était seulement « casa y casa » (à la maison, rien qu'à la maison). Nous avons maintenant un endroit où nous pouvons parler, nous rencontrer, nous détendre et exercer nos droits. Dans la pépinière, nous partageons nos joies et nos problèmes. Nous sommes unies.</p>
                                 <p>Nous voulons que davantage de personnes reboisent leurs montagnes dans la municipalité, alors nous allons passer le mot sur notre travail bénévole. </p>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
                  <div class="row">
                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0  padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-orange flip-container" data-wow-delay="300ms">
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">LE POINT DE VUE D’UN GOUVERNEMENT MUNICIPAL</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Le volontariat: un outil d'intégration sociale dans les villes</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Matthew</p>
                              </div>
                              <div class="back">
                                 <p>Avec le maire de Londres, Sadiq Khan, mon rôle est de rassembler tous les Londoniens et de renforcer nos communautés. L'une des leçons les plus importantes que j'ai apprises est que le volontariat permet d'atteindre ces objectifs.</p>
                                 <p>Au début de cette année, nous avons lancé notre stratégie d’intégration sociale. Reposant sur des recherches approfondies, elle donne une nouvelle définition de l'intégration sociale. Elle insiste sur le fait qu'elle va au-delà du simple lien entre les personnes mais qu'elle assure également la promotion de l'égalité et l'amélioration des niveaux d'activités de la population, ainsi que sa participation au sein des communautés locales.</p>
                                 <p>Mais encourager l’intégration sociale est un exercice dénué de sens si les gens n'ont pas l'occasion de se rassembler. Le volontariat remplit exactement ce rôle.</p>
                                 <p>Il aide les citoyens à nouer des relations avec d'autres membres de leur communauté, qui peuvent être issus de milieux totalement différents. Il crée des liens et un sentiment d'appartenance qui vont au-delà des différences superficielles qui pourraient sinon sembler importantes. Le volontariat est également un moyen efficace de résoudre des problèmes sociaux, par exemple, en réduisant l'isolement social ou en améliorant la santé mentale, tant pour le volontaire que pour la personne qui bénéficie du volontariat.</p>
                                 <p><a href="https://www.unv.org/fr/node/3790">PLUS D'INFORMATIONS</a></p>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-lg-6 offset-lg-0 col-md-12 offset-md-0 offset-sm-0 padding-bottom-30 quote-box">
                        <div class="wow fadeInUp bg-rect-red flip-container" data-wow-delay="400ms">
                           
                           <div class="flipper">
                              <div class="front">
                                 <span class="paragraph-15 text-color-white">VOIX DE VOLONTAIRES</span>
                                 <h4 class="fontface-one text-color-white font-weight-700 padding-bottom-10">Des volontaires travaillant en ligne utilisent la cartographie géospatiale pour organiser les interventions d'urgence</h4>
                                 <p class="text-color-darkgrey paragraph-14 fontface-two">
                                    Rohini</p>
                              </div>
                              <div class="back">
                                 <p>En tant que spécialiste du géospatial, je suis volontaire pour cartographier les zones affectées en utilisant les données satellitaires. </p>
                                 <p>Le 20 septembre 2017, vers minuit, j'ai reçu un e-mail:</p>
                                 <p>“Chers volontaires du GISCorps,  ...nous cherchons de l'aide pour évaluer les dommages causés à... des centres de santé affectés par l'ouragan Maria à Porto Rico... Si vous êtes intéressés et disponibles, veuillez nous envoyer un courriel.”</p>
                                 <p>J'ai immédiatement répondu, de même que cinq autres volontaires de différents coins du monde. En travaillant ensemble au moyen d'un groupe en ligne, nous avons parcouru des kilomètres et des kilomètres de données en quelques jours, ce qui aurait pris des semaines sur le terrain. Disposer de ce type d'informations au bon moment peut accélérer les opérations de relèvement et même sauver des vies. Le volontariat en ligne est un moyen rentable et efficace pour transmettre des informations satellitaires importantes aux équipes sur le terrain. Cela me permet aussi d'utiliser mes compétences techniques d'une manière qui revêt du sens à mes yeux et d'apporter ma contribution pour aider l'humanité. Je crois que les futurs efforts de secours et de relèvement après catastrophe seront de plus en plus tributaires des données de télédétection, comme celles des drones. En analysant ces informations à l'aide de plates-formes de cartographie géospatiale créées de façon participative, les volontaires comme moi ont un rôle important à jouer.</p>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>



      <section class="background-grey padding-top-bottom" id="">
         <div class="container">

                  <div class="row">
                     <div class="col-lg-9 offset-lg-2 col-md-12 offset-md-0 offset-sm-0">
                        <div class="" data-wow-delay="300ms">
                           <span class="quote-sign">“</span>
                           <!-- Slider main container -->
                           <div class="swiper-container testimonials">
                               <!-- Additional required wrapper -->
                               <div class="swiper-wrapper">
                                   <!-- Slides -->
                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">Le volontariat relie les gens, leur permettant de travailler ensemble pour résoudre les problèmes urgents de notre époque. Pour tenir notre promesse et faire des objectifs de développement durable une réalité pour tous, nous avons besoin que chacun suive l'exemple du milliard de volontaires actuellement estimé et fasse la différence dans chacune de nos communautés.</p>
                                       <span class="author">Achim Steiner</span>
                                       <span class="title">Administrateur, programme des Nations Unies pour le développement</span>
                                   </div>

                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">Le volontariat n’est pas le seul moyen d'améliorer l’intégration sociale et il n'est pas la clé de tous les problèmes. Mais c'est un outil extrêmement important que le gouvernement et les autorités locales peuvent utiliser pour rassembler les gens. Nous savons que tous les Londoniens souhaitent être des membres appréciés de leur communauté et jouer un rôle actif dans les décisions qui comptent pour notre ville.</p>
                                       <span class="author">Matthew Ryder </span>
                                       <span class="title">Adjoint au maire, Londres </span>
                                   </div>

                                   <div class="swiper-slide">
                                      <p class="fontface-two paragraph-32">En encourageant une citoyenneté engagée, l’État peut potentiellement atteindre des objectifs plus ambitieux, en investissant moins d'argent et en obtenant une plus grande cohésion sociale dans la foulée. Un volontaire se demande: Comment puis-je aider ma famille élargie, ma communauté, mon pays et la planète tout entière ?</p>
                                       <span class="author">Isabel Schmidt</span>
                                       <span class="title">Directrice, South Africa Stats</span>
                                   </div>

                               </div>

                               <div class="swiper-pagination"></div>

                           </div>
                        </div>
                     </div>
                  </div>

         </div>
      </section>


      <section class="padding-top-bottom bg-setting padding-bottom-45" data-image-src="images/bg_cta.jpg" id="call-to-action">
        <img id="unv-circle" src="images/unv-circle.svg" alt=""/>
         <div class="container">

            <div class="">
               <div class="">
                  <div class="row">
                     <div class="col-lg-12 offset-lg-0 col-md-12 offset-md-0 offset-sm-0">
                        <div class="text-center" data-wow-delay="300ms">
                           <h2 class="fontface-two text-color-blue padding-bottom-60">Volontariat et le Programme 2030</h2>
                           <p class="">Le Programme 2030 exige que tous les acteurs s'appuient sur l'engagement, la représentation et l'innovation des citoyens du monde entier.</p>

                           <p class="">Comprendre les volontaires et s'associer à eux en s'appuyant sur des normes d'inclusion et d'équité favorise une approche du développement centrée sur la population.</p>

                           <p class="">Les communautés seront-elles la dernière ligne de défense ou la première ligne de prévention ? </p>

                           <p class="">Cela dépendra de la manière dont les gouvernements et les partenaires du développement choisiront de soutenir les volontaires dans leur travail au quotidien visant à renforcer la résilience.</p>

                           <a href="https://www.unv.org/fr/node/2866" target="_blank" class="btn btn-blue padding-top-50">Participer</a>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="background-white padding-bottom-70 " id="footer">
         <div class="footer-top-bg padding-top-70 padding-bottom-45">
            <div class="container">
               <div class="row">
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">À PROPOS DU RAPPORT</p>
                        <p>Tous les trois ans, le Programme des Volontaires des Nations Unies (VNU) publie le Rapport sur l'état du volontariat dans le monde (SWVR), une publication phare des Nations Unies visant à renforcer la compréhension du volontariat et à démontrer son universalité, son envergure et sa portée au vingt et unième siècle.</p>
                        <div class="footer-logo">
                           <a href="https://www.unv.org/fr/node/2865" target="_blank"><img src="images/logo_fr.svg" alt="logo"></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">RETROUVEZ-NOUS EN LIGNE</p>
                        <ul class="footer-contact">
                           <li>
                              <a target="_blank" href="https://twitter.com/evidenceunv"><i class="fab fa-twitter"></i>@EvidenceUNV</a>
                           </li>
                           <li>
                              <a href="mailto:unv.swvr@unv.org"><i class="fas fa-envelope"></i>unv.swvr@unv.org</a>
                           </li>
                           <li>
                              <a href="https://www.unv.org/fr/node/2865"><i class="fas fa-home"></i>www.unv.org/swvr</a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp footer-engage" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">IMPLIQUEZ-VOUS À NOS CÔTÉS</p>
                        <a href="https://www.unv.org/swvr/swvr-policy-challenge" target="_blank">Organisez un débat politique, ou prenez-y part, ou bien planifiez un événement</a>
                        <a href="https://www.unv.org/fr/swvr/blogsubmissionguidelines" target="_blank">Intervenez sur notre blog</a>
                        <a href="http://eepurl.com/c9U54H" target="_blank">Inscrivez-vous à notre liste de diffusion</a>
                        <a href="mailto:unv.swvr@unv.org" target="_blank">Partagez vos idées et vos réflexions</a>
                     </div>
                  </div>
                  <div class="col-lg-3 offset-lg-0 col-md-6 offset-md-0 offset-sm-0">
                     <div class="wow fadeInUp" data-wow-delay="300ms">
                        <p class="paragraph-15 padding-bottom-15 footer-title">TÉLÉCHARGEMENTS</p>
                        <ul class="footer-contact">
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_FR_WEB.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Rapport complet</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_FR_WEB_OVERVIEW.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Présentation</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_FR_WEB_CH1+INTRO.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Introduction et chapitre 1</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_FR_WEB_CH2.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapitre 2</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_FR_WEB_CH3.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapitre 3</a>
                           </li>
                           <li>
                              <a href="files/51692_UNV_SWVR_2018_FR_WEB_CH4+CONC.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Chapitre 4 et conclusion</a>
                           </li>
                           <li>
                              <a class="" href="files/51692_UNV_SWVR_2018_FR_WEB_ANNEX+NOTES+REFERENCES.pdf" target="_blank"><i class="fas fa-arrow-down"></i>Annexes</a>
                           </li>

                        </ul>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </section>



   </section>
   <!-- /Page Wrapper End -->

   <!-- jQuery v3.1.1 -->
   <script defer src="js/jquery.js"></script>

   <!-- Popper Core Javascript -->
   <script defer src="js/popper.min.js"></script>

   <!-- Bootstrap Core JavaScript -->
   <script defer src="js/bootstrap.min.js"></script>

   <!-- Appear Core Javascript -->
   <script defer src="js/jquery.appear.min.js"></script>

   <!-- Header Threads Files -->
   <script defer src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
   <script defer src="js/threads.js"></script>
   <script defer src="js/threads_init.js"></script>


   <!--  Fancybox Core Javascript -->
   <script defer src="js/jquery.fancybox.min.js"></script>

   <!-- Swiper Core Javascript -->
   <script defer src="js/swiper.min.js"></script>

   <!--wow Transitions-->
   <script defer src="js/wow.min.js"></script>

   <!-- Custom JavaScript -->
   <script defer src="js/script.js"></script>
   <script defer src="js/TweenMax.min.js"></script>
   <script defer src="js/CSSRulePlugin.min.js"></script>
   <script defer src="js/jquery.waypoints.min.js"></script>
   <script defer src="js/custom-animations-fr.js"></script>

</body>
</html>
